<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define("admin&employee", function ($user) {
            if (isset($user->employee)) return $user->employee->hasAnyRoles(["admin", "employee"]);
        });
        Gate::define("adminOnly", function ($user) {
            if (isset($user->employee)) return $user->employee->hasRole("admin");
        });
    }
}
