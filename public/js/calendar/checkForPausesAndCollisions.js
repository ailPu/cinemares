/**
 * Check if event user wants to insert collides or has short pauses before/after 
 * @param {object} eventToInsert 
 * @param {[]} allEvents 
 * @returns {[]} Array that contains warnings
 */
function checkForPausesAndCollisions(eventToInsert,allEvents) {

	const warnings = [];
	for (const event of allEvents) {
		// Vergleiche nicht mit dir selbst! Das passiert nur bei Events, die schon im Kalender liegen! Also bei kopierten/draggeden Events
		if (event.id == eventToInsert.id) continue;
		// Überlappung
		if (
			event.start >= eventToInsert.start &&
			event.start <= eventToInsert.end
		) {
			alert("Bitte anderen Zeitslot wählen");
			return false;
		}
		// Subtrahiere Zeitstempel. Wenn auf 0, dann haben sie dieselben Startzeiten/Endzeiten = WARNUNG
		if (eventToInsert.start - event.end == 0) warnings.push("AnoPauseBefore");
		else if (eventToInsert.end - event.start == 0) warnings.push("CnoPauseAfter");
		// Bei 15 Minuten Unterschied = WARNUNG. Durch 1000 wegen Millisekunden. (900 Sek = 15 Min)
		else if ((eventToInsert.start - event.end) / 1000 == 900) warnings.push("BshortPauseBefore");
		else if ((event.start - eventToInsert.end) / 1000 == 900) warnings.push("DshortPauseAfter");
	}

	return warnings;

}
