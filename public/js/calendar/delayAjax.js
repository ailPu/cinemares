/**
 * @callback ajaxCallback
 */
/**
 * Execute a function on each input
 * @param {ajaxCallback} fn Callback-Function called when user types
 * @param {number} duration timeout 
 */
function delayAjax(fn, duration) {
   var timer;
   return function(){
      clearTimeout(timer);
      timer = setTimeout(fn, duration);
   }
}