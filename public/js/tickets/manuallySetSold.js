
$("form.set-sold").on("submit",function(e){
   e.preventDefault();
   $(".overlay").show();
   $.ajax({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      method:"post",
      url: $(this).attr("action"),
      type: "put",
      data: $(this).serialize(),
      success:function(resp){
         $(".overlay").hide();
         if(resp.status == 200) window.location.href = resp.successRoute;
         else if(resp.status = 400) window.location.href = resp.failureRoute;
      },
      error: function(err){
         console.log(err);
      }
   })
})