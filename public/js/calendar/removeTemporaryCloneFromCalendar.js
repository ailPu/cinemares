// If copycheckbox is selected and user drags an event, a temporary clone gets inserted at its position. Temporary/clientsides clones don't have an ID in calendar5. When user drops clone at position where clone cannot be dropped there are 2 events at the same spot. To prevent showing the same warnings if another events get placed near twice or more, any temporary clone have to be removed
/**
 * Remove potential temporary drawn clone from clientside to prevent showing warnings twice when checking for pauses and collisions
 * @param {object} calendar5 Object with all its available properties
 */
function removeTemporaryCloneFromCalendar(calendar5){
   const temporaryClone = calendar5.getEventById("")
   if(temporaryClone) temporaryClone.remove();
}