/**
 * Creates Timerange from current time till current view's enddate
 * @param {string} endDate 
 * @returns {object} Object that contains start- and endrange for current view
 */
function formatReleaseRangeForDb(endDate){
   let startRange = new Date();
   start = "";
   start += startRange.getFullYear() + "-";
   start += (startRange.getMonth() + 1) < 10 ? "0" + startRange.getMonth() + 1 : startRange.getMonth() + "-";
   start += (startRange.getDate()) < 10 ? "0" + startRange.getDate(): startRange.getDate() + " ";
   start += (startRange.getHours()) < 10 ? "0" + startRange.getHours(): startRange.getHours() + ":";
   start += (startRange.getMinutes()) < 10 ? "0" + startRange.getMinutes(): startRange.getMinutes() + ":";
   start += (startRange.getSeconds()) < 10 ? "0" + startRange.getSeconds(): startRange.getSeconds();

   return{
      start,
      end: moment(endDate, "DD.MM.YYYY").format("Y-MM-DD HH:mm:ss"),
   }
}