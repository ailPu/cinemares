/**
 * Gets called whenever User refreshes or changes view to keep last scrollposition
 */
function rememberScrollPosition() {
	let scrollTop;

	// get last known scrollposition from sessionstorage
	const top = sessionStorage.getItem("lastScrollPosition");
	if (top !== null) {
		$(".fc-scroller.fc-scroller-liquid-absolute").scrollTop(parseInt(top));
	}
	
	// onscroll always fires once at load. Not when rememberscollPosition is called recursive 
	$(".fc-scroller.fc-scroller-liquid-absolute").on("scroll",function(){
		scrollTop = $(".fc-scroller.fc-scroller-liquid-absolute").scrollTop();
	}) 

	/* Whenever user changes view, remove old eventListeners for scrollArea and clickEvents.
	Then call rememberScrollPosition recursive to initialize eventListeners for new Scrollarea and Buttons. onscroll is not fired in this case.
	After initializing, set the last known scrollposition from the previous instance of rememberscrollposition as your current scrollposition 
	*/
	$(".fc-toolbar-chunk:nth-child(1), .fc-toolbar-chunk:nth-child(3)").on("click",function(e){
		if (!$(e.target).closest("button").length) return;
		$(".fc-scroller.fc-scroller-liquid-absolute").off("scroll");
		$(".fc-toolbar-chunk:nth-child(1), .fc-toolbar-chunk:nth-child(3)").off("click");

		rememberScrollPosition();

		$(".fc-scroller.fc-scroller-liquid-absolute").scrollTop(parseInt(scrollTop));
	});

	// On unload store the last scrollposition in sessionstorage
	$(window).on("unload",function() {
			sessionStorage.setItem("lastScrollPosition", $(".fc-scroller.fc-scroller-liquid-absolute").scrollTop());
	})
}
