<?php

return [
   /*
   |--------------------------------------------------------------------------
   | Stripe Data
   |--------------------------------------------------------------------------
   */

   'PUBLISHABLE_KEY' => env('STRIPE_PUBLISHABLE_KEY'),
   'SECRET_KEY' => env('STRIPE_SECRET_KEY'),
   'CURRENCY' => env('STRIPE_CURRENCY'),
];
