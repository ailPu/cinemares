<?php

namespace App\Classes;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Guest
{
   private static $instance = null;
   protected $id = NULL;

   private function __construct()
   {
   }

   public static function instance()
   {
      if (is_null(self::$instance)) {
         self::$instance = new Guest;
      }
      return self::$instance;
   }

   public function setSessionId()
   {
      if ($this->id == NULL || $this->id != Session::getId()) {
         $this->id = Session::getId();
      }
   }

   public function __get($name)
   {
      return $this->$name;
   }
}
