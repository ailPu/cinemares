<?php


if (!function_exists("previewText")) {
   /**
    * Shortens Text if necessary
    * @param string $str
    * @param int $maxLength
    * @return string shortened Text
    */
   function previewText($str, $maxLength = 200)
   {
      if (mb_strlen($str) > $maxLength) {
         return mb_substr($str, 0, $maxLength) . "...";
      }
      // echo $str;
      return $str;
   }

   function substrwords($text, $maxchar, $end = ' ...')
   {
      if (mb_strlen($text) > $maxchar) {
         // Spalte Satz in Array aus Worten (immer bei whitespace)
         $words = preg_split('/\s/', $text);
         $output = '';
         $i = 0;
         while (true) {
            // Die Stringlänge ist immer die alte Stringlänge (anfangs 0) plus die Länge von dem string an dem gegenwärtigen Index
            $length = mb_strlen($output) + mb_strlen($words[$i]);
            // Sollte die neue Stringlänge dann größer sein als die maxlänge, dann beende Schleife
            if ($length > $maxchar) {
               break;
            } else {
               // Für dem alten String ein Leerzeichen und das Wort an dem gegenwärtigen Index hinzu und setze Index rauf
               $output .= " " . $words[$i];
               ++$i;
            }
         }
         // Füge noch "..." hinzu   
         $output .= $end;
      } else {
         $output = $text;
      }
      return $output;
   }
}
