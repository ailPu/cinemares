var globalLoggedOut = false;
// Logout user from calendar when no activity for certain time
/**
 * @param {string} logoutRoute 
 */
function listenForLogout(logoutRoute){
   var t;
   resetTimer();
   window.onclick = resetTimer;

   /**
    * Logout user 
    */
   function makeCalendarAvailable(){
      $(".overlay").show();
      $.ajax({
         url: logoutRoute,
         method: "delete",
         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
         success: function() {
            alert("Session abgelaufen. Die Seite wird neu geladen");
            globalLoggedOut = true;
            window.location.reload();
            return;
         },
         error: function(err){
            console.log(err);
         }
      });
   };

   /**
    * Reset Timer when Activity detected
    */
   function resetTimer(){
      clearTimeout(t);
      t = setTimeout(makeCalendarAvailable, 5 * 60 * 1000);
   }
   
   /* 
      Logout from Calendar when using Browser Navigationbuttons or when Browser/Tab is closing. Tested with Chrome & Edge. 
      Only go to logout route if user hasn't already been logged out because of sessionTimeout in makeCalendarAvailable(). Otherwise loop
   */
   $(window).on("beforeunload",()=>{
      if(globalLoggedOut) return;
      $.ajax({
         url: logoutRoute,
         method: "delete",
         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
         error: function(err){
            console.log(err);
         }
      })
   })

   /* 
      To logout from calendar when navigation Buttons clicked or Tab/Browser closing.
      The idea is to change hash value when site gets loaded. So when user clicks on browser navigation button, hash = empty string, so hashchange gets fired.
      Tested with Firefox Developer, since method above doesn't work with firefox developer
   */
   location.hash = "backBtnConfirm";
   $(window).on('hashchange', function() {
      console.log("HASHING???");
      // If the assigned hash value at start equals the one manually assigned, do nothing
      if(location.hash == "#backBtnConfirm") return;
      else {
            $(".overlay").show();
            $.ajax({
            url: logoutRoute,
            method: "delete",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function() {
               history.back()
            },
            error: function(err){
               console.log(err);
            }
         });
      }
   })
}