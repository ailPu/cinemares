@extends('layouts.app')

@section('pageTitle','Vorschau')

@section('content')

<div>
   <h1 class="text-center pt-4">Vorschau</h1>
</div>
<div class="movies container d-flex justify-content-center flex-wrap">
   @foreach($movies as $movie)
   <div class="col-12 row p-5">
      <div class="col-sm-4 col-md-3">
         <a href="{{route("movie.showPreview",$movie->id)}}">
            <img src="https://image.tmdb.org/t/p/original/{{$movie->poster_path}}" class="img-fluid" alt="">
         </a>
      </div>
      <div class="col-sm col-md-9">
         <div class="col-sm col-md-9">
            <h2 class="text-break">{{$movie->title}}</h2>
            <div class="col">
               <div class="d-none d-lg-block">
                  <div class="movie-duration row flex-nowrap">
                     <p class="col-md-5 col-lg-4"><strong>Dauer:</strong></p>
                     <p>{{$movie->duration}} Minuten</p>
                  </div>
                  <div class="movie-genre row flex-nowrap">
                     <p class="col-md-5 col-lg-4"><strong>Genre:</strong></p>
                     <p>{{$movie->genre}}</p>
                  </div>
                  <div class="movie-release-year row flex-nowrap">
                     <p class="col-md-5 col-lg-4"><strong>Veröffentlicht:</strong></p>
                     <p>{{$movie->release_year}}</p>
                  </div>
               </div>
            </div>
            <div class="col d-none d-md-block">
               <p>{{substrWords($movie->overview,100)}}</p>
               <a class="btn border border-dark btn-light" href="{{route("movie.showPreview",$movie->id)}}">Mehr
                  lesen</a>
            </div>
            <div class="col d-md-none">
               <p>{{substrWords($movie->overview,50)}}</p>
               <a class="btn border border-dark btn-light" href="{{route("movie.showPreview",$movie->id)}}">Mehr
                  lesen</a>
            </div>
         </div>
      </div>
   </div>
   @endforeach
</div>

@endsection