
$(".formDelete.user").on("submit",function(e){
   e.preventDefault();
   const tr = $(this).closest("tr"); 
   const delText = $(this).data("title");
   const url = $(this).attr("action");
   const data = $(this).serialize();

   $("#modal .modal-title").text("Löschen"); 
   $("#modalBody").text(delText) 
   $("#modal .btn.footer").addClass("btn-danger").text("Löschen");
   $('#modal').modal("show");

   $("#modal .btn.btn-primary").on("click",function(e){
      $('#modal').modal("hide");
   })

   $("#modal .btn.btn-danger").on("click",function(e){
      $('#modal').modal("hide");

      $(".overlay").toggle();
      $.ajax({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         url,
         type: "delete",
         data,
         success:function(resp){
            $(".overlay").toggle();
            if(resp.status == 200){
               $(".soft-deleted-users").removeClass("disabled");
               tr.remove();
               $(`<div class="alert alert-success">${resp.msg}</div>`).insertAfter("h1");
            }
            else $(`<div class="alert alert-danger">${resp.msg}</div>`).insertAfter("h1");
         },
         error:function(err){
            console.log(err);
         }
      })
   })
})