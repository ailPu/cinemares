
$(document).on("click",".formStore.event",function(e){
   e.preventDefault();
   const oldCartCount = $("#cart-quantity").text() * 1;
   const storeRoute = $(this).attr("action");
   const formHtml = $(this);
   let seatNum = $(this).find("[name=seat]").val()*1;
   seatNum = seatNum < 10 ? `0${seatNum}` : seatNum;
   const rowNum = $(this).find("[name=row]").val()*1;

   $(".overlay").toggle();
   $.ajax({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: storeRoute,
      type: "post",
      data: $(this).serialize(),
      success:function(resp){
         $(".overlay").toggle();
         if(resp.status == 200) { 
            const ticketId = resp.ticket;
            const destroyRoute = storeRoute + `/${ticketId}`;
            // Passe form Methode und actionUrl an
            formHtml.attr("action",destroyRoute).removeClass().addClass("formDelete event row")
            $('<input type="hidden" name="_method" value="delete">').appendTo(formHtml);
            $("button",formHtml).addClass("bg-warning");
            
            $("#total").text(resp.newTotal);
            $("#cart-quantity").text(oldCartCount + 1);
            if(resp.newTotal) $(".cart.btn").removeClass("disabled");             
         }
         else {
            if(resp.notAvailable != undefined && resp.notAvailable){
               $(".modal-body").text(resp.msg);
               $("#removeOtherTickets").css("visibility","hidden");
               formHtml.replaceWith(`<button class="seat sold h3 m-0 seat-width bg-danger" data-row="${rowNum}" data-seat="${seatNum}">&times;</button>`);
               $("#exampleModal").modal("show");   
            }
            else if(resp.hasTicketsWithDifferentEvent) {
               $(".modal-title").text(resp.msg_title);
               $(".modal-body").text(resp.msg_body);
               $("#removeOtherTickets").text(resp.btn_text).css("visibility","visible");
               $("#exampleModal").modal("show"); 
               $("#removeOtherTickets").click(function(){
                  e.preventDefault();
                  $("[data-dismiss=modal]").click();
                  $(".overlay").toggle();
                  $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                     url: resp.destroyOtherTicketsRoute,
                     type: "DELETE",
                     data: {
                        event_id: resp.event_id,
                     },
                     success:function(resp){
                        $(".overlay").toggle();
                        if(resp.status == 200) {
                           $(".event:first").prepend(`<div class="alert alert-success">${resp.msg}</div>`)
                           $("#cart-quantity").text(0);
                        };
                     },
                     error:function(err){
                        $(".overlay").toggle();
                        console.log(err);
                     }
                  })
               })
            }
         }
      },
      error:function(err){
         $(".overlay").toggle();
         console.log(err);
      },
   })
})