/**
 * If there are any warnings ask user if he wants to confirm date
 * @param {[]} warnings 
 * @returns {bool} bool
 */
function confirmDate(warnings) {
	const warningMessages = {
		AnoPauseBefore: "Achtung, keine Pause vor Film!",
		BshortPauseBefore: "Achtung, kurze Pause vor Film!",
		CnoPauseAfter: "Achtung, keine Pause nach Film!",
		DshortPauseAfter: "Achtung, kurze Pause nach Film!",
	};
	
	const warningsSorted = warnings.sort();

	for (const warning of warningsSorted) if (!confirm(warningMessages[warning])) return false;

	return true;
}
