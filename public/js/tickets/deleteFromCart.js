
$(".formDelete.cart").on("submit",function(e){
   e.preventDefault();
   const oldCartCount = $("#cart-quantity").text() * 1;
   const tr = $(this).closest("tr");
   $(".overlay").toggle();
   $.ajax({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: jQuery(this).attr("action"),
      type: "delete",
      data: $(this).serialize(),
      success:function(resp){
         $(".overlay").toggle();
         if(resp.status == 200) {
            tr.remove();
            $("#cart-quantity").text(oldCartCount - 1);
            if(!$("tbody.cart").children().length){
               $('.order-options').remove();
               $('<p class="no-tickets">Es befinden sich keine Tickets im Warenkorb</p>').insertAfter("table");
            }
            $("#total").text(resp.newTotal)
            return;
         }
      },
      error: function(err){
         $(".overlay").toggle();
         console.log(err);
      }
   })
})