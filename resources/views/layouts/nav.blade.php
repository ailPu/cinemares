<nav class="navbar p-0 border border-left-0 border-right-0 border-top-0 border-dark navbar-expand-sm navbar-light">
   <button class="navbar-toggler text-dark" type="button" data-toggle="collapse" data-target="#collapsibleFirstLevel"
      aria-controls="collapsibleFirstLevel" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon navbar-light"></span>
   </button>
   <div class="collapse navbar-collapse" id="collapsibleFirstLevel">
      <ul class="navbar-nav w-100 row mr-auto mt-2 mt-lg-0">
         <li
            class="nav-item text-center border-top border-right border-dark m-0 col h3 {{ Route::currentRouteName() === 'movie-program' ? 'active' : null }}">
            <a class="nav-link text-dark" href="{{ route('movie.all') }}">Wochenübersicht</a>
         </li>
         <li
            class="nav-item text-center border-top border-right border-dark m-0 col h3 {{ Route::currentRouteName() === 'movie.index' ? 'active' : null }}">
            <a class="nav-link text-dark" href="{{ route('movie.index') }}">Tagesprogramm</a>
         </li>
         <li
            class="nav-item text-center border-top border-right border-dark m-0 col h3 {{ Route::currentRouteName() === 'movie.preview' ? 'active' : null }}">
            <a class="nav-link text-dark" href="{{ route('movie.preview') }}">Vorschau</a>
         </li>
         @auth
         <li class="logout nav-item text-center border-top border-right border-dark m-0 col">
            <form class="" action="{{route("logout")}}" method="POST">
               @csrf
               <button class="p-0 w-100 nav-link text-dark btn btn-link" type="submit">Logout</button>
            </form>
         </li>
         @else
         <li class="nav-item text-center border-top border-right border-dark m-0 col h3">
            <a class="nav-link text-dark {{ Route::currentRouteName() ==='login' ? 'active' : null }}"
               href="{{ route('login') }}">Login</a>
         </li>
         <li class="nav-item text-center border-top border-right border-dark m-0 col h3">
            <a class="nav-link text-dark {{ Route::currentRouteName() ==='register' ? 'active' : null }}"
               href="{{ route('register') }}">Register</a>
         </li>
         @endauth
         @can("admin&employee")
         {{-- <li class="nav-item text-center border-top border-right border-dark m-0 col h3">
            <a class="nav-link text-dark {{ Route::currentRouteName() ==='calendar.index' ? 'active' : null }}"
         href="{{ route('calendar.index') }}">Calendar</a>
         </li> --}}
         @endcan
         <li class="nav-item text-center border-top border-right border-dark m-0 col h3 ">
            <a class="cart" href="{{route("cart.index")}}"><svg class="cart-icon text-dark" width="1em" height="1em"
                  viewBox="0 0 16 16" class="bi bi-cart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd"
                     d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
               </svg>
               <span id="cart-quantity">{{$cartQuantity ?? 0}}</span>
            </a>
         </li>
         @can("admin&employee")
         <li class="nav-item text-center border-top border-right border-dark m-0 col h3 dropdown">
            <a class="nav-link text-dark dropdown-toggle" href="#" id="navbarDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mitarbeiterbereich</a>
            <div class="dropdown-menu p-4" aria-labelledby="navbarDropdown">
               @can("adminOnly")
               <a class="nav-link text-dark {{ Route::currentRouteName() ==='user.index' ? 'active' : null }}"
                  href="{{ route('user.index') }}">Mitarbeiter</a>
               <a class="nav-link text-dark {{ Route::currentRouteName() ==='ticket.badTickets' ? 'active' : null }}"
                  href="{{ route('ticket.badTickets') }}">Fehlgeschlagene Käufe</a>
               @endcan
               {{-- <li class="nav-item text-center border-top border-right border-dark m-0 col h3"> --}}
               <a class="nav-link text-dark {{ Route::currentRouteName() ==='calendar.index' ? 'active' : null }}"
                  href="{{ route('calendar.index') }}">Calendar</a>
               {{-- </li> --}}
               <form action="{{route("search.find")}}" class="form-inline ml-auto" method="GET">
                  @csrf
                  <div class="input-group w-auto">
                     <input name="searchCode" type="text" class="margin-left-auto border-0 bg-light form-control"
                        placeholder="Ordercode" aria-label="Search">
                     <div class="input-group-append">
                        <button class="btn btn-light" type="submit">
                           <i class="fa fa-search"></i>
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </li>
         @endcan
   </div>
</nav>