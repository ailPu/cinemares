@extends('layouts.app')

@section('pageTitle','Erfolgreich reserviert')

@section("content")
<div class="container">

   <table class="table table-bordered table-responsive-sm text-center">
      <h1 class="text-center my-4">
         @if($user->type != "employee")Deine @endif Reservierung wurde erfolgreich abgewickelt
      </h1>
      @if($user->type != "employee")<h3 class="text-center">Wir haben dir deine Reservierungsdetails auch an
         {{$reservationData["mail"]}}
         geschickt</h3>
      @endif
      <thead class="thead-inverse">
         <tr>
            <th>Film</th>
            <th>Spielzeit</th>
            <th>Saal</th>
            <th>Reihe</th>
            <th>Sitzplatz</th>
            <th>Preis</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($reservationData["tickets"] as $ticket)
         <tr>
            <td scope="row">{{$reservationData["movieTitle"]}}</td>
            <td>{{ \Carbon\Carbon::parse($reservationData["startEvent"])->format('H:i d.m.Y')}}</td>
            <td>{{$reservationData["roomName"]}}</td>
            <td>{{$ticket->row}}</td>
            <td>{{$ticket->seat}}</td>
            <td>{{$ticket->price}}€</td>
         </tr>
         @endforeach
      </tbody>

      <tfoot class="bg-dark text-light">
         <tr>
            <td>Total:</td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td>{{$reservationData["total"]}}€</td>
         </tr>
      </tfoot>
   </table>

   <div class="my-4">@if($user->type !="employee") Dein @endif Reservierungscode:
      {{$reservationData["reservationCode"]}}</div>
</div>

@endsection