<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $fillable = ["title", "duration", "genre", "cast", "overview", "poster_path", "release_year"];

    /**
     * Get related Eventmodel Data
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany(\App\Models\Event::class);
    }

    /**
     * Get related Eventmodel Data that start in the future
     * @return mixed
     */
    public function futureEvents()
    {
        return $this->events()->withCount("tickets")->where([["start_event", ">", Carbon::now()], ["released", "=", config("constants.calendar_status_codes.released")]]);
    }

    /**
     * Get related Eventmodel Data whose end is in the future
     * @return mixed
     */
    public function futureEndEvents()
    {
        return $this->events()->withCount("tickets")->where([["end_event", ">", Carbon::now()], ["released", "=", config("constants.calendar_status_codes.released")]]);
    }

    /**
     * Get movies(/events) that are playing current week
     * @return mixed 
     */
    public function currentWeek()
    {
        return $this->events()->withCount("tickets")->whereRaw("YEARWEEK(start_event,1) = YEARWEEK(NOW(),1)")->orderBy("start_event", "asc");
    }

    /**
     * Get movies(/events) that are playing the following week
     * @return mixed 
     */
    public function nextWeek()
    {
        return $this->events()->withCount("tickets")->whereRaw("YEARWEEK(start_event,1) = YEARWEEK(NOW(),1)+1")->orderBy("start_event", "asc");
    }

    /**
     * Get movies(/events) that are playing today
     * @return mixed 
     */
    public function today()
    {
        return $this->events()->withCount("tickets")->whereRaw("DATE(start_event) = DATE(NOW())")->orderBy("start_event", "asc");
    }
}
