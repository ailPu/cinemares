<?php

namespace App\Traits;

trait GetEventData
{
   /**
    * Get current User
    * @return mixed
    */
   public function getEventData($event)
   {
      return [
         "movieTitle" => $event->movie->title,
         "roomName" => $event->room->name,
         "startEvent" => $event->start_event,
         "posterPath" => $event->movie->poster_path,
      ];
   }
}
