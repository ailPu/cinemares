// Damit man bei Refresh im selben View landet
/**
 * Store last Date and Viewtype in Session to make it available on refresh or when user changes view
 * @param {object} dateInfo 
 */
function storeCurrentViewInSession(dateInfo){
   sessionStorage.setItem("fcDefaultView", dateInfo.view.type)
   sessionStorage.setItem("fcDefaultDate", dateInfo.startStr)
}