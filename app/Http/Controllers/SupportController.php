<?php

namespace App\Http\Controllers;

use App\Mail\Invoice;
use App\Models\Ticket;
use App\Traits\CartItemCount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SupportController extends Controller
{

    use CartItemCount;

    public function __construct()
    {
        $this->middleware(["auth", "can:adminOnly"]);
    }

    /**
     * Send Mail to Customer, suggesting refund and update Tickets' Status to "in_support"
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function sendMail(Request $request)
    {
        $badTickets =  Ticket::whereIn("id", $request->badTicketIds)->get();
        if (!$badTickets[0]->receipt || !$badTickets[0]->receipt->order_mail) {
            if (request()->ajax()) return response()->json(["status" => 400, "msg" => "Es kann keine Mail versendet werden, weil keine Bestellmail vorhanden ist"]);
            else return redirect()->route("ticket.badTickets")->with("error", "Es kann keine Mail versendet werden, weil keine Bestellmail vorhanden ist");
        }

        $dataForMail = [
            "tickets" => $badTickets, "orderMail" => $badTickets[0]->receipt->order_mail, "startEvent" => $badTickets[0]->event->start_event, "movieTitle" => $badTickets[0]->event->movie->title, "roomName" => $badTickets[0]->event->room->name, "total" => $badTickets->sum("price")
        ];
        Mail::to($dataForMail["orderMail"])->send(new Invoice($dataForMail, "support"));

        foreach ($badTickets as $badTicket) $badTicket->update(["status" => config("constants.ticket_status_codes.in_support")]);

        if ($request->ajax()) return response()->json(["status" => 200, "msg" => "Mail erfolgreich versendet"]);
        return redirect()->route("ticket.badTickets")->with("success", "Mail erfolgreich versendet");
    }
}
