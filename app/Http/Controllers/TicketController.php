<?php

namespace App\Http\Controllers;

use App\Models\Row;
use App\Models\Ticket;
use App\Traits\CartItemCount;
use App\Traits\CreateOrderCode;
use App\Traits\GetUser;
use App\Traits\SelectTickets;
use App\Traits\SetDataForRedirect;
use App\Traits\UpdateTicketsTimestamp;
use Illuminate\Http\Request;

class TicketController extends Controller
{

    use CreateOrderCode, UpdateTicketsTimestamp, SetDataForRedirect, SelectTickets, GetUser, CartItemCount;

    /**
     * Store Ticket in DB
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|void
     */
    public function store(Request $request)
    {
        $request->validate([
            "row" => "exists:App\Models\Row,row",
            "seat" => "between:1,50",
            "event_id" => "exists:App\Models\Event,id"
        ]);

        $seatTaken = $this->checkIfSeatTaken($request);
        if ($seatTaken) return $seatTaken;

        $user = $this->getUser();

        $previousTicketHasDifferentEvent = $this->previousTicketHasDifferentEvent($user->queryCondition, $user->id, $request->event_id, $request);
        if ($previousTicketHasDifferentEvent) return $previousTicketHasDifferentEvent;

        $price = Row::select("price")->where("row", $request->row)->first();
        $ticket = new Ticket(["row" => $request->row, "seat" => $request->seat, "event_id" => $request->event_id,  $user->queryCondition => $user->id, "price" => $price->price, "status" => config("constants.ticket_status_codes.selected")]);
        $ticket->save();

        $this->updateTicketsTimestamp(config("constants.ticket_status_codes.selected"), $user);

        if (request()->ajax()) {
            $newTotal = Ticket::where([$user->queryCondition => $user->id, "status" => config("constants.ticket_status_codes.selected")])->sum("price");
            return response()->json(["status" => 200, "ticket" => $ticket->id, "newTotal" => $newTotal, "user" => $user->type, "msg" => "Ticket erfolgreich gestored."]);
        } else {
            return redirect()->route("event.show", $request->event_id);
        }
    }

    /**
     * Check if seat selected in View is available
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|false
     */
    public function checkIfSeatTaken(Request $request)
    {
        $ticket = Ticket::where(["seat" => $request->seat, "row" => $request->row, "event_id" => $request->event_id])->first();
        if ($ticket) {
            if (request()->ajax()) return response()->json(["status" => 400, "notAvailable" => true, "msg" => "Dieses Ticket ist leider schon vergeben."]);
            return redirect()->route("event.show", $request->event_id)->with("seatTaken", "Dieses Ticket ist leider schon vergeben.");
        } else {
            return false;
        }
    }

    /**
     * Check if User has previously selected tickets for different event
     * @param string $condition DB query condition
     * @param int $userId
     * @param int $eventId
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|false
     */
    public function previousTicketHasDifferentEvent($condition, $userId, $eventId, $request)
    {
        $previouslySelectedTicket = Ticket::where([$condition => $userId, "status" => config("constants.ticket_status_codes.selected")])->first();
        if ($previouslySelectedTicket && $previouslySelectedTicket->event_id != $eventId) {
            if (request()->ajax()) return response()->json(["status" => 400, "destroyOtherTicketsRoute" =>  route('ticket.destroyTicketsWithDifferentEvent'), "hasTicketsWithDifferentEvent" => true, "msg_title" => "Ticket mit anderem Event im Warenkorb", "msg_body" => "Es können immer nur Tickets für ein Event gebucht werden. Wollen Sie, dass ihre anderen Tickets gelöscht werden?", "btn_text" => "Andere Tickets entfernen", "event_id" => $request->event_id]);
            else return redirect()->route("event.show", $request->event_id)->with("error", "Es können immer nur Tickets für ein Event gebucht werden. Wenn Sie Ihre anderen Tickets entferne möchten, bitte hier klicken");
        } else {
            return false;
        }
    }

    /**
     * Remove Ticket with given id 
     * @param \Illuminate\Http\Request $request
     * @param int $id TicketId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        $user = $this->getUser();

        $this->destroyTicketDependingOnRequestCondition($request, $user, $id);

        if (request()->ajax()) {
            if ($request->bad_ticket) {
                $newTotal = Ticket::where("updated_at", $request->updated_at)->sum("price");
                return response()->json(["status" => 200, "datetime" => $request->updated_at, "newTotal" => $newTotal, "msg" => "Ticket erfolgreich freigeschaltet"]);
            } else if ($request->reservation_search_results) {
                $newTotal = Ticket::whereHas("reservation", function ($query) use ($request) {
                    $query->where("reservation_code", $request->code);
                })->sum("price");
                return response()->json(["status" => 200, "newTotal" => $newTotal, "code" => $request->code, "msg" => "Ticket erfolgreich gelöscht"]);
            } else {
                $newTotal = Ticket::where([$user->queryCondition => $user->id, "status" => config("constants.ticket_status_codes.selected")])->sum("price");
                return response()->json(["status" => 200, "storeRoute" => route("ticket.store"), "newTotal" => $newTotal, "code" => $request->code, "user" => $user->type, "msg" => "Ticket erfolgreich gelöscht"]);
            }
        } else {
            if (isset($request->cart)) {
                return redirect()->route("cart.index");
            } else if (isset($request->bad_ticket)) {
                return redirect()->route("tickets.badTickets")->with("success", "Ticket erfolgreich freigeschaltet");
            } else if (isset($request->reservation_search_results)) {
                return redirect()->route("search.show", ["code" => $request->code, "type" => "reservation", "condition" => "reservation_code", "status" => config("constants.ticket_status_codes.reserved")]);
            } else {
                return redirect()->route("event.show", $request->event_id);
            }
        }
    }

    /**
     * Destroy tickets that are stuck in payment for whom the customer has not paid already
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroyBadTickets(Request $request)
    {
        $badTicketsIds = $request->badTicketIds;
        Ticket::destroy($badTicketsIds);
        if ($request->ajax()) return response()->json(["status" => 200, "msg" => "Tickets erfolgreich freigeschaltet"]);
        else return redirect()->route("ticket.badTickets")->with("success", "Tickets erfolgreich freigeschaltet");
    }

    /**
     * Destroy previously selected tickets if User chose ticket for another event 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse
     */
    public function destroyTicketsWithDifferentEvent(Request $request)
    {
        $user = $this->getUser();
        Ticket::where([[$user->queryCondition, "=", $user->id], ["event_id", "<>", $request->event_id], ["status", "=", config("constants.ticket_status_codes.selected")]])->delete();
        if (request()->ajax()) {
            $newTotal = Ticket::where([$user->queryCondition => $user->id, "status" => config("constants.ticket_status_codes.selected")])->sum("price");
            return response(["status" => 200, "newTotal" => $newTotal, "msg" => "Andere Tickets wurden entfernt"]);
        } else {
            return redirect()->route("event.show", $request->event_id)->with("success", "Andere Tickets wurden entfernt.");
        }
    }

    /**
     * Helper function for destroying tickets based on Requestconditions
     * @param \Illuminate\Http\Request $request
     * @param mixed $user 
     * @param int $id
     * @return void
     */
    public function destroyTicketDependingOnRequestCondition(Request $request, $user, $id)
    {
        if (isset($request->reservation_search_results) && $user->type == "employee") {
            Ticket::destroy($id);
        } elseif (isset($request->reservation_expired) && $user->type == "employee") {
            Ticket::where(["id" => $id, "status" => config("constants.ticket_status_codes.reserved")])->delete();
        } else {
            Ticket::where(["id" => $id, $user->queryCondition => $user->id, "status" => config("constants.ticket_status_codes.selected")])->delete();
            $remainingTickets = Ticket::where([$user->queryCondition => $user->id, "status" => config("constants.ticket_status_codes.selected")])->get();
            if ($remainingTickets->count()) $this->updateTicketsTimestamp(config("constants.ticket_status_codes.selected"));
        }
    }

    /**
     * Show Tickets where error occurred during payment
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function badTickets()
    {
        $cartQuantity = $this->cartItemCount();

        $tickets = Ticket::where("status", config("constants.ticket_status_codes.in_payment"))->orWhereHas("receipt", function ($query) {
            $query->where([["order_mail", "<>", NULL], ["confirmation_mail_sent", "=", NULL], ["status", "<>", config("constants.ticket_status_codes.in_support")]]);
        })->with("receipt")->get();

        $ticketsGroupedByTimestampAndOrdermail = [];
        foreach ($tickets as $ticket) {
            $parsedTime = $ticket->updated_at->format("H:i:s d.m.Y ");
            $ticketsGroupedByTimestampAndOrdermail[$parsedTime][$ticket->receipt->order_mail ?? ""][] = $ticket;
        }
        $ticketsGroupedByTimestampAndOrdermail = collect($ticketsGroupedByTimestampAndOrdermail);

        return view("bad_ticket.index", compact("ticketsGroupedByTimestampAndOrdermail", "cartQuantity"));
    }
}
