@if (!isset($_GET["roomId"]))
@php
$_GET["roomId"] =1;
@endphp
@endif

<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   {{-- FÜR Laravel mit Ajax Calls --}}
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <link rel="stylesheet" href="{{asset('lib/main.css')}}">
   <link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
   <script src="{{asset('lib/main.js')}}"></script>
   <script src=" https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
      integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
   </script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
      integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
   </script>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

   <script src="{{ asset('js/calendar/rememberScrollPosition.js')}}"></script>
   <script src="{{ asset('js/calendar/storeCurrentViewInSession.js')}}"></script>

   {{-- To fire event when someone else wants to access calendar --}}
   <script src="{{asset('js/app.js')}}"></script>

   <title>Calendar</title>

   <script>
      $(document).ready(function() {

         // ----- Global Variables START -----

         // Necessary for keeping the user at current view and date after refresh
         var initialView = sessionStorage.getItem("fcDefaultView") || "timeGridWeek";
         var initialDate = sessionStorage.getItem("fcDefaultDate") || new Date;

         // ----- Globale Variables END -----

         // KALENDER 5 START 
         const calendar5 = new FullCalendar.Calendar($("#calendar")[0], {
            // Hours to display and to forbid dragging events outside this timerange via "constraint" property in eventSources when fetching events from DB
            businessHours: {
               startTime: '17:00:00',
               endTime: '23:59:59',
               daysOfWeek: [0,1,2,3,4,5,6],
            },
            // Mintime to display in calendar5
            slotMinTime: "17:00:00",
            // Default scrollposition when calendar5 is started
            scrollTime: "17:00:00",
            // How many weeks to display in Month view. True = always 6, otherwise depending on month
            fixedWeekCount: false,
            // Show indicator for current time
            nowIndicator: true,
            // Which view to show when opening calendar5
            initialView: initialView,
            // Which week/date to show when opening calendar5
            initialDate: initialDate,
            // Fetch events from DB
            eventSources: [{
               events: function(info, successCallback, failureCallback) {
                  fetchingNewEvents = true;
                  $(".overlay").show();
                  $.ajax({
                     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                     url: "{{route("calendar.fetchEvents")}}",
                     type: "POST",
                     data: {
                        room_id: <?= $_GET["roomId"] ?>,
                        start_event: info.startStr,
                        end_event: info.endStr,
                        permissions: false,
                     },
                     success: function(resp) {
                        $(".overlay").hide();
                        if (resp.status != 200) {
                           alert(resp.msg)
                           location.reload();
                           return;
                        }
                        const events = resp.data;
                        let unconfirmedDates = false;
                        successCallback(
                           events.map(function(eventEl, index, array) {
                              if (eventEl.released == "0") unconfirmedDates = true;
                              if (index == array.length - 1) {
                                 fetchingNewEvents = false;
                              }
                              return {
                                 title: eventEl.title,
                                 id: eventEl.id,
                                 movie_id: eventEl.movieId,
                                 start: eventEl.start,
                                 end: eventEl.end,
                                 durationEditable: false,
                                 confirmed: eventEl.released,
                                 constraint: "businessHours",
                                 editable: false,
                                 textColor: eventEl.released == "0" ? "black" : "white",
                                 borderColor: eventEl.released == "0" ? "yellow" : "green",
                                 backgroundColor: eventEl.released == "0" ? "yellow" : "green"
                              }
                           })
                        )
                     }
                  })
               },
               
            }],
            // Gets called whenever view changes
            datesSet: function(dateInfo) {
               globalDateObj = {
                  startDate: calendar5.view.activeStart,
                  endDate: calendar5.view.activeEnd,
               }
               if (dateInfo.view.type == "dayGridMonth") {
                  sessionStorage.setItem("fcDefaultView", dateInfo.view.type)
                  sessionStorage.setItem("fcDefaultDate", dateInfo.startStr)
                  if (dateInfo.start.getDate() != 1) {
                     /* 
                        If Monday is not 1. in month, refreshing causes calendar5 to jump back one month till monday is the 1.  
                        calendar5.getDate() returns  1. from month 
                     */
                     const dateObj = calendar5.getDate();
                     // Format date for calendar5 ro recognize on refresh, to avoid jumping back
                     const momentObj = moment(dateObj, 'DD.MM.YYYY').format('Y-MM-DD HH:mm:ss');
                     const modifiedMomentStr = momentObj.replace(/\s/, "T");
                     sessionStorage.setItem("fcDefaultDate", modifiedMomentStr);
                     return;
                  }
               } else {
                  storeCurrentViewInSession(dateInfo)
               }
            },
            locale: "de",
            buttonText: {
               today: 'Heute',
               month: 'Monat',
               week: 'Woche',
               day: 'Tag',
               list: 'list'
            },
            allDayText: "all-day",
            // Formats time displayed in timegrid slots 
            slotLabelFormat: {
               hour: '2-digit',
               minute: '2-digit',
               meridiem: false,
            },
            // Formats time displayed on events
            eventTimeFormat: {
               hour: '2-digit',
               minute: '2-digit',
               meridiem: false
            },
            // Slotsizes
            slotDuration: '00:15:00',
            // Configures first day to display (1 = Monday)
            firstDay: 1,
            // editable true allows dragging and resizing events
            editable: false,
             // formats headerToolbar (left, middle, right)
            headerToolbar: {
               start: "prev,next today",
               center: "title",
               end: "dayGridMonth,timeGridWeek,timeGridDay"
            },
            selectable: true,
            select: function(selectionInfo) {
               if (selectionInfo.view.type == "timeGridDay") return;
               else if (selectionInfo.view.type == "timeGridWeek"){
                  calendar5.changeView('timeGridDay', selectionInfo.startStr);
                  return;
               }
               else if (selectionInfo.view.type == "dayGridMonth") {
                  calendar5.changeView('timeGridWeek', selectionInfo.startStr);
                  rememberScrollPosition();
                  return;
               }
            },
      })
      calendar5.render();

      // Since calendar5's toolbar gets rendered via javascript, custom HTML elements that are meant to be inserted in the toolbar have to wait till calendar5 is rendered and then placed at desired position
      $("#tools").insertAfter(".fc-header-toolbar.fc-toolbar");
      $("body").css("visibility","visible")

      rememberScrollPosition();
   })
   </script>

   <style>
      body {
         visibility: hidden;
         overflow-anchor: none;
      }

      .fc .fc-toolbar.fc-header-toolbar {
         margin: 0;
      }

      #tools {
         display: flex;
         justify-content: center;
         margin: 1rem;
      }

      .container-fluid {
         margin: 0;
         width: 100%;
         padding: 1rem;
         margin-right: 80px;
      }

      #hall-name {
         text-align: center;
         margin-bottom: 1rem;
      }


      .dropdown {
         width: inherit
      }

      .dropdown a {
         width: inherit
      }

      #calendar {
         border-top: 1px solid;
         padding-top: 1rem;
      }

      .fc-direction-ltr .fc-timegrid-slot-label-frame {
         text-align: center;
      }

      .overlay {
         position: absolute;
         display: none;
         top: 0;
         left: 0;
         width: 100%;
         height: 100%;
         z-index: 10;
         background: rgba(0, 0, 0, .3);
      }

      .lds-roller {
         display: inline-block;
         position: relative;
         width: 80px;
         height: 80px;
         position: absolute;
         top: 20%;
         z-index: 10;
         left: 50%;
         transform: translateX(-50%);
      }

      .lds-roller div {
         animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
         transform-origin: 40px 40px;
      }

      .lds-roller div:after {
         content: " ";
         display: block;
         position: absolute;
         width: 7px;
         height: 7px;
         border-radius: 50%;
         background: #fff;
         margin: -4px 0 0 -4px;
      }

      .lds-roller div:nth-child(1) {
         animation-delay: -0.036s;
      }

      .lds-roller div:nth-child(1):after {
         top: 63px;
         left: 63px;
      }

      .lds-roller div:nth-child(2) {
         animation-delay: -0.072s;
      }

      .lds-roller div:nth-child(2):after {
         top: 68px;
         left: 56px;
      }

      .lds-roller div:nth-child(3) {
         animation-delay: -0.108s;
      }

      .lds-roller div:nth-child(3):after {
         top: 71px;
         left: 48px;
      }

      .lds-roller div:nth-child(4) {
         animation-delay: -0.144s;
      }

      .lds-roller div:nth-child(4):after {
         top: 72px;
         left: 40px;
      }

      .lds-roller div:nth-child(5) {
         animation-delay: -0.18s;
      }

      .lds-roller div:nth-child(5):after {
         top: 71px;
         left: 32px;
      }

      .lds-roller div:nth-child(6) {
         animation-delay: -0.216s;
      }

      .lds-roller div:nth-child(6):after {
         top: 68px;
         left: 24px;
      }

      .lds-roller div:nth-child(7) {
         animation-delay: -0.252s;
      }

      .lds-roller div:nth-child(7):after {
         top: 63px;
         left: 17px;
      }

      .lds-roller div:nth-child(8) {
         animation-delay: -0.288s;
      }

      .lds-roller div:nth-child(8):after {
         top: 56px;
         left: 12px;
      }

      @keyframes lds-roller {
         0% {
            transform: rotate(0deg);
         }

         100% {
            transform: rotate(360deg);
         }
      }
   </style>
</head>

<body>
   <div class="container-fluid">
      <div id="hall-name">
         <div id="statusInfo" class="text-center alert alert-danger">Der Kalender wird gerade bearbeitet</div>
         <div><a id="change-calendar" href="{{route("calendar.index")}}" class="btn btn-warning d-none">Zum Kalender mit
               Vollzugriff</a>
         </div>
         <h1 id="hall"><?= $roomsMap[$_GET["roomId"]]["name"] ?></h1>
      </div>
      <div id="calendar">
      </div>

      <div id="tools">
         <div class="dropdown">
            <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Saal wechseln</a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
               <?php foreach ($roomsMap as $room) : ?>
               <?php if ($room["id"] == $_GET["roomId"]) continue ?>
               <a class="dropdown-item" href="calendar?roomId=<?= $room["id"] ?>">
                  <?= $room["name"] ?></a>
               <?php endforeach ?>
            </div>
         </div>
      </div>

   </div>

   <div class="overlay">
      <div class="lds-roller">
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
      </div>
   </div>

   <script>
      Echo.private('calendar')
         .listen('LeftCalendar', (e) => {
            console.log(e);
            $("#statusInfo").text(e.message).removeClass("alert-danger").addClass("alert-success");
            $("#change-calendar").removeClass("d-none");
      }).listen('CalendarOccupied', (e)=>{
         $("#change-calendar").addClass("d-none");
         $("#statusInfo").text(e.message).removeClass("alert-success").addClass("alert-danger");
      });

   </script>

</body>

</html>