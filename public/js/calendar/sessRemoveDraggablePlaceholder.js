
/**
 * Remove all sessionData for current draggable placeholder and remove it from DOM
 */
function sessRemoveDraggablePlaceholderAndDataForDb(){
   if($("#drag").length) $("#drag").remove();
   sessionStorage.removeItem("takenSlots");
   sessionStorage.removeItem("dataForDb");
   sessionStorage.removeItem("draggablePlaceholder");
}