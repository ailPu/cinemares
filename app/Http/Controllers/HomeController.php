<?php

namespace App\Http\Controllers;

use App\Traits\GetUser;

class HomeController extends Controller
{
    use GetUser;

    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * Redirect to Employeearea or Movieoverview depending on role
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function index()
    {
        $user = $this->getUser();
        if ($user->type == "employee") return redirect()->route("employee.index");
        elseif ($user->type == "registeredUser") return redirect()->route('movie.index');
    }
}
