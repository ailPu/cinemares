<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Traits\CartItemCount;
use App\Traits\GetUser;
use Carbon\Carbon;

class EventController extends Controller
{

    use GetUser, CartItemCount;

    public function __construct()
    {
        $this->middleware("prevent-history");
    }

    /**
     * Display all playtimes for movie
     *
     * @param int $id movieId
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function show($id)
    {
        $user = $this->getUser();
        $cartQuantity = $this->cartItemCount($user);

        $eventData = Event::with("movie", "room", "room.rows", "tickets", "tickets.reservation")->where("end_event", ">", Carbon::now())->findOrFail($id);

        $pricingRows = [];
        $prevPrice = null;

        $rowNames = ["A", "B", "C",];
        $i = 0;
        foreach ($eventData->room->rows as $row) {
            if (!$prevPrice) $prevPrice = $row->price;
            if ($prevPrice != $row->price) $i++;
            $pricingRows[$row->price] = ["name" => $rowNames[$i], "price" => $row->price];
            $prevPrice = $row->price;
        }

        $tickets = $this->sortTickets($eventData->tickets, $user);
        $total = $eventData->tickets->whereIn("status", config("constants.ticket_status_codes.selected"))->where($user->queryCondition, $user->id)->sum("price");

        return view("event.show", compact("eventData", "tickets", "total", "user", "cartQuantity", "pricingRows"));
    }

    /**
     * Sort tickets for event to adapt styling in view
     * @param mixed $tickets
     * @param mixed $user
     * @return array $sortedTickets
     */
    public function sortTickets($tickets, $user)
    {
        $sortedTickets = [];
        foreach ($tickets as $ticket) {
            if ($ticket["status"] == config("constants.ticket_status_codes.in_payment") || $ticket["status"] == config("constants.ticket_status_codes.sold_at_reception") || $ticket["status"] == config("constants.ticket_status_codes.in_support") || $ticket["status"] == config("constants.ticket_status_codes.bought_online")) {
                $sortedTickets[$ticket["seat"]]["sold"] = "sold";
            } else if ($ticket["status"] == config("constants.ticket_status_codes.selected")) {
                if ($user->type == "employee" && ($ticket["employee_id"] != NULL && $ticket["employee_id"] == $user->id)) {
                    $sortedTickets[$ticket["seat"]] = $ticket->id;
                } elseif ($user->type == "registeredUser" && $ticket["user_id"] != NULL && $ticket["user_id"] == $user->id) {
                    $sortedTickets[$ticket["seat"]] = $ticket->id;
                } elseif ($ticket["guest_id"] == $user->id) {
                    $sortedTickets[$ticket["seat"]] = $ticket->id;
                } else $sortedTickets[$ticket["seat"]]["sold"] = "sold";
            } else if ($ticket["status"] == config("constants.ticket_status_codes.reserved")) {
                if ($ticket->reservation->deleted_at && $user->type == "employee") {
                    $sortedTickets[$ticket["seat"]]["expired"] = $ticket->id;
                } else {
                    $sortedTickets[$ticket["seat"]]["sold"] = "sold";
                }
            }
        }

        return $sortedTickets;
    }
}
