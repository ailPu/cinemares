<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = ["role_id", "user_id", "firstname", "lastname"];

    /** Get related Usermodel Data
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, "user_id", "id");
    }

    /** Check if current User has Admin Privileges
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->role()->where("name", "admin");
    }

    /** Get related Rolemodel Data
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(\App\Models\Role::class, "role_id", "id");
    }

    /** Check if current User has given role
     * @param mixed $role
     * @return bool
     */
    public function hasRole($role)
    {
        if ($this->role()->where("name", $role)->first()) return true;
        return false;
    }

    /** Check if current User has any of given roles
     * @param array $roles
     * @return bool
     */
    public function hasAnyRoles(array $roles)
    {
        if ($this->role()->whereIn("name", $roles)->first()) return true;
        return false;
    }
}
