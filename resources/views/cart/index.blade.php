@extends('layouts.app')

@section('pageTitle','Warenkorb')

@section('content')

<div id="reservationFailureUrl" class="d-none">{{$reservationFailureUrl ?? ''}}</div>
<div id="reservationSuccessUrl" class="d-none">{{$reservationSuccessUrl ?? ''}}</div>

@push("scripts")
<script src="{{ asset('js/tickets/deleteFromCart.js')}}"></script>
<script src="{{ asset('js/tickets/reserve.js')}}"></script>
<script src="{{ asset('js/tickets/buy.js')}}"></script>
@endpush
<div class="container">

   <div class="container-fluid">
      <table class="table table-responsive-sm table-bordered text-center">
         <h1 class="text-center my-4">Warenkorb</h1>
         <noscript>
            <div class="alert alert-warning text-center">Um Online bezahlen zu können bitte Javascript aktivieren</div>
         </noscript>
         @if(session("error"))
         <div class="text-center alert alert-warning">{{session("error")}}</div>
         @endif
         <thead class="thead-inverse">
            <tr>
               <th>Film</th>
               <th>Spielzeit</th>
               <th>Saal</th>
               <th>Reihe</th>
               <th>Sitzplatz</th>
               <th>Preis</th>
               <th>Entfernen</th>
            </tr>
         </thead>
         <tbody class="cart">
            @foreach ($tickets as $ticket)
            <tr>
               <td scope="row">{{$eventData["movieTitle"]}}</td>
               <td>{{ \Carbon\Carbon::parse($eventData["startEvent"])->format('H:i | d.m.Y')}}</td>
               <td>{{$eventData["roomName"]}}</td>
               <td>{{$ticket->row}}</td>
               <td>{{$ticket->seat}}</td>
               <td>{{$ticket->price}}€</td>
               <td>
                  <form class="formDelete cart" action="{{route("ticket.destroy",$ticket->id)}}" method="POST">
                     @csrf
                     @method("delete")
                     <input type="hidden" name="cart" value="cart">
                     <button class="btn btn-danger">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="remove bi bi-x" fill="currentColor"
                           xmlns="http://www.w3.org/2000/svg">
                           <path fill-rule="evenodd"
                              d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                     </button>
                  </form>
               </td>
            </tr>
            @endforeach
         </tbody>

         <tfoot class="bg-dark text-light">
            <tr>
               <td>Total:</td>
               <td class="border-0"></td>
               <td class="border-0"></td>
               <td class="border-0"></td>
               <td class="border-0"></td>
               <td><span id="total">@if(isset($total)){{$total}}@else 0 @endif</span>€</td>
               <td class="border-0"></td>
            </tr>
         </tfoot>
      </table>

      @if(!isset($total))
      <p class="no-tickets">Es befinden sich keine Tickets im Warenkorb</p>
      @else

      <div class="order-options d-flex justify-content-center">
         @if($user->type != "employee")
         <form id="stripe-form" action="{{route("charge.online")}}" method="post">
            @csrf
            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
               data-key="{{config("stripe.PUBLISHABLE_KEY")}}"
               data-image="https://image.tmdb.org/t/p/w200{{$eventData["posterPath"]}}"
               data-description="Ticket/s für {{$eventData["movieTitle"]}}" data-amount="{{$total*100}}"
               data-locale="auto">
            </script>
         </form>
         @elseif($user->type == "employee")
         <form class="sell-cash form" action="{{route("charge.atReceptionWithCash")}}" method="POST">
            @csrf
            @method("put")
            <button class="btn btn-success mr-2">Barzahlung</button>
         </form>
         <a href="" class="btn btn-success mr-2">Bankomat</a>
         @endif
         <form class="formReserve" action="{{route("reservation.reserve")}}" method="POST">
            @csrf
            @method("put")
            @if($tooLateForReservation)
            <input type="hidden" name="too_late_for_reservation" value="too_late_for_reservation">
            @endif
            <button class="reserve btn btn-warning" @if($tooLateForReservation ||$user->type == 'guest')
               @endif>
               Reservieren</button>
         </form>
      </div>
      @endif
      <div class="text-center my-2">
         <a href="{{route("movie.index")}}" class="redirect-home btn btn-primary">Zurück zur Filmübersicht</a>
      </div>


      @if($user->type != "employee")
      <div class="text-center text-danger">
         <h3>Stripe-Demo-Version</h3>
         <p>Bei "Kreditkartennummer": 4242424242424242 eintragen</p>
         <p>Bei "MM/JJ": 07/70 eintragen</p>
         <p>Bei "CVC": 123 eintragen </p>
      </div>
      @endif
   </div>
</div>

<div class="modal" data-toggle="modal" id="modalReservation" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modalReservationBody text-center p-3">
            <p id="modalReservationText"></p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
         </div>
      </div>
   </div>
</div>

<script>
   $(".stripe-button-el span").text("Jetzt Zahlen");
</script>

@endsection