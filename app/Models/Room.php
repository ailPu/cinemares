<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected $fillable = ["name"];

    /**
     * Get related Eventmodel Data
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function event()
    {
        return $this->hasMany(\App\Models\Event::class, "room_id", "id");
    }

    /**
     * Get related Rowmodel Data
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rows()
    {
        return $this->hasMany(\App\Models\Row::class, "room_id", "id");
    }

    /**
     * Get last_seat_num for room
     * @return mixed
     */
    public function maxSeats()
    {
        return $this->hasOne(\App\Models\Row::class, "room_id", "id")->select("last_seat_num", "room_id")->orderBy("last_seat_num", "desc");
    }
}
