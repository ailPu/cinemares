@extends('layouts.app')

@section('pageTitle','Programm')

@section('content')
<div>
   <h1 class="text-center pt-4">Diese Woche im Kino</h1>
</div>

<div class="movies container d-flex justify-content-center flex-wrap">
</div>

<script>
   const url = "{{route("movie.all")}}";
   var newData = true;
   var fetchingData = false;
   var page = 1;
   loadContent(page);

   $(window).scroll(()=>{
      if($(window).scrollTop() + $(window).height() >= $(document).height() -20){
         if(newData && !fetchingData){
               page++;
               loadContent(page);
         }
      } 
   });

   function loadContent(page){
      fetchingData = !fetchingData;
      $('.overlay').show();
      $.ajax({
         url: url + "?page=" + page,
         type: "get",
         datatype: "html",
         success:(pageData)=>{
            const movies = pageData.data;
            $('.overlay').hide();

            if(!movies.length) {
                  newData = false;
                  return;
                  }

            let moviesMap = movies.map((movie) => {
               let route = '{{route('movie.show',':movie.id')}}'
               console.log("MOVIE ID",movie.id);
               route = route.replace(':movie.id',movie.id);
               return `<div class='col-12 row p-5'>
                     <div class='col-sm-4 col-md-3'>
                           <a href=${route}>
                              <img src='https://image.tmdb.org/t/p/original/${movie.poster_path}' class='img-fluid' alt=''>
                           </a>
                     </div>
                     <div class='col-sm col-md-9'>
                           <div class='col-sm col-md-9'>
                              <h2>${movie.title}</h2>
                              <div class='col'>
                              <div class='d-none d-lg-block'>
                                 <div class='movie-duration row flex-nowrap'>
                                       <p class='col-md-5 col-lg-4'><strong>Dauer:</strong></p>
                                       <p>${movie.duration} Minuten</p>
                                 </div>
                              </div>
                              </div>
                              <div class='col d-none d-md-block'>
                              <p>substrWords(movie.overview,100)</p>
                              <a class='btn border border-dark btn-light' href=${route}>Mehr lesen</a>
                              </div>
                           </div>
                     </div>
                  </div>`
               }).join("");

            $(".movies").append(moviesMap);
            fetchingData = !fetchingData;

         if($(window).height() == $(document).height()){
               page++;
               loadContent(page);
         }
         },
         failure:(err) => console.log(err),
      })
   };


</script>

@endsection