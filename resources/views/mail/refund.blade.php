<!DOCTYPE html>
<html>

<head>
   <title>Cinemares.com</title>
</head>

<body>
   <h1 style="text-align: center">Hallo {{$orderMail}}</h1>
   <h3>Wir haben festgestellt, dass eine Abbuchung stattgefunden hat, du aber keinerlei Bestätigungen von uns
      bekommen hast.
   </h3>
   <table style="border-collapse: collapse; border: 1px solid black; width: 100%">
      <thead style="text-align: left">
         <tr style="border: 1px solid black">
            <th style="border: 1px solid black">Film</th>
            <th style="border: 1px solid black">Spielzeit</th>
            <th style="border: 1px solid black">Saal</th>
            <th style="border: 1px solid black">Reihe</th>
            <th style="border: 1px solid black">Sitzplatz</th>
            <th style="border: 1px solid black">Preis</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($tickets as $ticket)
         <tr style="border: 1px solid black">
            <td style="border: 1px solid black" scope="row">{{$movieTitle}}</td>
            <td style="border: 1px solid black">{{ \Carbon\Carbon::parse($startEvent)->format('H:i d.m.Y')}}</td>
            <td style="border: 1px solid black">{{$roomName}}</td>
            <td style="border: 1px solid black">{{$ticket->row}}</td>
            <td style="border: 1px solid black">{{$ticket->seat}}</td>
            <td style="border: 1px solid black">{{$ticket->price}}€</td>
         </tr>
         @endforeach
      </tbody>
      <tfoot style="background:lightgray; color: black">
         <tr style="border: 1px solid black">
            <td style="border: 1px solid black">Total:</td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td style="border: 1px solid black">{{$total}}€</td>
         </tr>
      </tfoot>
   </table>
   <p>Wir erstatten dir den Betrag umgehend, sobald du uns auf diese Mail antwortest.</p>
   <p>Wir entschuldigen uns für die Unannehmlichkeiten, dein Cinemares-Team</p>
</body>

</html>