<?php if (!isset($_GET["roomId"])) $_GET["roomId"] = 1; ?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <link rel="stylesheet" href="{{asset('lib/main.css')}}">
   <link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
   <script src="{{asset('lib/main.js')}}"></script>
   <script src=" https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
      integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
   </script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
      integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
   </script>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

   <script src="{{ asset('js/calendar/rememberScrollPosition.js')}}"></script>
   <script src="{{ asset('js/calendar/sessDrawDraggablePlaceholder.js')}}"></script>
   <script src="{{ asset('js/calendar/checkForPausesAndCollisions.js')}}"></script>
   <script src="{{ asset('js/calendar/formatReleaseRangeForDb.js')}}"></script>
   <script src="{{ asset('js/calendar/storeCurrentViewInSession.js')}}"></script>
   <script src="{{ asset('js/calendar/calculateEndTime.js')}}"></script>
   <script src="{{ asset('js/calendar/checkForUnreleasedEventsInCurrentView.js')}}"></script>
   <script src="{{ asset('js/calendar/removeTemporaryCloneFromCalendar.js')}}"></script>
   <script src="{{ asset('js/calendar/sessRemoveDraggablePlaceholder.js')}}"></script>
   <script src="{{ asset('js/calendar/sessRemoveModal.js')}}"></script>
   <script src="{{ asset('js/calendar/formatDateForDb.js')}}"></script>
   <script src="{{ asset('js/calendar/formatDataForDb.js')}}"></script>
   <script src="{{ asset('js/calendar/confirmDate.js')}}"></script>
   <script src="{{ asset('js/calendar/delayAjax.js')}}"></script>

   <script src="{{asset('js/calendar/listenForLogout.js')}}"></script>
   {{-- To fire event when someone else wants to access calendar --}}
   <script src="{{asset('js/app.js')}}"></script>

   <title>Calendar</title>

   <script>
      $(document).ready(function() {
         
         // ----- Global Variables START -----

         // Global Eventobject. If refreshed, fetch eventdata from sessionstorage. Also necessary for pause- and collisioncheck
         var globalEventObj = sessionStorage.getItem("dataForDb") ? JSON.parse(sessionStorage.getItem("dataForDb")) : null;
         // Global Dateobject from Calendar5. Necessary to only release events visible in current Viewrange
         var globalDateObj;
         // Global Loadingstadiumobject. Necessary for datesset function to hide buttons when changing from month- to weekview and vice versa
         var globalFetchingNewEvents;
         // Necessary for keeping the user at current view and date after refresh
         var globalInitialView = sessionStorage.getItem("fcDefaultView") || "timeGridWeek";
         var globalInitialDate = sessionStorage.getItem("fcDefaultDate") || new Date;

         // ----- Globale Variables END -----
         
         // Fetch copycheckbox value from Session. * 1, because in session there are only strings so "0" evaluate to true 
         $("#copy").prop("checked",sessionStorage.getItem("checkBoxValue") * 1 ? true : false);
         
         // Calendar 5 START 
         const calendar5 = new FullCalendar.Calendar($("#calendar")[0], {
            // Hours to display and to forbid dragging events outside this timerange via "constraint" property in eventSources when fetching events from DB
            businessHours: {
               startTime: '17:00:00',
               endTime: '23:59:59',
               daysOfWeek: [0, 1, 2, 3, 4, 5, 6],
            },
            // Mintime to display in calendar5
            slotMinTime: "17:00:00",
            // Default scrollposition when calendar5 is started
            scrollTime: "17:00:00",
            // How many weeks to display in Month view. True = always 6, otherwise depending on month
            fixedWeekCount: false,
            // Show indicator for current time
            nowIndicator: true,
            // Which view to show when opening calendar5
            initialView: globalInitialView,
            // Which week/date to show when opening calendar5
            initialDate: globalInitialDate, 
            locale: "de",
            buttonText: {
               today: 'Heute',
               month: 'Monat',
               week: 'Woche',
               day: 'Tag',
               list: 'list'
            },
            allDayText: "all-day",
            // Formats time displayed in timegrid slots 
            slotLabelFormat: {
               hour: '2-digit',
               minute: '2-digit',
               meridiem: false,
            },
            // Formats time displayed on events
            eventTimeFormat: {
               hour: '2-digit',
               minute: '2-digit',
               meridiem: false
            },
            // Slotsizes
            slotDuration: '00:15:00',
            // Configures first day to display (1 = Monday)
            firstDay: 1,
            // editable true allows dragging and resizing events
            editable: true,
            // formats headerToolbar (left, middle, right)
            headerToolbar: {
               start: "prev,next today",
               center: "title",
               end: "dayGridMonth,timeGridWeek,timeGridDay"
            },
            // Fetch events from DB
            eventSources: [{
               // Dynamically fetches events when not already loaded (ex. Start in monthview fetches all events for this month, so changing to week doesn't fire function again)
               events: function(info, successCallback, failureCallback) {
                  globalFetchingNewEvents = true;
                  $(".overlay").show();
                  $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                     url: "{{route("calendar.fetchEvents")}}",
                     type: "POST",
                     data: {
                        room_id: <?= $_GET["roomId"] ?>,
                        start_event: info.startStr,
                        end_event: info.endStr,
                        // Necessary property for show_with_tools.blade.php to tell Controller to check if user still has access to calendar with tools or not (ex. logout because of timeout)
                        permissions: true,
                     },
                     success: function(resp) {
                        $(".overlay").hide();
                        if (resp.status != 200) {
                           alert(resp.msg)
                           sessRemoveDraggablePlaceholderAndDataForDb();
                           location.reload();
                           return;
                        }
                        const events = resp.data;
                        if (!events.length) {
                           $("#release-button").attr("disabled",true);
                        }
                        let unconfirmedDates = false;
                        $("#release-button").attr("disabled",true);

                        // Set all properties for DB Events
                        successCallback(
                           events.map(function(eventEl, index, array) {
                              if (eventEl.released == "0"){
                                 if(Date.parse(eventEl.start) > Date.now()) unconfirmedDates = true;
                              }
                              if (index == array.length - 1) {
                                 if (unconfirmedDates) $("#release-button").attr("disabled",false);
                                 globalFetchingNewEvents = false;
                              }
                              return {
                                 title: eventEl.title,
                                 id: eventEl.id,
                                 movie_id: eventEl.movieId,
                                 start: eventEl.start,
                                 end: eventEl.end,
                                 durationEditable: false,
                                 released: eventEl.released,
                                 constraint: "businessHours",
                                 // If event has been released but copycheckbox is checked, make events copyable too 
                                 editable: eventEl.released ? $("#copy:checked").length ? true : false : true, 
                                 textColor: eventEl.released == "0" ? "black" : "white",
                                 borderColor: eventEl.released == "0" ? "yellow" : "green",
                                 backgroundColor: eventEl.released == "0" ? "yellow" : "green"
                              }
                           })
                        )
                     }
                  })
               },
            }],
            // Gets called whenever view changes
            datesSet: function(dateInfo) {
               globalDateObj = {
                  start: calendar5.view.activeStart,
                  end: calendar5.view.activeEnd,
               }

               if (dateInfo.view.type == "dayGridMonth") {
                  $("#add-event").hide();
                  $("#copy-container").hide();
                  storeCurrentViewInSession(dateInfo)
                  if (dateInfo.start.getDate() != 1) {
                     /* 
                        If Monday is not 1. in month, refreshing causes calendar5 to jump back one month till monday is the 1.  
                        calendar5.getDate() returns  1. from month 
                     */
                     const dateObj = calendar5.getDate();
                     // Format date for calendar5 ro recognize on refresh, to avoid jumping back
                     const momentObj = moment(dateObj, 'DD.MM.YYYY').format('Y-MM-DD HH:mm:ss');
                     const modifiedMomentStr = momentObj.replace(/\s/, "T");
                     sessionStorage.setItem("fcDefaultDate", modifiedMomentStr);
                  }
               } else {
                  $("#add-event").show();
                  $("#copy-container").show();
                  if(sessionStorage.getItem("dataForDb")){ 
                     $(".clipboard").show();
                     $("#movie-to-insert").html(JSON.parse(sessionStorage.getItem("dataForDb")).title);
                  }
                  else $(".clipboard").hide();

                  if(sessionStorage.getItem("addEventModal")){
                     $("#add-event-modal-container").show()
                     $("#search-in-external-db").val(sessionStorage.getItem("modalInput"))
                  }
                  storeCurrentViewInSession(dateInfo)
               }
               // Since eventSources function which fetches events is an async task, checkForUnreleasedEventsInCurrentView would get an empty array to loop
               if (globalFetchingNewEvents) return;
               /* Since when calendar5 starts in monthview and all events for this timeperiod are fetched, buttons would not get hidden/disabled when changing to a view where there are no events to release.
               (ex. Starting in monthview fetches all events for this timeperiod and loops through all events. If there are events that have not been released yet, enable Releasebutton. When changing to to a view where there are either no events or no events that haven't been released, releasebutton would not get disabled because eventSources would not fire again, which is necessary for checking if there are events to release or not) .
               So if view has changed and globalFetchingNewEvents is false, loop through all events previously fetched and check if there are unreleased events in new view to disable/hide buttons 
               */
               checkForUnreleasedEventsInCurrentView(calendar5.getEvents(),calendar5.view.activeStart, calendar5.view.activeEnd)
            },

            // Enables dragNdrop and selecting Timeslot
            selectable: true,
            select: function(selectionInfo) {
               if (selectionInfo.view.type == "dayGridMonth") {
                  calendar5.changeView('timeGridWeek', selectionInfo.startStr);
                  // When changing from monthview to weekview reinitialize rememberscrollPosition
                  rememberScrollPosition();
                  return;
               }
               else if (selectionInfo.view.type == "timeGridWeek" && !globalEventObj) {
                  calendar5.changeView('timeGridDay', selectionInfo.startStr);
                  // When changing from weekview to dayview reinitialize rememberscrollPosition
                  rememberScrollPosition();
                  return;
               }
               else if(selectionInfo.view.type == "timeGridDay") return;
               else if (Date.now() > selectionInfo.start) {
                  alert("Es können keine Filme in der Vergangenheit abgelegt/hinzugefügt werden! Bitte anderen Zeitslot auswählen");
                  return;
               }

               const globalEventObjWithCalculatedEndTime = calculateEndTime(selectionInfo,globalEventObj);
               const warnings = checkForPausesAndCollisions(globalEventObjWithCalculatedEndTime, calendar5.getEvents())
               if(warnings.length && !confirmDate(warnings)) return; 

               const formattedDateRangeForDb = formatDate(globalEventObjWithCalculatedEndTime)

               $(".overlay").show();
               // Insert event only or new movie AND event in DB. Depending on "add" property (either "newMovieAndEvent" or "eventOnly")
               if(globalEventObj.add == "newMovieAndEvent") {
                  $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                     url: "{{route("calendar.insertNewMovieAndEvent")}}",
                     type: "POST",
                     data: {
                        movie_id: globalEventObj.movieId,
                        title: globalEventObj.title,
                        duration: globalEventObj.duration,
                        genre: globalEventObj.genre,
                        cast: globalEventObj.cast,
                        overview: globalEventObj.overview,
                        room_id: <?= $_GET["roomId"] ?>,
                        start_event: formattedDateRangeForDb.start,
                        end_event: formattedDateRangeForDb.end,
                        poster_path: globalEventObj.posterPath,
                        release_year: globalEventObj.releaseYear
                     },
                     success: function(resp) {
                        $(".overlay").hide();
                        if (resp.status != 200) {
                           alert(resp.msg);
                           sessRemoveDraggablePlaceholderAndDataForDb();
                           location.reload();
                           return;
                        }
                        // After a new movie has been inserted in DB, change "add" property to "eventOnly" in globalEventObj to prevent readding new movie entries in DB with same data. 
                        else {
                           globalEventObj = {
                              title: globalEventObj.title,
                              movieId: resp.movie_id,
                              add: "eventOnly",
                              duration: globalEventObj.duration
                           }
                           sessionStorage.setItem("dataForDb", JSON.stringify(globalEventObj))
                           calendar5.refetchEvents();
                        }
                     }
                  })
               }
               else if (globalEventObj.add == "eventOnly") {
                  $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                     url: "{{route("calendar.insertNewEvent")}}",
                     type: "POST",
                     data: {
                        movie_id: globalEventObj.movieId,
                        room_id: <?= $_GET["roomId"] ?>,
                        start_event: formattedDateRangeForDb.start,
                        end_event: formattedDateRangeForDb.end,
                     },
                     success: function(resp) {
                        $(".overlay").hide();
                        if (resp.status != 200) {
                           alert(resp.msg);
                           sessRemoveDraggablePlaceholderAndDataForDb();
                           location.reload();
                           return;
                        }
                        else {
                           sessionStorage.setItem("dataForDb", JSON.stringify(globalEventObj))
                           calendar5.refetchEvents();
                        }
                     },
               });
            };
         },  
            //! Gets callend when element gets droppen on available slot
            eventDrop: function(event) {
               if (Date.now() > event.event.start) {
                  alert("Es können keine Filme in der Vergangenheit hinzugefügt werden!");
                  calendar5.refetchEvents();
                  removeTemporaryCloneFromCalendar(calendar5)
                  return;
               }

               const warnings = checkForPausesAndCollisions(event.event, calendar5.getEvents())
               if(warnings.length && !confirmDate(warnings)){
                  calendar5.refetchEvents();
                  removeTemporaryCloneFromCalendar(calendar5)
                  return;
               } 

               const formattedDateRangeForDb = formatDate(event.event)

               $(".overlay").show();
               /*
                  If an event was copied, the globalEventObj get the original events' movieId
                  If globalEventObj is not null, insert a new event, otherwise an existing event was dragged and gets updated
                */
               if (globalEventObj) {
                  $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                     url: "{{route("calendar.insertNewEvent")}}",
                     type: "POST",
                     data: {
                        add: "eventOnly",
                        movie_id: globalEventObj.movieId,
                        room_id: <?= $_GET["roomId"] ?>,
                        start_event: formattedDateRangeForDb.start,
                        end_event:formattedDateRangeForDb.end,
                     },
                     success: function(resp) {
                        $(".overlay").hide();
                        if (resp.status != 200) {
                           alert(resp.msg);
                           sessRemoveDraggablePlaceholderAndDataForDb();
                           location.reload();
                           return;
                        }
                        calendar5.refetchEvents();
                        removeTemporaryCloneFromCalendar(calendar5)
                        // globalEventObj has to be unset because it holds data from the copied event, that are only valid for this action
                        globalEventObj = null;
                     }
                  })
               }
               else {
                  $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                     url: "{{route("calendar.update")}}",
                     type: "PUT",
                     data: {
                        event_id:event.event.id,
                        start_event:formattedDateRangeForDb.start,
                        end_event:formattedDateRangeForDb.end
                     },
                     success: function(resp) {
                        $(".overlay").hide();
                        if (resp.status != 200) {
                           alert(resp.msg);
                           sessRemoveDraggablePlaceholderAndDataForDb();
                           location.reload();
                           return;
                        }
                        calendar5.refetchEvents();
                     }
                  })
               }
            },

            //! Allows clicking on an element (to remove event)
            eventClick: function(event) {
               removeTemporaryCloneFromCalendar(calendar5);
               if(event.view.type == "dayGridMonth"){ 
                  calendar5.changeView("timeGridWeek")
                  return;
               }
               // If an element has already been released, dont allow removal
               else if (!!parseInt(event.event.extendedProps.released)) return;
               else if (confirm("Film wirklich entfernen?")) {
                  const eventId = event.event.id
                  $(".overlay").show();
                  $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                     url: "{{route("calendar.destroy")}}",
                     type: "DELETE",
                     data: {
                        event_id:eventId,
                     },
                     success: function(resp) {
                        $(".overlay").hide();
                        if(resp.status != 200){
                           alert(resp.msg);
                           sessRemoveDraggablePlaceholderAndDataForDb();
                           location.reload();
                           return;
                        }
                        calendar5.refetchEvents();
                     }
                  })
               }
            },

            // To forbid dragging or resizing events on taken slots when return value = false
            eventOverlap: function(stillEvent, movingEvent) { return false; },
            // To forbid clicking on timeslots that are already taken if value = false
            selectOverlap: function(event) {
               // To allow user clicking on events in month view to get to weekview
               if (calendar5.view.type === "dayGridMonth") return true;
            },

            eventDragStart: function(info) {
               // To forbid dragging events in monthView
               if (info.view.type == "dayGridMonth") return;

               globalEventObj = null
               $(".clipboard").hide();
               sessRemoveDraggablePlaceholderAndDataForDb();
               removeTemporaryCloneFromCalendar(calendar5);

               if ($("#copy").prop("checked")) {
                  const title = info.event.title;
                  // extendedProps are manually assigned during eventSources function (fetching)
                  const movieId = info.event["_def"].extendedProps.movie_id;
                  const formattedDateRangeForDb = formatDate(info.event);
                  globalEventObj = {
                     title,
                     movieId,
                     start: formattedDateRangeForDb.start,
                     end: formattedDateRangeForDb.end,
                  }
                  // Insert temporary clone in calendar 5
                  calendar5.addEvent(globalEventObj)
               }
            }
         });
         calendar5.render();
         // KALENDER 5 END
         
         // Since calendar5's toolbar gets rendered via javascript, custom HTML elements that are meant to be inserted in the toolbar have to wait till calendar5 is rendered and then placed at desired position
         $(".custom-header-buttons").insertAfter(".fc-header-toolbar.fc-toolbar");
         // To avoid visible content jumping  
         $("body").css("visibility","visible")


         // ---- START Eventlisteners ----

         // Make released events copyable
         $("#copy").click(function(){
            removeTemporaryCloneFromCalendar(calendar5);
            sessRemoveDraggablePlaceholderAndDataForDb();
            globalEventObj = null;
            // During refetch, events editable property is set depending on released status AND copycheckbox value
            calendar5.refetchEvents();
            $(".clipboard").hide();
            sessionStorage.setItem("checkBoxValue", $("#copy:checked").length);
         })

         // Clear clipboard
         $(".clipboard #clear-clipboard").click(function(e){
            sessRemoveDraggablePlaceholderAndDataForDb();
            globalEventObj = null;
            // Damit Eventlistener für Draggen entfernt wird
            document.onmousemove = null;
            $(".clipboard").hide();
         })

         // Release events
         $("#release-button").click(function() {
            removeTemporaryCloneFromCalendar(calendar5);
            const formattedReleaseRangeForDb = formatReleaseRangeForDb(globalDateObj.end);
            $(".overlay").show();
            $.ajax({
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               method: "PUT",
               url: "{{route("calendar.release")}}",
               data: {
                  room_id: <?= $_GET["roomId"] ?>,
                  start_event:formattedReleaseRangeForDb.start,
                  end_event: formattedReleaseRangeForDb.end,
               },
               success: function(resp) {
                  $(".overlay").hide();
                  if (resp.status != 200) {
                     alert(resp.msg);
                     sessRemoveDraggablePlaceholderAndDataForDb();
                     location.reload();
                     return;
                  }
                  calendar5.refetchEvents();
               }
            })
         })

         // Open Modal
         $("#add-event").click(function(e){
            removeTemporaryCloneFromCalendar(calendar5);
            $("#add-event-modal-container").show();
            sessionStorage.setItem("addEventModal", $("#add-event-modal-container").css("display"))
         })

         // Clear found results
         $("#add-event-form").click(function(e){
            $("#external-db-results").html("");
         })

         // Close modal
         $(document).on("click", (e) => {
            if(e.target == $("#add-event-modal-container")[0]) {
               $("#add-event-modal-container").hide();
               $("#search-in-external-db").html("");
               $("#external-db-results").html("");
               sessRemoveModal();}
            })

         // Draw Placeholder for new event when selected movie from searchResults
         $("#btn-time-slot").click(function(e) {
            event.preventDefault();
            sessRemoveDraggablePlaceholderAndDataForDb();

            // Get takenslots for drawing draggablePlaceholder 
            const takenSlots = (globalEventObj.duration % 15) <= 8 ? Math.floor(globalEventObj.duration / 15) : Math.ceil(globalEventObj.duration / 15)

            // Build draggablePlaceholder DIV with current slotWidhts and height
            const jQueryPlaceholderEl = $("<div></div>").css({
               "width":$(".fc-scrollgrid-sync-inner").width() + "px", 
               "height":$(".fc-timegrid-slot.fc-timegrid-slot-lane").outerHeight() * takenSlots + "px", 
               "left": e.pageX - $(".fc-scrollgrid-sync-inner").width() / 2 + "px",
               "top": e.pageY - 10 + "px"})
               .attr("id","drag")
               .attr("takenslots", takenSlots)
               .text(globalEventObj.title);

            /* 
               Stringify adds backslash in front of doublequotes, which causes problems when inserting placeholderEl in DOM.
               To keep doublequotes for inline style properties, replace any occurence of \" with "
            */
            sessionStorage.setItem("draggablePlaceholder", JSON.stringify(jQueryPlaceholderEl[0].outerHTML).replace(/\\"/g, '"'));

            // draw draggablePlaceholder to DOM 
            sessDrawDraggablePlaceholder();

            $("#add-event-modal-container").hide();
            $("#search-in-external-db").html("");
            $("#external-db-results").html("");
            /* 
            To get movieData in clipboard after refreshing and when clicking on Timeslot while holding draggablePlaceholder to have data available for inserting in DB 
            */ 
            sessionStorage.setItem("dataForDb", JSON.stringify(globalEventObj))
            $(".clipboard").show();
            $("#movie-to-insert").html(globalEventObj.title)
            sessRemoveModal();
         })

         /*
            Search for movies in external DB and delay ajax call till timeout to prevent slow curl 
          */
         $("#search-in-external-db").on("input", delayAjax(() =>{
               sessionStorage.setItem("modalInput", $("#search-in-external-db").val())
               if (!$("#search-in-external-db").val().length) {
                  $("#external-db-results").html("");
                  return;
               }
               const searchQuery = $("#search-in-external-db").val();
               $.ajax({
                  url: "{{route("calendar.searchResults")}}",
                  data: {
                     searchQuery,
                  },
                  type: "GET", 
                  success: function(resp) {
                     console.log("RESULTS",resp);
                     const apiResults = resp.data.results;
                     let lis = ``;
                     for (const movie of apiResults) {
                        lis += `<li id="${movie.id}" >${movie.title}</li>`;
                     }
                     $("#external-db-results").html(lis);
                     if (!!!resp) console.log("KEINE ERGEBNISSE GEFUNDEN");

                  }
               });
            },500)
         )

         /* Fetch data for own DB 
            After clicking on a result from search in external db, check if clicked movie is already in own db or not to decide whether movie AND event or event only has to be inserted in DB  
         */
         $("#external-db-results").click(function(e) {
            $(".overlay").show();
            sessionStorage.setItem("modalInput", $("#search-in-external-db").val())
            const movieId = e.target.id;
            $("#external-db-results").html("");
            $.ajax({
               url: "{{route("calendar.getSearchResultData")}}",
               data: {
                  movieId,
               },
               type: "GET",
               success: function(resp) {
                  const movieFromExternalDb = resp.movie;
                  $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                     url: "{{route("calendar.check")}}",
                     type: "POST",
                     data: {
                        title: movieFromExternalDb.title,
                     },
                     success: function(resp) {
                        sessionStorage.setItem("checkBoxValue", false);
                        $("#copy").prop("checked",false)
                        $(".overlay").hide();
                        $("#btn-time-slot").prop("disabled",false);

                        if (resp.status != 200) {
                           alert(resp.msg);
                           sessRemoveDraggablePlaceholderAndDataForDb();
                           location.reload();
                           return;
                        }
                        else if (resp.movie) {
                           console.log("Adding event only");
                           const movie = JSON.parse(resp.movie);
                           globalEventObj = {
                              add: "eventOnly",
                              movieId: movie.id,
                              title: movie.title,
                              duration: movie.duration,
                           }
                        } else {
                           console.log("Adding new movie and event");
                           if (!movieFromExternalDb.runtime || movieFromExternalDb.runtime <= 8) {
                              alert("Film hat keine Länge. Dieser Film kann nicht hinzugefügt werden");
                              sessRemoveDraggablePlaceholderAndDataForDb();
                              return;
                           }

                           const dataFormatedForDb = formatDataForDb(movieFromExternalDb);

                           globalEventObj = {
                              add: "newMovieAndEvent",
                              title: movieFromExternalDb.title,
                              duration: movieFromExternalDb.runtime,
                              overview: movieFromExternalDb.overview,
                              posterPath: movieFromExternalDb["poster_path"],
                              ...dataFormatedForDb
                           }
                        }
                     }
                  })
               }
            })
         })

         // If there is a draggablePlacholder, save all its last values into session to redraw after refresh 
         $(window).on("unload",function() {
            if ($("#drag").length) {
               sessionStorage.setItem("draggablePlaceholder",JSON.stringify($("#drag")[0].outerHTML).replace(/\\"/g, '"'));
            }
         })

         // ---- END Eventlisteners ----

         sessDrawDraggablePlaceholder()

         rememberScrollPosition();

         // To start listening for logout actions i.e. closing browser/tab or using browser navigation buttons
         listenForLogout('{{route("calendar.logout")}}');
         
         // Channel for Laravelevents, to show if someone wants to access calendar
         Echo.private('calendar')
         .listen('WantsToAccessCalendar', (e) => {
            $("#asksForAccess").removeClass("d-none").text(e.message);
         })

      })
   </script>

   <style>
      body {
         /* 
            Since depending on the window size, there may be two scrollable divs, this property prevents browser from jumping 
         */
         overflow-anchor: none;
         /* To avoid contentjumping onload */
         visibility: hidden;
      }

      .custom-header-buttons {
         justify-content: center;
         display: flex;
         flex-direction: column;
         align-items: center;
      }

      #tools {
         display: flex;
         justify-content: center;
         margin: 1rem;
      }

      #tools div {
         margin: 0 .2rem;
      }

      .container-fluid {
         margin: 0;
         width: 100%;
         padding: 1rem;
         margin-right: 80px;
      }

      #hall-name {
         text-align: center;
         margin-bottom: 1rem;
      }


      .dropdown {
         width: inherit
      }

      .dropdown a {
         width: inherit
      }

      #add-event {
         width: inherit;
      }

      #add-event button {
         width: inherit;
      }

      .clipboard {
         display: none;
      }

      .clipboard .dropdown-menu.show {
         display: flex;
      }

      .clipboard #movie-to-insert {
         padding: 0 1rem;
      }

      .clipboard #clear-clipboard {
         align-self: start;
         font-size: 1.5rem;
         color: red;
         margin-right: 1rem;
         margin-top: -.5rem;
         cursor: pointer;
      }

      .clipboard #clear-clipboard:hover {
         background: lightgray;
      }

      #copy-container {
         width: inherit;
         display: flex;
         align-items: center;
         background: yellow;
      }

      #copy-container label {
         width: inherit;
         margin-bottom: 0;
      }

      #copy-container input {
         margin-left: 1rem;
      }

      #release {
         width: inherit;
         text-align: center;
      }

      #release button {
         width: inherit;
      }

      #calendar {
         border-top: 1px solid;
         padding-top: 1rem;
      }

      .fc .fc-toolbar.fc-header-toolbar {
         margin: 0;
      }

      /*
         The shown bar from events. Necessary to fill whole width when copying events, otherwise events would sit beside each other.
         z-index is a must, otherwise after dragging same event more than once, the event wouldn't have an id, since it's the temporary clone that would get clicked
      */
      .fc-timegrid-event-harness:not(.fc-timegrid-event-harness-inset) {
         z-index: 100 !important;
         margin-right: 0 !important;
         right: 0 !important;
      }

      /* Necessary to fill whole width when copying events, otherwise events would sit beside each other.*/
      .fc-timegrid-event-harness.fc-timegrid-event-harness-inset {
         left: 0 !important;
      }

      /* To center shown time in timeslots on the left */
      .fc-direction-ltr .fc-timegrid-slot-label-frame {
         text-align: center;
      }

      #add-event-modal-container {
         z-index: 3;
         display: none;
         position: fixed;
         left: 0;
         top: 0;
         width: 100%;
         height: 100%;
         overflow: auto;
         background-color: rgb(0, 0, 0);
         background-color: rgba(0, 0, 0, 0.4);
      }

      #add-event-modal-container form {
         position: absolute;
         top: 30%;
         left: 50%;
         transform: translateX(-50%);
         z-index: 3;
         padding: 2rem;
         border-radius: 10px;
         background: white;
      }

      .form-group-container {
         display: flex;
      }

      #own-movies,
      #external-movies {
         position: relative;
      }

      #own-movies ul,
      #external-movies ul {
         position: absolute;
         overflow: auto;
         max-height: 200px;
         list-style-type: none;
         padding: 0;
         border: 1px solid;
         border-top: 0px;
         border-bottom: 0px;
         width: 100%;
         z-index: 5;
         background: wheat;
      }

      ul li {
         padding: .5rem .3rem;
      }

      ul li:hover {
         background: lightgray;
      }

      .btn-container {
         text-align: center;
      }

      #drag {
         position: absolute;
         background: gray;
         color: white;
         opacity: .8;
         border-radius: 5px;
         z-index: -10;
      }

      .overlay {
         position: fixed;
         display: none;
         top: 0;
         left: 0;
         width: 100%;
         height: 100%;
         z-index: 10;
         background: rgba(0, 0, 0, .3);
      }

      .lds-roller {
         display: inline-block;
         position: relative;
         width: 80px;
         height: 80px;
         position: absolute;
         top: 20%;
         z-index: 10;
         left: 50%;
         transform: translateX(-50%);
      }

      .lds-roller div {
         animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
         transform-origin: 40px 40px;
      }

      .lds-roller div:after {
         content: " ";
         display: block;
         position: absolute;
         width: 7px;
         height: 7px;
         border-radius: 50%;
         background: #fff;
         margin: -4px 0 0 -4px;
      }

      .lds-roller div:nth-child(1) {
         animation-delay: -0.036s;
      }

      .lds-roller div:nth-child(1):after {
         top: 63px;
         left: 63px;
      }

      .lds-roller div:nth-child(2) {
         animation-delay: -0.072s;
      }

      .lds-roller div:nth-child(2):after {
         top: 68px;
         left: 56px;
      }

      .lds-roller div:nth-child(3) {
         animation-delay: -0.108s;
      }

      .lds-roller div:nth-child(3):after {
         top: 71px;
         left: 48px;
      }

      .lds-roller div:nth-child(4) {
         animation-delay: -0.144s;
      }

      .lds-roller div:nth-child(4):after {
         top: 72px;
         left: 40px;
      }

      .lds-roller div:nth-child(5) {
         animation-delay: -0.18s;
      }

      .lds-roller div:nth-child(5):after {
         top: 71px;
         left: 32px;
      }

      .lds-roller div:nth-child(6) {
         animation-delay: -0.216s;
      }

      .lds-roller div:nth-child(6):after {
         top: 68px;
         left: 24px;
      }

      .lds-roller div:nth-child(7) {
         animation-delay: -0.252s;
      }

      .lds-roller div:nth-child(7):after {
         top: 63px;
         left: 17px;
      }

      .lds-roller div:nth-child(8) {
         animation-delay: -0.288s;
      }

      .lds-roller div:nth-child(8):after {
         top: 56px;
         left: 12px;
      }

      @keyframes lds-roller {
         0% {
            transform: rotate(0deg);
         }

         100% {
            transform: rotate(360deg);
         }
      }
   </style>
</head>

<body>
   <div class="container-fluid">

      <div id="hall-name">
         <div id="asksForAccess" class="alert alert-danger d-none"></div>
         <h1 id=" hall"><?= $roomsMap[$_GET["roomId"]]["name"] ?></h1>
      </div>
      <div id="calendar">
      </div>

      <div class="custom-header-buttons">
         <form class="logout" action="{{route("calendar.logout")}}" method="post">
            @csrf
            @method("delete")
            <button class="btn btn-warning">Zurück zur Filmübersicht</button>
         </form>

         <div id="tools">
            <div class="dropdown">
               <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Saal wechseln</a>
               <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <?php foreach ($roomsMap as $room) : ?>
                  <?php if ($room["id"] == $_GET["roomId"]) continue ?>
                  <a class="dropdown-item" href="calendar?roomId=<?= $room["id"] ?>">
                     <?= $room["name"] ?></a>
                  <?php endforeach ?>
               </div>
            </div>
            <div id="release">
               <button id="release-button" class="btn btn-success">Veröffentlichen</button>
            </div>
            <div id="add-event"><button class="btn btn-primary">Film +</button></div>
            <div class="dropdown clipboard">
               <a class="btn btn-warning dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Zwischenablage</a>
               <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <div id="movie-to-insert"></div>
                  <div id="clear-clipboard">&times;</div>
               </div>
            </div>
            <div id="copy-container">
               <label class="btn btn-primary" for="copy">Kopieren<input type="checkbox" name="copy" id="copy"></label>
            </div>
         </div>
      </div>

      <div id="add-event-modal-container">
         <form id="add-event-form">
            <div class="form-group" id="external-movies">
               <label for="search-in-external-db">Film suchen</label>
               <input type="text" class="form-control" id="search-in-external-db" autocomplete="off"
                  placeholder="Harry Potter...">
               <ul id="external-db-results"></ul>
            </div>
            <div class="btn-container">
               <button id="btn-time-slot" type="submit" class="btn btn-primary" disabled>Zeitslot auswählen</button>
            </div>
         </form>
      </div>
   </div>

   <div class="overlay">
      <div class="lds-roller">
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
      </div>
   </div>
</body>

</html>