$(".formReserve").on("submit",function(e){
   e.preventDefault();
   $(".overlay").toggle();
   $.ajax({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: jQuery(this).attr("action"),
      type: "put",
      data: $(this).serialize(),
      success:function(resp){
         // $("button.reserve").attr("disabled",true);
         if(resp.status == 200) window.location.href = resp.successRoute;
         else if(resp.status == 403){
            $(".overlay").toggle();
            $("#modalReservation #modalReservationText").text(`${resp.msg}`);
            $("#modalReservation").modal("show");
         }
         else if(resp.status == 400){
            $(".overlay").toggle();
            if(resp.tooLate){
               console.log(resp);
               $("#modalReservation #modalReservationText").text(`${resp.msg}`);
               $("#modalReservation").modal("show");
            }
            else window.location.href = resp.failureRoute;
         }
      },
      error: function(err){
         $(".overlay").toggle();
         console.log(err);
      }
   })
})