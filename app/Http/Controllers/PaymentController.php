<?php

namespace App\Http\Controllers;

use App\Mail\Invoice;
use App\Models\Employee;
use App\Models\Receipt;
use App\Models\Ticket;
use App\Traits\CartItemCount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Omnipay\Omnipay;
use Illuminate\Support\Facades\Session;
use App\Traits\CreateOrderCode;
use App\Traits\GetEventData;
use App\Traits\GetUser;
use App\Traits\SelectTickets;
use App\Traits\SetDataForRedirect;
use App\Traits\UpdateTicketsTimestamp;
use DateTime;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    use CreateOrderCode, SelectTickets, UpdateTicketsTimestamp, SetDataForRedirect, GetUser, CartItemCount, GetEventData;

    public function __construct()
    {
        $this->middleware("prevent-history");
    }

    /**
     * Create onlinecharge and send confirmation mail on success
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function chargeOnline(Request $request)
    {
        $user = $this->getUser();

        $receipt = new Receipt(["order_mail" => $request->stripeEmail]);
        $receipt->save();

        $this->setTicketsInPayment(null, $user->queryCondition, $user->id);
        $tickets = $this->selectTickets("ticketsInPayment", "futureEndEvent", $user->queryCondition, $user->id);
        if (!$tickets->count()) return redirect()->route("payment.failure");

        $ticketsLinkedToReceipt = $this->linkTicketsToReceipt($tickets, $receipt->id);

        $response = $this->connectToStripe($request, $ticketsLinkedToReceipt);
        if ($response) {
            $this->updateDBAfterStripePayment($response, $ticketsLinkedToReceipt, $receipt, $request);
            return redirect()->route("payment.success");
        } else {
            return redirect()->route("payment.failure");
        }
    }

    /**
     * Update receipt data after successful stripe payment and send order details mail
     * @param mixed $responseData
     * @param mixed $tickets
     * @param \App\Models\Ticket $receipt
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function updateDBAfterStripePayment($responseData, $tickets, $receipt, Request $request)
    {
        $orderCode = $this->createOrderCode();
        $receipt->update(["transaction_id" => $responseData["balance_transaction"], "stripe_reference_id" => $responseData["id"], "order_code" => $orderCode]);

        $soldTickets = $this->setTicketsSold($tickets, config("constants.ticket_status_codes.bought_online"), $receipt->id);

        $dataForRedirect = $this->setDataForRedirect($soldTickets, $orderCode, $responseData, false, $request->stripeEmail,);
        session(["orderData" => $dataForRedirect]);

        Mail::to($request->stripeEmail)->send(new Invoice($dataForRedirect, "order"));

        DB::beginTransaction();
        $newReceiptId = $this->createReceiptId();
        $receipt->update(["confirmation_mail_sent" => 1, "receipt_id_for_tax" => $newReceiptId]);
        DB::commit();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param mixed $tickets
     * @return mixed
     */
    public function connectToStripe(Request $request, $tickets)
    {
        if ($request->input('stripeToken')) {
            $gateway = Omnipay::create('Stripe');
            // $gateway->setApiKey(env('STRIPE_SECRET_KEY'));
            $gateway->setApiKey(config('stripe.SECRET_KEY'));
            $response = $gateway->purchase([
                'amount' => $tickets->sum("price"),
                // 'currency' => env('STRIPE_CURRENCY'),
                'currency' => config('stripe.CURRENCY'),
                'token' => $request->input('stripeToken'),
            ])->send();
            if ($response->isSuccessful()) {
                return $response->getData();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Create new ReceiptId
     * @return string new ReceiptId
     */
    public function createReceiptId()
    {
        $latestReceiptId = DB::table("receipts")->latest("updated_at")->where("receipt_id_for_tax", "<>", null)->value("receipt_id_for_tax");
        if ($latestReceiptId) {
            $newReceiptId = (substr($latestReceiptId, strpos($latestReceiptId, "_") + 1) * 1) + 1;
            return (new DateTime)->format("Y") . "_" . $newReceiptId;
        } else {
            return (new DateTime)->format("Y") . "_" . 1;
        }
    }

    /**
     * Link given tickets to receipt
     * @param mixed $tickets 
     * @param int $receiptId
     * @return mixed
     */
    public function linkTicketsToReceipt($tickets, $receiptId)
    {
        foreach ($tickets as $ticket) $ticket->update(["receipt_id" => $receiptId]);
        return Ticket::where("receipt_id", $receiptId)->get();
    }

    /**
     * Update Tickets' Status to sold
     * @param mixed $tickets
     * @param string $statusCode Ticket Statuscode
     * @param int $receiptId
     * @param int|null $employeeId
     * @return mixed
     */
    public function setTicketsSold($tickets, $statusCode, $receiptId, $employeeId = NULL)
    {
        foreach ($tickets as $ticket) $ticket->update(["status" => $statusCode, "employee_id" => $employeeId]);
        return Ticket::where(["receipt_id" => $receiptId, "status" => $statusCode])->get();
    }

    /**
     * Update Tickets' Status to in Payment to prevent automated removal from DB
     * @param string|null $reservationCode
     * @param string|null $condition DB query condition
     * @param int|null $userId
     * @return void
     */
    public function setTicketsInPayment($reservationCode = null, $condition = null, $userId = null)
    {
        if ($reservationCode) Ticket::whereHas("reservation", function ($query) use ($reservationCode) {
            $query->where("reservation_code", $reservationCode);
        })->update(["status" => config("constants.ticket_status_codes.in_payment")]);
        else {
            Ticket::whereHas("futureEndEvent")->where([$condition => $userId, "status" => config("constants.ticket_status_codes.selected")])->update(["status" => config("constants.ticket_status_codes.in_payment")]);
        }
    }

    /**
     * When Employee sells Ticket and gets cash
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function chargeAtReceptionWithCash(Request $request)
    {
        if (isset($request->reservation_search_results)) {
            $this->setTicketsInPayment($request->reservation_code);
            $exists = $this->selectTickets("ticketInPaymentExists", "reservation", $request->reservation_code);
            if (!$exists) return redirect()->route("payment.failure");
            return redirect()->route("payment.awaitCash", $request->reservation_code);
        } else {
            $user = $this->getUser();
            $this->setTicketsInPayment(null, $user->queryCondition, $user->id);
            $exists = $this->selectTickets("ticketInPaymentExists", "futureEndEvent", $user->queryCondition, $user->id);
            if (!$exists) return redirect()->route("payment.failure");
            return redirect()->route("payment.awaitCash");
        }
    }

    /**
     * Await cash for Tickets either selected by employee or found with given reservationcode 
     * @param string|null $reservationCode
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse
     */
    public function awaitCash($reservationCode = null)
    {
        $reservedByUser = false;
        if ($reservationCode) {
            $reservedByUser = true;
            $tickets = $this->selectTickets("ticketsInPayment", "reservation", $reservationCode);
        } else {
            $employee = Employee::where("user_id", Auth::id())->first();
            $tickets = $this->selectTickets("ticketsInPayment", "futureEndEvent", "employee_id", $employee->id);
        }

        if (!$tickets->count()) return redirect()->route("payment.failure");

        $total = $tickets->sum("price");

        $eventData = $this->getEventData($tickets[0]->event);
        return view('payment.in_payment', compact("total", "tickets", "eventData", "reservedByUser", "reservationCode"));
    }

    /**
     * Set Tickets sold when cash received
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function manuallySetSold(Request $request)
    {
        $user = $this->getUser();

        if ($request->reserved_by_user) {
            $reservationCode = $request->code;
            $tickets = $this->selectTickets("ticketsInPayment", "reservation", $reservationCode);
        } else {
            $tickets = $this->selectTickets("ticketsInPayment", "futureEndEvent", $user->queryCondition, $user->id);
        }

        if (!$tickets->count()) {
            if (request()->ajax()) {
                $failureRoute = route("payment.failure");
                return response()->json(["status" => 400, "failureRoute" => $failureRoute]);
            } else {
                return redirect()->route("payment.failure");
            }
        }

        $orderCode = $this->createOrderCode();

        DB::beginTransaction();
        $newReceiptId = $this->createReceiptId();
        $receipt = new Receipt(["order_code" => $orderCode, "receipt_id_for_tax" => $newReceiptId, "transaction_id" => 123456789]);
        $receipt->save();
        DB::commit();

        $ticketsLinkedToReceipt  = $this->linkTicketsToReceipt($tickets, $receipt->id);
        $soldTickets = $this->setTicketsSold($ticketsLinkedToReceipt, config("constants.ticket_status_codes.sold_at_reception"), $receipt->id, $user->id);

        $dataForRedirect = $this->setDataForRedirect($soldTickets, $orderCode, [], $receipt->transaction_id);
        session(["orderData" => $dataForRedirect]);
        if (request()->ajax()) {
            $successRoute = route("payment.success");
            return response()->json(["status" => 200, "successRoute" => $successRoute]);
        } else {
            return redirect()->route("payment.success");
        }
    }

    /**
     * Display Order-Details after successful payment
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function success()
    {
        $user = $this->getUser();
        $cartQuantity = $this->cartItemCount($user);

        $orderData = Session::get("orderData");
        return view("payment.success", compact("orderData", "user", "cartQuantity"));
    }

    /**
     * Display Failure-Message after failed payment
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function failure()
    {
        dd("OOPS.. SOMETHING WENT WRONG DURING YOUR PAYMENT... SORRY!!");
        return view("payment.failure");
    }
}
