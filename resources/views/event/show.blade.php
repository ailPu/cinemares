@extends('layouts.app')

@section('pageTitle','Saalansicht')

@section('content')

@push("scripts")
<script src="{{ asset('js/tickets/deleteSelectedSeat.js')}}"></script>
<script src="{{ asset('js/tickets/selectSeat.js')}}"></script>
<script src="{{ asset('js/tickets/reserve.js')}}"></script>
@endpush


@if(session("success"))
<div class="alert alert-success"> {{session("success")}}</div>
@elseif(session("error"))
<form action="{{route("ticket.destroyTicketsWithDifferentEvent")}}" method="POST">
   @csrf
   @method("DELETE")
   <input type="hidden" name="event_id" value="{{$eventData->id}}">
   <button class="btn btn-warning">{{session("error")}}</button>
</form>
@elseif(session("seatTaken"))
<div class="alert alert-warning"> {{session("seatTaken")}}</div>
@endif

<div class="event container-fluid">
   <h1 class="text-center my-4">{{$eventData->movie->title}}</h1>
   <h2 class="room text-center">{{$eventData->room->name}}</h2>
   <h3 class="room text-center">{{ \Carbon\Carbon::parse($eventData["start_event"])->format('H:i | d.m.Y')}}
   </h3>
   <div class="rows d-flex justify-content-center text-center">
      <div class="row-width overflow-auto">
         @php
         $prevMaxSeat = NULL;
         foreach ($eventData->room->rows as $row) :
         if ($row->row == 1){
         $seat = $row->row;
         $lastSeat = intval($row->last_seat_num);
         }
         else{
         $seat = $prevMaxSeat + 1;
         $lastSeat = intval($row->last_seat_num);
         }
         @endphp
         <div class="row">
            @if($row->row == 1 || $row->row == 2)<div class="text-center col-1  align-self-center"><strong>A</strong>
            </div>
            @elseif($row->row == 3 || $row->row == 4)<div class="text-center col-1  align-self-center">
               <strong>B</strong></div>
            @elseif($row->row == 5)<div class="text-center col-1  align-self-center"><strong>C</strong></div>
            @endif
            <div class="col-11 seats row flex-nowrap">
               @while ($seat <= $lastSeat) @if(isset($tickets[$seat]["sold"])) <div>
                  <button class="seat-height seat-width seat h3 m-0 sold bg-danger" data-row="{{$row->row}}"
                     data-seat="{{$seat}}">
                     &times;
                  </button>
            </div>
            @elseif(isset($tickets[$seat]) && is_int($tickets[$seat]))
            <form class="formDelete event row" action="{{route("ticket.destroy",$tickets[$seat])}}" method="POST">
               @csrf
               @method("delete")
               <input type="hidden" name="seat" value="{{$seat}}">
               <input type="hidden" name="row" value="{{$row->row}}">
               <input type="hidden" name="event_id" value="{{$eventData->id}}">
               <button
                  class="seat-height seat-width seat bg-warning selected">{{$seat < 10 ? "0".$seat : $seat}}</button>
            </form>
            @elseif(isset($tickets[$seat]["expired"]))
            <form class="formDelete event row" action="{{route("ticket.destroy",$tickets[$seat]["expired"])}}"
               method="POST">
               @csrf
               @method("delete")
               <input type="hidden" name="reservation_expired" value="reservation_expired">
               <input type="hidden" name="seat" value="{{$seat}}">
               <input type="hidden" name="row" value="{{$row->row}}">
               <input type="hidden" name="event_id" value="{{$eventData->id}}">
               <button
                  class="seat-height seat-width seat selected bg-primary">{{$seat < 10 ? "0".$seat : $seat}}</button>
            </form>
            @else
            <form class="formStore row event" action="{{route("ticket.store")}}" method="POST" class="seat">
               @csrf
               <input type="hidden" name="seat" value="{{$seat}}">
               <input type="hidden" name="row" value="{{$row->row}}">
               <input type="hidden" name="event_id" value="{{$eventData->id}}">
               <button class="seat-height seat-width">{{$seat < 10 ? "0".$seat : $seat}}</button>
            </form>
            @endif
            @php $seat++; @endphp
            @endwhile
         </div>
      </div>
      @php
      $prevMaxSeat = $row->last_seat_num;
      endforeach
      @endphp

      <div class="row py-4">
         <div class="col-1 "></div>
         <div class="text-left col col-sm-4 pl-0">
            @foreach($pricingRows as $pricingRow)
            <div class="row">
               <p class="col-8 col-sm-6"><strong>Rang {{$pricingRow["name"]}}</strong></p>
               <p><strong>{{$pricingRow["price"]}}€</strong></p>
            </div>
            @endforeach
         </div>
         <div class="d-none d-sm-block col-3"></div>
         <div class="col">
            <div class="d-flex">
               <p class="border-danger pl-3 status"><strong>Vergeben</strong></p>
            </div>
            <div class="d-flex">
               <p class="border-warning pl-3 status"><strong>Ausgewählt</strong></p>
            </div>
            @can("admin&employee")
            <div class="d-flex">
               <p class="border-primary pl-3 status"><strong>Verfallen</strong></p>
            </div>
            @endcan
         </div>
      </div>
   </div>

</div>


<div class="text-center border-top mb-4">
   <div class="my-4">
      <strong>Preis: <span id="total">{{$total}}</span>€</strong>
   </div>
   <a href="{{route("cart.index")}}" class="cart btn btn-success mb-2 @if(!$total)disabled @endif">Zur
      Bestellübersicht</a>
</div>


<div class="modal" data-toggle="modal" id="exampleModal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <p></p>
         </div>
         <div class="modal-footer">
            <button id="removeOtherTickets" type="button" class="btn btn-primary"></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
         </div>
      </div>
   </div>
</div>

@endsection

<style>
   .seat-height {
      height: 54px;
   }

   .seat-width {
      width: 50px;
   }

   /* .row-width {
      width: 600px;
   } */

   .status {
      border-left: 5px solid;
   }

   .seats {
      cursor: pointer;
   }

   @media only screen and (max-width: 576px) {
      .row-width>.row:not(:last-child) {
         /* transform: scale(.5) */
      }
   }
</style>