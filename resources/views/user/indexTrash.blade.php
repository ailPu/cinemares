@extends('layouts.app')

@section('pageTitle','Softdeleted Mitarbeiter')

@section('content')

@push("scripts")
<script src="{{ asset('js/user/removeEmployee.js')}}"></script>
<script src="{{ asset('js/user/restoreEmployee.js')}}"></script>
@endpush

<div class="container">
   <div class="my-4"></div>
   <h1>Alle Softdeleted Users</h1>
   {{-- Wenn eine Session mit success existiert, dann hau den Text raus, der dort steht --}}
   @if(session("success"))
   <div class="alert alert-success">{{ session("success") }}</div>
   @endif
   @if(session("error"))
   <div class="alert alert-danger">{{ session("error") }}</div>
   @endif
   <table class="table">
      <thead>
         <tr>
            <th scope="col">#</th>
            <th scope="col">Vorname</th>
            <th scope="col">Nachname</th>
            <th scope="col">Email</th>
            <th scope="col">Wiederherstellen</th>
            <th scope="col">Löschen</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($trashedUsers as $user)
         <tr>
            <th scope="row">{{$user->id}}</th>
            <td>{{$user->employee->firstname}}</td>
            <td>{{$user->employee->lastname}}</td>
            <td>{{$user->email}}</td>
            <td>
               <form class="formRestore user" action="{{route("user.untrash",$user->id)}}" method="POST"
                  data-title="MitarbeiterIn {{$user->employee->firstname}} {{$user->employee->lastname}} wiederherstellen?">
                  @csrf
                  @method("PUT")
                  <button class="btn btn-info">
                     <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                           d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                     </svg>
                  </button>
               </form>
            </td>
            <td>
               <form class="formDelete user"
                  data-title="Wollen Sie {{$user->employee->firstname}} {{$user->employee->lastname}} endgültig löschen?"
                  action="{{route("user.destroyTrash",$user->id)}}" method="POST">
                  <button class="btn btn-danger"><svg width="1em" height="1em" viewBox="0 0 16 16"
                        class="bi bi-trash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                           d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                     </svg></button>
                  @csrf
                  @method("delete")
               </form>
            </td>
         </tr>
         @endforeach
      </tbody>
   </table>
</div>

<div class="modal" id="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <p id="modalBody"></p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary">Abbrechen</button>
            <button type="button" class="btn footer" data-dismiss="modal"></button>
         </div>
      </div>
   </div>
</div>

@endsection