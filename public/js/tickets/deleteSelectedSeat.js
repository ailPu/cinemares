$(document).on("click",".formDelete.event",function(e){
      e.preventDefault();
      const oldCartCount = $("#cart-quantity").text() * 1;
      const formHtml = $(this);
      console.log(formHtml);
      let seatNum = $(this).find("[name=seat]").val()*1;
      seatNum = seatNum < 10 ? `0${seatNum}` : seatNum;
      $(".overlay").toggle();
      $.ajax({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         url: $(this).attr("action"),
         type: "delete",
         data: $(this).serialize(),
         success:function(resp){
            if(resp.status == 200) {
               $(".overlay").toggle();
               if(!$("[name=reservation_expired]", formHtml).length) {
                  $("#cart-quantity").text(oldCartCount - 1);
               }   
               if ($("[name=reservation_expired]").length, formHtml) $("[name=reservation_expired]",formHtml).remove()
               $("[name=_method]",formHtml).remove();
               formHtml.attr("action",resp.storeRoute).removeClass().addClass("formStore event row");
               $("button",formHtml).removeClass("bg-warning");
               $("button",formHtml).removeClass("bg-primary");

               $("#total").text(resp.newTotal);
               if(!resp.newTotal) $(".cart.btn").addClass("disabled");
            }
         },
         error: function(err){
            $(".overlay").toggle();
            console.log(err);
         }
      })
   })