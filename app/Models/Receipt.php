<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    use HasFactory;

    protected $fillable = ["receipt_id_for_tax", "transaction_id", "stripe_reference_id", "order_code", "order_mail", "confirmation_mail_sent"];

    /**
     * Get related Ticketmodel Data
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(\App\Models\Ticket::class, "receipt_id", "id");
    }
}
