$("form.bad-tickets").on("submit",function(e){
   e.preventDefault();
   const table = $(this).closest("table");
   $(".overlay").toggle();
   $.ajax({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: $(this).attr("action"),
      type: $(this).attr("method"),
      data: $(this).serialize(),
      success:function(resp){
		  console.log(resp);
         if(resp.status == 200 ){
            table.remove();
            $(".overlay").toggle();
            $(`<div class="alert alert-success">${resp.msg}</div>`).insertAfter(".container h1");
            if($("table",$(".container-fluid")).length == 0) {
               $('<div class="no-bad-tickets">Es gibt keine fehlgeschlagenen Käufe</div>').insertAfter(".container h1");
            }
         }
		  else if(resp.status == 400) {
			  $(".overlay").toggle();
			  alert(resp.msg);
		  }
      },
      error:function(err){
         console.log(err);
      }
   })
})