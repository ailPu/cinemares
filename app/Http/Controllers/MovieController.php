<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Traits\CartItemCount;
use App\Traits\GetUser;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MovieController extends Controller
{

    use GetUser, CartItemCount;
    /**
     * Display all movies playing today
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $movies = Movie::whereHas("today")->get();
        $cartQuantity = $this->cartItemCount();

        return view("movie.today", compact("movies", "cartQuantity"));
    }

    /**
     * Display all movies playing next week
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function preview()
    {
        $movies = Movie::whereHas("nextWeek")->get();
        $cartQuantity = $this->cartItemCount();

        return view("movie.preview", compact("movies", "cartQuantity"));
    }

    /**
     * Display all movies playing this week
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function all(Request $request)
    {

        $cartQuantity = $this->cartItemCount();

        //! OHNE AJAX
        $movies = Movie::whereHas("currentWeek")->get();
        return view("movie.all", compact("movies", "cartQuantity"));


        //! AJAX TEST
        // $movies = Movie::whereHas("currentWeek")->paginate(1);
        // if ($request->ajax()) {
        //     return $movies;
        // }
        // return view("movie.all_ajax", compact("cartQuantity"));
    }



    /**
     * Display movie with given id
     *
     * @param int $id movieId
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function show($id)
    {
        setlocale(LC_TIME, 'German');

        $cartQuantity = $this->cartItemCount();

        $movieData = Movie::with("currentWeek.room", "currentWeek.room.maxSeats")->findOrFail($id);

        $title = $movieData->title;
        $allPlaytimesPerDay = [];
        foreach ($movieData->currentWeek as $playtime) {
            $startDatetime = Carbon::parse($playtime->start_event);
            $startDate = strftime("%a", strtotime($startDatetime));
            $startDate .= $startDatetime->format(" d.m.Y");
            $endDatetime = Carbon::parse($playtime->end_event);
            $movieIsRunning = Carbon::now() > $startDatetime && Carbon::now() < $endDatetime ? true : false;
            $movieHasEnded = Carbon::now() > $endDatetime ? true : false;
            $allPlaytimesPerDay[$startDate][] = ["id" => $playtime->id, "startEvent" => $startDatetime->format("H:i"), "endEvent" => $endDatetime->format("H:i"), "roomName" => $playtime->room->name, "movieIsRunning" => $movieIsRunning, "movieHasEnded" => $movieHasEnded];
        };

        return view("movie.show", compact("title", "movieData", "allPlaytimesPerDay", "cartQuantity"));
    }

    //! TODO NEU 
    public function showPreview($id)
    {
        setlocale(LC_TIME, 'German');

        $cartQuantity = $this->cartItemCount();
        $movieData = Movie::whereHas("nextWeek")->with("nextWeek.room", "nextWeek.room.maxSeats")->findOrFail($id);

        $title = $movieData->title;
        $allPlaytimesPerDay = [];
        foreach ($movieData->nextWeek as $playtime) {
            $startDatetime = Carbon::parse($playtime->start_event);
            $startDate = strftime("%a", strtotime($startDatetime));
            $startDate .= $startDatetime->format(" d.m.Y");
            $endDatetime = Carbon::parse($playtime->end_event);
            $allPlaytimesPerDay[$startDate][] = ["id" => $playtime->id, "startEvent" => $startDatetime->format("H:i"), "endEvent" => $endDatetime->format("H:i"), "roomName" => $playtime->room->name];
        };

        return view("movie.show_preview", compact("title", "movieData", "allPlaytimesPerDay", "cartQuantity"));
    }
}
