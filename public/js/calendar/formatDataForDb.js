/**
 * Formats Data for DB when fetched from external DB
 * @param {object} movieFromExternalDb 
 * @returns {object} Object that contains data formated for DB
 */
function formatDataForDb(movieFromExternalDb){
   let genreForDb = ""
   for (const i in movieFromExternalDb.genres) {
      genreForDb += (i < movieFromExternalDb.genres.length - 1) ? `${movieFromExternalDb.genres[i].name}, ` : movieFromExternalDb.genres[i].name;
   }

   const releaseYear = movieFromExternalDb["release_date"].substring(0, movieFromExternalDb["release_date"].indexOf("-"));

   const cast = movieFromExternalDb.credits.cast;
   let castForDb = "";
   if (!cast.length) castForDb = "";
   else {
      for (let i = 0; i <= 4; i++) {
      castForDb += (i < 4) ? `${cast[i].name}, ` : cast[i].name;
      }
   }

   return {
      genre:genreForDb,
      cast:castForDb,
      releaseYear,
   }
}
