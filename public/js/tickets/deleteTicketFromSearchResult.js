
$(".formDelete.search-overview").on("submit",function(e){
   const tr = $(this).closest("tr"); 
   e.preventDefault();
   $(".overlay").toggle();
   $.ajax({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: $(this).attr("action"),
      type: "delete",
      data: $(this).serialize(),
      success:function(resp){
         $(".overlay").toggle();
         if(resp.status == 200) {
            tr.remove();
            if(resp.newTotal * 1) $("#total").text(resp.newTotal)
            else {
               $(".employee-charging-methods").remove();
               $(`<h1 class="text-center">Es wurden keine Tickets für '${resp.code}' gefunden</h1>`).insertBefore(".results-header");
               $(".results-header").remove();
            }
         }
      },
      error:function(err){
         $(".overlay").toggle();
         console.log(err);
      }
   })
})