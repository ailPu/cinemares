@extends('layouts.app')

@section('pageTitle','Suchergebnisse')

@section('content')

@push("scripts")
<script src="{{ asset('js/tickets/deleteTicketFromSearchResult.js')}}"></script>
@endpush
<div class="container">

   <table class="table table-bordered table-responsive-sm text-center">
      @if(!$tickets->count())
      <h1 class="text-center my-4">Es wurden keine Tickets für '{{$code}}' gefunden</h1>
      @else
      <h1 class="results-header my-4 text-center">@if($ticketType == "reservation") Reservierte @else Gekaufte @endif
         Tickets für '{{$code}}'</h1>
      @endif
      <thead class="thead-inverse">
         <tr>
            <th>Film</th>
            <th>Spielzeit</th>
            <th>Saal</th>
            <th>Reihe</th>
            <th>Sitzplatz</th>
            <th>Preis</th>
            @if($ticketType == "reservation")
            <th>Entfernen</th>
            @endif
         </tr>
      </thead>
      <tbody class="search">
         @foreach ($tickets as $ticket)
         <tr>
            <td scope="row">{{$eventData["movieTitle"]}}</td>
            <td>{{ \Carbon\Carbon::parse($eventData["startEvent"])->format('H:i | d.m.Y')}}</td>
            <td>{{$eventData["roomName"]}}</td>
            <td>{{$ticket->row}}</td>
            <td>{{$ticket->seat}}</td>
            <td>{{$ticket->price}}€</td>
            @if($ticketType == "reservation")
            <td>
               <form class="formDelete search-overview" action="{{route("ticket.destroy",$ticket->id)}}" method="POST">
                  @csrf
                  @method("delete")
                  <input type="hidden" name="reservation_search_results" value="reservation_search_results">
                  <input type="hidden" name="code" value="{{$code}}">
                  <button class="btn btn-danger">
                     <svg width="1em" height="1em" viewBox="0 0 16 16" class="remove bi bi-x" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                           d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                     </svg>
                  </button>
               </form>
            </td>
            @endif
         </tr>
         @endforeach
      </tbody>
      <tfoot class="bg-dark text-light">
         <tr>
            <td>Total:</td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td><span id="total">@if(isset($total)) {{$total}}@else 0 @endif</span>€</td>
            @if($ticketType == "reservation")
            <td class="border-0"></td>
            @endif
         </tr>
      </tfoot>
   </table>

   @if($tickets->count() && $ticketType == "reservation")
   <div class="employee-charging-methods d-flex justify-content-center">
      <form class="sell-cash form" action="{{route("charge.atReceptionWithCash")}}" method="POST">
         @csrf
         @method("put")
         <input type="hidden" name="reservation_search_results" value="reservation_search_results">
         <input type="hidden" name="reservation_code" value="{{$code}}">
         <button class=" btn btn-success mr-2">Barzahlung</button>
      </form>
      <a href="endpoint" class="btn btn-success">Bankomatkarte</a>
   </div>
   @endif

   <div class="text-center my-2">
      <a href="{{route("movie.index")}}" class="redirect-home btn btn-primary">Zurück zur Filmübersicht</a>
   </div>
</div>

@endsection