/**
 * Format date for DB
 * @param {object} event 
 * @returns {object} Object that contains start- and enddate formated for DB
 */
function formatDate(event){
   return {
      start: moment(event.start, "DD.MM.YYYY").format("Y-MM-DD HH:mm:ss"),
      end: moment(event.end, "DD.MM.YYYY").format("Y-MM-DD HH:mm:ss"),
   }
}