<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Traits\CartItemCount;
use App\Traits\GetUser;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    use GetUser, CartItemCount;

    public function __construct()
    {
        $this->middleware(["auth", "can:admin&employee"]);
    }

    /**
     * Display Dashboard
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $cartQuantity = $this->cartItemCount();

        $employee = Employee::with("user")->where("user_id", Auth::id())->first();
        return view("employee.index", compact("employee", "cartQuantity"));
    }
}
