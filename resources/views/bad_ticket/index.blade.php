@extends('layouts.app')

@section('pageTitle','Fehlgeschlagene Käufe')

@section('content')

@push("scripts")
<script src="{{ asset('js/tickets/badTickets.js')}}"></script>
@endpush
<div class="container">

   <h1 class="text-center my-4">Fehlerhafte Käufe</h1>

   @if(session("success"))
   <div class="alert alert-success">{{session("success")}}</div>
   @elseif(session("error"))
   <div class="alert alert-danger">{{session("error")}}</div>
   @endif

   @if(!$ticketsGroupedByTimestampAndOrdermail->count())
   <p>Es gibt keine fehlgeschlagenen Käufe</p>
   @else
   @foreach ($ticketsGroupedByTimestampAndOrdermail as $ticketsGroupedByTimestamp)
   <table class="table table-bordered table-responsive-md text-center">
      <thead class="thead-inverse">
         <tr>
            <th class="align-middle">Bestellmail</th>
            <th class="align-middle">Zeitstempel</th>
            <th class="align-middle">Status</th>
            <th class="align-middle">Bestätigungsmail versendet</th>
            <th class="align-middle">Preis</th>
            <th class="align-middle">Aktion</th>
         </tr>
      </thead>
      <tbody class="bad-tickets">
         @foreach ($ticketsGroupedByTimestamp as $ticketsGroupedByOrderMail)
         @php
         $total = 0;
         $badTicketIdsPerOrderMail = [];
         @endphp
         @foreach ($ticketsGroupedByOrderMail as $ticket)
         @php $badTicketIdsPerOrderMail[] = $ticket->id; @endphp
         @php $total += $ticket->price; @endphp
         <tr>
            <td class="align-middle">@if(isset($ticket->receipt->order_mail) && $ticket->receipt->order_mail)
               {{$ticket->receipt->order_mail}}
               @else N/A @endif
            </td>
            <td class="align-middle">
               @if(isset($ticket->updated_at)){{\Carbon\Carbon::parse($ticket->updated_at)->format('H:i:s | d.m.Y')}}
               @else N/A @endif
            </td>
            <td class="align-middle">@if($ticket->status == config("constants.ticket_status_codes.in_payment")) In
               Zahlung @else Verkauft @endif</td>
            <td class="align-middle">@if(isset($ticket->receipt->confirmation_mail_sent) &&
               $ticket->receipt->confirmation_mail_sent)
               JA @else NEIN @endif
            </td>
            <td class="align-middle"><span class="price">{{$ticket->price}}</span>€</td>
            <td class=""></td>
         </tr>
         @endforeach
         <tr class="bg-danger total-container">
            <td class="align-middle"><strong>Bestellsumme</strong></td>
            <td class="align-middle" colspan=3></td>
            <td class="align-middle"><strong class="total">{{$total}} </strong><strong>€</strong></td>
            <td class="align-middle">
               <form action="{{route("support.sendMail")}}" class="formUpdate bad-tickets" method="POST">
                  @foreach($badTicketIdsPerOrderMail as $badTicketId)
                  <input type="hidden" name="badTicketIds[]" value="{{$badTicketId}}">
                  @endforeach
                  @csrf
                  @method("put")
                  <button class="btn btn-warning">Rückerstattung anfragen</button>
               </form>
               <form action="{{route("ticket.destroyBadTickets")}}" class="formDelete bad-tickets" method="POST">
                  @foreach($badTicketIdsPerOrderMail as $badTicketId)
                  <input type="hidden" name="badTicketIds[]" value="{{$badTicketId}}">
                  @endforeach
                  @csrf
                  @method("delete")
                  <button class="btn btn-success">Tickets freischalten</button>
               </form>
            </td>
         </tr>
         <tr>
            <td colspan=6></td>
         </tr>
         @endforeach
      </tbody>

      <tfoot class="bg-dark text-light">
      </tfoot>
   </table>
   @endforeach
   @endif
</div>

<style>
   .bad-order-start {
      border: 4px solid #dc3545 !important;
      border-bottom: none;
      /* border-top: none !important; */
   }

   .bg-danger {
      border: 4px solid #dc3545 !important;
   }

   .bad-order-end {}
</style>

@endsection