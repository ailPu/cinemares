<?php

namespace App\Http\Controllers;

use App\Traits\CartItemCount;
use App\Traits\GetEventData;
use App\Traits\GetUser;
use App\Traits\SelectTickets;
use App\Traits\UpdateTicketsTimestamp;
use Carbon\Carbon;

class CartController extends Controller
{

    use UpdateTicketsTimestamp, SelectTickets, GetUser, CartItemCount, GetEventData;

    public function __construct()
    {
        $this->middleware("prevent-history");
    }
    /**
     * Display Cart Items
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $user = $this->getUser();
        $cartQuantity = $this->cartItemCount($user);

        $this->updateTicketsTimestamp(config("constants.ticket_status_codes.selected"), $user);
        $tickets = $this->selectTickets("ticketsInCart", "futureEndEvent", $user->queryCondition, $user->id);
        if (!$tickets->count()) return view("cart.index", compact("tickets", "user"));

        $startDate = Carbon::parse($tickets[0]->event->start_event);
        $tooLateForReservation = Carbon::now() >= $startDate->subMinutes(30) ? true : false;

        $total = $tickets->sum("price");
        $eventData = $this->getEventData($tickets[0]->event);
        return view('cart.index', compact("total", "tickets", "eventData", "user", "tooLateForReservation", "cartQuantity"));
    }
}
