<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Role;
use App\Models\User;
use App\Traits\CartItemCount;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    use SoftDeletes, CartItemCount;

    public function __construct()
    {
        $this->middleware(["auth", "can:adminOnly"]);
    }

    /**
     * Display all employees
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $cartQuantity = $this->cartItemCount();

        $users = User::whereHas("employee")->with("employee", "employee.role")->paginate(20);
        $softDeletedUsers = User::onlyTrashed()->first();
        $admins = User::whereHas("employee.isAdmin")->count();
        return view("user.index", compact("users", "admins", "softDeletedUsers", "cartQuantity"));
    }

    /**
     * Show the form for creating a new Employee
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function create()
    {
        $cartQuantity = $this->cartItemCount();

        $roles = Role::where("name", "<>", "client")->get();
        return view("user.create", compact("roles", "cartQuantity"));
    }

    /**
     * Store a newly created employee in DB
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            "firstname" => "required|min:4",
            "lastname" => "required|min:4",
            "email" => "required|email:filter,dns|unique:users,email",
            "password" => "required|confirmed|min:4",
            "role_id" => "exists:\App\Models\Role,id",
        ]);

        $user = new User(["email" => $request->email, "password" => $request->password]);
        $user->password = Hash::make($request->password);
        $user->save();
        $employee = new Employee(["role_id" => $request->role_id, "user_id" => $user->id, "firstname" => $request->firstname, "lastname" => $request->lastname]);
        $employee->save();
        return redirect()->route("user.index")->with("success", "MitarbeiterIn wurde erfolgreich hinzugefügt");
    }

    /**
     * Show the form for editing employee.
     * @param \App\Models\User $user 
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(User $user)
    {
        $cartQuantity = $this->cartItemCount();

        $roles = Role::where("name", "<>", "client")->get();
        $admins = User::whereHas("employee.isAdmin")->count();
        return view("user.edit", compact("user", "roles", "admins", "cartQuantity"));
    }

    /**
     * Update employee in DB
     * @param \Illuminate\Http\Request $request
     * @param int $id EmployeeId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::with("employee")->findOrFail($id);
        $admins = User::whereHas("employee.isAdmin")->count();
        if ($admins == 1 && $user->employee->role_id == 1) {
            if ($request->role_id && $request->role_id != 1) {
                $username = $user->employee->firstname . " " . $user->employee->lastname;
                return redirect()->route("user.index")->with("error", "MitarbeiterIn '$username' darf nicht weniger Zugriffsrechte erhalten, da es sich um den letzten Admin handelt.");
            }
        }

        $request->validate([
            "email" => "nullable|email:filter,dns|unique:users,email,$user->id,id",
            "role_id" => "nullable|exists:\App\Models\Role,id",
            "password" => "nullable|confirmed|min:4",
        ]);

        $userData = $request->except("role_id");
        if ($request->password) $userData["password"] = Hash::make($request->password);
        else unset($userData["password"]);

        $username = $user->employee->firstname . " " . $user->employee->lastname;
        Employee::where("user_id", $user->id)->update(["role_id" => $request->role_id]);
        $user->update($userData);
        return redirect()->route("user.index")->with("success", "MitarbeiterIn '$username' wurde erfolgreich geupdated.");
    }

    /**
     * Show soft deleted employees
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function indexTrash()
    {
        $cartQuantity = $this->cartItemCount();

        $trashedUsers = User::with("employee")->onlyTrashed()->get();
        return view("user.indexTrash", compact("trashedUsers", "cartQuantity"));
    }

    /**
     * Restore soft deleted employee with given id
     * @param int $id EmployeeId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function untrash($id)
    {
        $user = User::with("employee")->onlyTrashed()->findOrFail($id);
        $user->restore();
        $username = $user->employee->firstname . " " . $user->employee->lastname;
        if (request()->ajax()) return response()->json(["status" => 200, "msg" => "Mitarbeiter/in '$username wurde erfolgreich wiederhergestellt."]);
        return redirect()->route("user.index")->with("success", "Mitarbeiter/in '$username wurde erfolgreich wiederhergestellt.");
    }

    /**
     * Completely delete employee
     * @param int $id EmployeeId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroyTrash($id)
    {
        $user = User::with("employee")->onlyTrashed()->findOrFail($id);
        $username = $user->employee->firstname . " " . $user->employee->lastname;
        $employee = Employee::where("user_id", $id)->first();
        $employee->delete();
        $user->forceDelete();
        if (request()->ajax()) return response()->json(["status" => 200, "msg" => "Mitarbeiter/in '$username' wurde endgültig gelöscht."]);
        else return redirect()->route("user.index")->with("success", "Mitarbeiter/in '$username' wurde endgültig gelöscht.");
    }

    /**
     * Softdelete employee from DB
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {
        $admins = User::with("employee")->whereHas("employee.isAdmin")->count();
        $username = $user->employee->firstname . " " . $user->employee->lastname;

        if ($admins == 1 && $user->employee->role_id == 1) {
            if (request()->ajax()) return response()->json(["status" => 400, "msg" => "MitarbeiterIn '$username' darf nicht gelöscht werden, da es sich um den letzten Administrator handelt."]);
            else return redirect()->route("user.index")->with("error", "MitarbeiterIn '$username' darf nicht gelöscht werden, da es sich um den letzten Administrator handelt.");
        } else {
            $user->delete();
            if (request()->ajax()) return response()->json(["status" => 200, "msg" => "MitarbeiterIn '$username' wurde erfolgreich gelöscht."]);
            else return redirect()->route("user.index")->with("success", "MitarbeiterIn '$username' wurde erfolgreich gelöscht.");
        }
    }
}
