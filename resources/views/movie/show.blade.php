@extends('layouts.app')

@section('pageTitle','Filmdetails')

@section('content')

<div class="movie container">
   <h1 class="col-md-12 text-center p-4">{{$title}}</h1>
   <div class="row">
      <div class="img-container col-sm-4">
         <img src="https://image.tmdb.org/t/p/original/{{$movieData->poster_path}}" class="img-fluid" alt="">
      </div>
      <div class="movie-info-container col-sm-8 mt-3">
         <div class="movie-duration row">
            <p class="col-sm-6 col-md-5 col-lg-4"><strong>Dauer:</strong></p>
            <p class="col">{{$movieData->duration}} Minuten</p>
         </div>
         <div class="movie-genre row">
            <p class="col-sm-6 col-md-5 col-lg-4"><strong>Genre:</strong></p>
            <p class="col">{{$movieData->genre}}</p>
         </div>
         <div class="movie-cast row">
            <p class="col-sm-6 col-md-5 col-lg-4"><strong>Cast:</strong></p>
            <p class="col">{{$movieData->cast}}</p>
         </div>
         <div class="movie-release-year row">
            <p class="col-sm-6 col-md-5 col-lg-4"><strong>Veröffentlicht:</strong></p>
            <p class="col">{{$movieData->release_year}}</p>
         </div>
      </div>
   </div>
   <div class="movie-overview col-sm-10 my-4 ">
      <h2>Handlung</h2>
      <p>{{$movieData->overview}}</p>
      <h2 class="playtimes">
         Spielzeiten
      </h2>
      {{-- Meine "Collection" mit den Spieltagen --}}
      @foreach($allPlaytimesPerDay as $day => $playtimesPerDay)
      <div class="row p-2">
         <p class="col-sm-4 col-lg-3">{{$day}}</p>
         {{-- Die Spielzeiten für einen Spieltag --}}
         <div class="row col">
            @foreach($playtimesPerDay as $playtimePerDay)
            @if($playtimePerDay["movieHasEnded"])
            <span class="col-12 col-sm-2 text-danger">
               <p>{{$playtimePerDay["roomName"]}}</p>
               <p>{{$playtimePerDay["startEvent"]}} - {{$playtimePerDay["endEvent"]}}</p>
            </span>
            @else
            <a href="{{route("event.show",$playtimePerDay["id"])}}" class="col-12 col-sm-5">
               <p>
                  {{$playtimePerDay["roomName"]}}
               </p>
               <p>
                  {{$playtimePerDay["startEvent"]}} - {{$playtimePerDay["endEvent"]}}
               </p>
               @if($playtimePerDay["movieIsRunning"])
               <p class="text-warning movie-is-running">
                  Läuft
               </p>
               @endif
            </a>
            @endif
            @endforeach
         </div>
      </div>
      @endforeach
   </div>

</div>

<style>
   @keyframes fadeIn {
      from {
         opacity: .2;
      }
   }

   .movie-is-running {
      animation: fadeIn .7s infinite alternate;
   }
</style>
@endsection