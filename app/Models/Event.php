<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = ["movie_id", "room_id", "start_event", "end_event", "released"];

    /**
     * Get related Moviemodel Data
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movie()
    {
        return $this->belongsTo(\App\Models\Movie::class);
    }

    /**
     * Get related Roommodel Data
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(\App\Models\Room::class, "room_id", "id");
    }

    /**
     * Get related Ticketmodel Data
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(\App\Models\Ticket::class, "event_id", "id");
    }

    /**
     * Check if Reservations are still allowed for Event
     * @return mixed
     */
    public function allowReservation()
    {
        return $this->hasMany(\App\Models\Ticket::class, "event_id", "id")->where([["start_event", ">", Carbon::now()->addMinutes(30)]]);
    }
}
