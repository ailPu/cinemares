<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;

    protected $details;
    protected $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details, $type)
    {
        $this->details = $details;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->type == "order") $viewFile = 'mail.invoice';
        elseif ($this->type == "reservation") $viewFile = 'mail.reservation';
        elseif ($this->type == "support") $viewFile = 'mail.refund';
        return $this->from(config("mail.from.address"), config("mail.from.name"))->subject($this->type)->view($viewFile)->with($this->details);
    }
}
