<?php

namespace App\Traits;

use App\Models\Ticket;

trait CreateOrderCode
{
   /**
    * Create Ordercode
    * @return string
    */
   public function createOrderCode()
   {
      $usedOrderCodes = $this->getUsedOrderCodes();
      $createdOrderCode = "";
      while (strlen($createdOrderCode) < 4) {
         $randomCharcode = rand(48, 90);
         while ($randomCharcode >= 58 && $randomCharcode <= 64) $randomCharcode = rand(48, 90);
         $createdOrderCode .= chr($randomCharcode);
      };
      if (count($usedOrderCodes)) {
         if (isset($usedOrderCodes[$createdOrderCode])) $this->createOrderCode($usedOrderCodes);
      }
      return $createdOrderCode;
   }

   /**
    * Get used Ordercodes
    * @return array
    */
   public function getUsedOrderCodes()
   {
      $soldAndReservedTickets = Ticket::whereHas("futureEndEvent")->with("reservation", "receipt")->where("reservation_id", "<>", "NULL")->orWhere("receipt_id", "<>", "NULL")->get();
      $usedOrderCodes = [];
      foreach ($soldAndReservedTickets as $ticket) {
         if ($ticket->reservation) {
            // Damit nicht derselbe Reservierungscode öfters reingeschmissen wird (ein Code kann ja bei mehreren Tickets stehen)
            if (isset($usedOrderCodes[$ticket->reservation->reservation_code])) continue;
            $usedOrderCodes[$ticket->reservation->reservation_code] = $ticket->reservation->orderCode;
         } elseif ($ticket->receipt) {
            // Damit nicht derselbe Bestellcode öfters reingeschmissen wird (ein Code kann ja bei mehreren Tickets stehen)
            if (isset($usedOrderCodes[$ticket->receipt->orderCode])) continue;
            $usedOrderCodes[$ticket->receipt->orderCode] = $ticket->receipt->orderCode;
         }
      }
      return $usedOrderCodes;
   }
}
