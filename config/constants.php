<?php

return [
   /*
   |--------------------------------------------------------------------------
   | Ticket Statuscodes
   |--------------------------------------------------------------------------
   |
   | Status Code Constants for ticket
   |
   */

   'ticket_status_codes' => [
      "selected" => 0,
      "in_payment" => 1,
      "bought_online" => 2,
      "reserved" => 3,
      "sold_at_reception" => 4,
      "in_support" => 5,
   ],

   'calendar_status_codes' => [
      'not_released' => 0,
      'released' => 1,
   ]
];
