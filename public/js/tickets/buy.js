// Listen for mutations on body to show loading spinner, when stripe form disappears, to show user that order is being processed
setTimeout(()=>{
   const targetNode = document.querySelector("body");
   const observerOptions = {
      childList: true,
      attributes: true,
      subtree: true 
   }
   
   const observer = new MutationObserver((mutationList,observer)=>{
      mutationList.forEach((mutation)=>{
         if (mutation.target == document.querySelector("#stripe-form")) { 
            $(".overlay").show();
            observer.disconnect();
         }
      })
   });
   observer.observe(targetNode, observerOptions);
},3000)