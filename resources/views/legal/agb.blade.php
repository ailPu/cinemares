@extends("layouts.app")

@section("title","AGB")

@section("content")

<div class="container">
   <h1 class="text-center">Allgemeine Geschäftsbedingungen für Kinos</h1>
   <h2 class="text-center">Hausordnung</h2>
   <div>
      <h3>1 Geltung der AGBs</h3>
      <ol>
         <li> Der Vertrag zwischen Kinobetreiber und Kinobesucher über den Besuch einer Filmvorführung wird mit der
            Aushändigung
            der Eintrittskarte an den Kinobesucher abgeschlossen. Mit der Erklärung, eine Eintrittskarte erwerben zu
            wollen,
            unterwirft sich der Kinobesucher diesen Allgemeinen Geschäftsbedingungen (,,AGB"). Dies gilt auch für den
            Fall,
            dass
            der Besucher die Eintrittskarte telefonisch, per Internet oder anderer Kommunikationstechniken vorbestellt.
         </li>
         <li>
            Mit der Übertragung der Kinokarte auf einen Dritten, wird das Vertragsverhältnis unter Anwendung dieser
            AGB
            auf
            den Erwerber übertragen. Der Veräußerer der Kinokarte ist verpflichtet, den Erwerber auf die Geltung dieser
            AGB
            hinzuweisen.
         </li>

         <li>
            Für Personen, die sich in den Kinoräumlichkeiten aufhalten, ohne dass diese AGB im Wege eines
            Vertragsabschlusses
            wirksam werden, gelten diese AGB als Hausordnung.
         </li>
      </ol>

      <h3>2 Erwerb der Kinokarte</h3>
      <ol>
         <li>
            Für den Erwerb der Eintrittskarte gelten die für die jeweilige Filmvorführung ausgewiesenen Preise. Die
            Preise
            enthalten die Umsatzsteuer und andere Abgaben in der gesetzlich vorgeschriebenen Höhe.
         </li>

         <li>
            Der Kinobesucher ist verpflichtet, sofort nach Übernahme der Eintrittskarte zu prüfen, ob er die Karte
            für
            die
            gewünschte Kinovorstellung erhalten hat. Nachträgliche Reklamationen werden nicht berücksichtigt.
         </li>

         <li>
            Der Kinobetreiber behält sich das Recht vor, Kinobesuchern, die andere Kinobesucher oder Mitarbeiter des
            Kinobetreibers belästigen und/oder die sich Anordnungen des Kinopersonals widersetzen, den Erwerb von
            Eintrittskarten
            für bestimmte Zeit oder in schwerwiegenden Fällen auf Dauer zu versagen.
         </li>

         <li>
            Bei Vorbestellung müssen die Eintrittskarten spätestens 30 Minuten vor der Filmvorführung abgeholt und
            bezahlt
            werden. Nicht fristgerecht abgeholte Eintrittskarten kann der Kinobetreiber ohne Rücksichtnahme auf die
            Vorbestellung
            verkaufen.
         </li>

         <li>
            Eine Rücknahme oder ein Umtausch bezahlter Eintrittskarten ist nicht möglich. Ein Ersatz für nicht oder
            (durch
            Zu-Spät-Kommen) nur teilweise in Anspruch genommene Eintrittskarten oder für wie immer abhanden gekommene
            Eintrittskarten wird nicht geleistet.
         </li>
      </ol>





      <h3>3 Zutritt zu den Kinosälen</h3>

      <ol>

         <li>
            Die Berechtigung, einer bestimmten Filmvorführung beizuwohnen, wird mit der gültigen Kinokarte
            ausgewiesen. Der
            Kinobetreiber ist nicht verpflichtet, zu prüfen, ob der Kinobesucher die Eintrittskarte rechtmäßig erworben
            hat.
         </li>

         <li>
            Kinosäle dürfen ohne eine gültige Eintrittskarte nicht betreten werden. Der Kinobetreiber behält sich das
            Recht
            vor, die Zutrittsberechtigung zu prüfen und bei fehlender Zutrittsberechtigung, den Zutritt zu dem Kinosaal
            zu
            verweigern bzw den Kinobesucher der Filmvorführung zu verweisen. Der Besucher hat daher die Eintrittskarte
            bis zum
            Ende der Vorführung aufzubewahren.
         </li>

         <li>
            Die Mitnahme von Waffen, Fotoapparaten, Digitalkameras, Video-, DVD- und ähnlichen Bild- oder
            Ton-Aufnahmegeräten
            in die Kinosäle ist nicht gestattet. Kinobesucher sind verpflichtet, derartige Gegenstände vor dem Betreten
            eines
            Kinosaals an der Kinokassa abzugeben und nach dem Verlassen des Kinosaales abzuholen. Geräte, die binnen
            drei
            Tagen
            nach dem Tag der Vorstellung nicht abgeholt werden, werden dem Fundamt übergeben.
         </li>

         <li>
            Die Mitnahme von Speisen und/oder Getränken, die beim Kinobuffet erworben wurden, in die Kinosäle ist mit
            Genehmigung des Kinosbetreibers zulässig. Die Mitnahme mitgebrachter Speisen und/oder Getränke in die
            Kinosäle ist
            jedenfalls unzulässig.
         </li>

         <li>
            Das Kinopersonal ist berechtigt, Taschen und Kleidung von Kinobesuchern vor dem Eintritt und bei
            Verlassen des
            Kinosaales zu durchsuchen. Verweigert oder verhindert der Kinobesucher die Durchsuchung, wird der Zutritt in
            den
            Kinosaal ohne Rückzahlung des Eintrittspreises verwehrt. Stellt sich nach Eintritt des Besuchers in den
            Kinosaal
            heraus, dass er einen der verbotenen Gegenstände in den Kinosaal mitgenommen hat, wird der Kinobesucher ohne
            Rückzahlung des Eintrittspreises der Filmvorführung verwiesen.
         </li>
      </ol>

      <h3>
         4 Verhalten im Kinosaal
      </h3>
      <ol>

         <li>
            Kinobesucher können nach einer Mahnung des Kinopersonals ohne Rückzahlung des Eintrittspreises der
            Filmvorführung
            verwiesen werden, wenn sie:
            <ul class="list-unstyled">
               <li class="p-2">
                  a) sich weigern, ihren auf der Eintrittskarte zugewiesenen Platz einzunehmen;
               </li>
               <li class="p-2">
                  b) andere Kinobesucher vor oder während der Filmvorführung akustisch, durch ihr Verhalten oder ihren
                  Geruch
                  stören
                  oder belästigen;
               </li>
               <li class="p-2">
                  c) im Kinosaal rauchen;
               </li>
               <li class="p-2">
                  d) Essensreste auf den Boden werfen oder sonst den Kinosaal verunreinigen;
               </li>
               <li class="p-2">
                  e) Waffen oder sonstige gemäß Punkt 11. verbotene Gegenstände - in den Kinosaal mitnehmen, und zwar
                  unabhängig
                  davon,
                  ob diese konkret benutzt werden oder nicht;
               </li>
               <li class="p-2">
                  f) ohne Genehmigung des Kinobetreibers Speisen und/oder Getränke in den Kinosaal mitnehmen.
               </li>
            </ul>
         </li>

         <li>
            Nach dem Ende der Filmvorführung sind die Kinosäle durch die bezeichneten Ausgänge zu verlassen. Der
            Aufenthalt in den Kinosälen ist nach dem Ende der Filmvorführung unzulässig.
         </li>
      </ol>

      <h3>
         5 Verbot von Bild- und Tonaufnahmen
      </h3>
      <ol>

         <li>
            Filme (Laufbildwerke) sind urheberrechtlich geschützte Werke. Das Vervielfältigen, Verbreiten oder das
            öffentliche
            Aufführen dieser Werke ohne ausdrückliche schriftliche Zustimmung des Rechteinhabers (Lizenzvereinbarung)
            ist
            gerichtlich strafbar. Bild- oder Tonbandaufnahmen während einer Filmvorführung sind daher (auch für private
            Zwecke)
            unter keinen Umständen zulässig.
         </li>

         <li>
            Liegen Gründe vor, anzunehmen, dass ein Kinobesucher unzulässige Bild- oder Tonaufnahmen während der
            Filmvorführung angefertigt hat oder anzufertigen versucht hat, kann ihn das Kinopersonal in angemessener
            Weise
            anhalten (§ 86 Abs 2 StPO). Das Kinopersonal wird die Anhaltung unverzüglich dem nächsten Sicherheitsorgan
            bekannt
            geben.
         </li>

         <li>
            Die Anhaltung kann unterbleiben, wenn der Kinobesucher der Aufforderung des Kinopersonals, seine Identität
            nachzuweisen nachkommt und das Gerät, mit dem die Bild- oder Tonaufnahme erfolgt sein könnte, in die
            Verwahrung
            des
            Kinopersonals übergibt. Der Kinobetreiber wird in diesem Fall unverzüglich entsprechende rechtliche Schritte
            zur
            Klärung des Sachverhalts einleiten. Werden die in Verwahrung genommenen Gegenstände nicht binnen acht Wochen
            gerichtlich beschlagnahmt, werden sie dem Kinobesucher an der Kinokassa zurückerstattet. Eine Versendung der
            in
            Verwahrung übernommenen Gegenstände kann nur auf Kosten und Risiko des Kinobesuchers erfolgen.
         </li>
      </ol>
   </div>
</div>
@endsection