/**
 * Calculate endtime for given eventObj
 * @param {object} selectionInfo 
 * @param {object} eventObj 
 * @return {object} Object with calculated start- and endtime
 */
function calculateEndTime(selectionInfo, eventObj){
   const durationForCalc = eventObj.duration % 15 <= 8
   ? Math.floor(eventObj.duration / 15) * 15
   : Math.ceil(eventObj.duration / 15) * 15;
   const hours = Math.floor(durationForCalc / 60);
   const minutes = durationForCalc % 60;
   // minus 15, da die Endzeit anfangs automatisch immer 15 Minuten später ist. Hängt von timeSlotDuration ab
   selectionInfo.end.setMinutes(
   selectionInfo.end.getMinutes() + minutes - 15
   );
   selectionInfo.end.setHours(selectionInfo.end.getHours() + hours);
   return {
      start: selectionInfo.start,
      end: selectionInfo.end,
   }
}