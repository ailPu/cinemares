<!DOCTYPE html>
<html lang="en">

<head>
   <title>@yield('pageTitle',config('app.name'))</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="csrf-token" content="{{ csrf_token() }}">


   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
   <link rel="preconnect" href="https://fonts.gstatic.com">
   <link rel="preconnect" href="https://fonts.gstatic.com">
   <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css2?family=Poiret+One&display=swap" rel="stylesheet">
   <!-- Optional JavaScript -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
   </script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
   </script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
   </script>

   <noscript>
      <style>
         .navbar-toggler {
            display: none;
         }

         .collapse.navbar-collapse {
            display: block;
         }
      </style>
   </noscript>

   <style>
      html,
      * {
         font-family: 'Poiret One', cursive, sans-serif;
      }

      body {
         position: relative;
      }

      h2,
      h3 {
         font-family: 'Abril Fatface', cursive;
      }

      .logo {
         font-size: 5rem;
      }

      .camera-logo {
         top: 0;
         left: 100%;
         width: 90px;
      }

      .banner {
         object-fit: cover;
         object-position: top center;
         height: 400px;
         width: 100%;
      }

      .flex-container {
         display: flex;
         flex-direction: column;
         justify-content: space-between;
         min-height: 100vh;
      }

      .container-fluid {
         padding: 0 !important;
      }

      .cart {
         position: relative;
      }

      .cart-icon {
         color: white;
         font-size: 2rem;
      }

      #cart-quantity {
         color: black;
         position: absolute;
         font-size: .8rem;
         left: 50%;
         top: 55%;
         transform: translate(-50%, -50%);
      }

      .logout button {
         font-size: 1.75rem !important;
      }

      .dropdown-menu .nav-link {
         color: black !important;
      }


      .row {
         margin: 0 !important;
      }

      footer {
         display: flex;
         justify-content: center
      }

      .overlay {
         position: fixed;
         display: none;
         top: 0;
         left: 0;
         width: 100%;
         height: 100%;
         z-index: 10;
         background: rgba(0, 0, 0, .3);
      }

      .lds-roller {
         display: inline-block;
         position: relative;
         width: 80px;
         height: 80px;
         position: absolute;
         top: 20%;
         z-index: 10;
         left: 50%;
         transform: translateX(-50%);
      }

      .lds-roller div {
         animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
         transform-origin: 40px 40px;
      }

      .lds-roller div:after {
         content: " ";
         display: block;
         position: absolute;
         width: 7px;
         height: 7px;
         border-radius: 50%;
         background: #fff;
         margin: -4px 0 0 -4px;
      }

      .lds-roller div:nth-child(1) {
         animation-delay: -0.036s;
      }

      .lds-roller div:nth-child(1):after {
         top: 63px;
         left: 63px;
      }

      .lds-roller div:nth-child(2) {
         animation-delay: -0.072s;
      }

      .lds-roller div:nth-child(2):after {
         top: 68px;
         left: 56px;
      }

      .lds-roller div:nth-child(3) {
         animation-delay: -0.108s;
      }

      .lds-roller div:nth-child(3):after {
         top: 71px;
         left: 48px;
      }

      .lds-roller div:nth-child(4) {
         animation-delay: -0.144s;
      }

      .lds-roller div:nth-child(4):after {
         top: 72px;
         left: 40px;
      }

      .lds-roller div:nth-child(5) {
         animation-delay: -0.18s;
      }

      .lds-roller div:nth-child(5):after {
         top: 71px;
         left: 32px;
      }

      .lds-roller div:nth-child(6) {
         animation-delay: -0.216s;
      }

      .lds-roller div:nth-child(6):after {
         top: 68px;
         left: 24px;
      }

      .lds-roller div:nth-child(7) {
         animation-delay: -0.252s;
      }

      .lds-roller div:nth-child(7):after {
         top: 63px;
         left: 17px;
      }

      .lds-roller div:nth-child(8) {
         animation-delay: -0.288s;
      }

      .lds-roller div:nth-child(8):after {
         top: 56px;
         left: 12px;
      }

      @keyframes lds-roller {
         0% {
            transform: rotate(0deg);
         }

         100% {
            transform: rotate(360deg);
         }
      }

      noscript {
         width: 100%;
         height: 100%;
         background: rgba(0, 0, 0, .6);
         z-index: 100;
      }

      noscript * {
         text-align: center;
         position: relative;
         margin: 0 auto;
         z-index: 1000;
         color: white;
         margin-bottom: 0 !important;
      }
   </style>


</head>

<body>
   <noscript>
      <div class="no-script-info alert bg-danger">
         <p>Um Onlinezahlungen tätigen zu können, bitte Javascript aktivieren.</p>
         <small> <i><u>Dein Cinemares-Team</u></i></small>
      </div>
   </noscript>

   <div class="flex-container">
      <div class="container-fluid">
         <div class="py-5 row">
            <div class="row col-12 justify-content-center align-items-start">
               <div class="position-relative">
                  <h1 class="logo text-center">Cinémares</h1>
                  <h3 class="text-center">seit 1963</h3>
                  <img class="camera-logo  d-none d-md-block position-absolute" src=" {{asset('img/cameralogo.jpg')}}"
                     alt="">
               </div>
            </div>
         </div>

         @include('layouts.nav')
         @yield('content')
      </div>
      <footer class="p-3 border bg-dark d-flex justify-content-center">
         <a class="text-white p-3" href="{{url("/agb")}}">AGB</a>
         <a class="text-white p-3" href="{{url("/legal-notice")}}">Impressum</a>
      </footer>
   </div>

   <div class="overlay">
      <div class="lds-roller">
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
      </div>
   </div>

   @stack('scripts')
</body>

</html>