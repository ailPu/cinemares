<?php

namespace App\Traits;

use App\Models\Ticket;

trait SelectTickets
{
   /**
    * Select Ticket/s with given conditions
    * @param mixed $type
    * @param mixed $condition1 DB query condition
    * @param mixed $condition2 DB query condition
    * @param int|null $userId
    * @return mixed
    */
   public function selectTickets($type, $condition1, $condition2, $userId = null)
   {
      if ($type == "ticketsInCart") return $this->cartTickets($condition1, $condition2, $userId);
      elseif ($type == "ticketsToReserve") return $this->ticketsToReserve($condition1, $condition2, $userId);
      elseif ($type == "ticketsInPayment") return $this->ticketsInPayment($condition1, $condition2, $userId);
      elseif ($type == "ticketInPaymentExists") return $this->ticketInPaymentExists($condition1, $condition2, $userId);
      elseif ($type == "ticketsReserved") return $this->reservedTickets($condition1, $condition2);
   }

   /**
    * Sort out Tickets that are stuck in payment to prevent user buying it without the ticket showing up in cart
    * @param mixed $tickets
    * @return \Illuminate\Support\Collection
    */
   public function sortOutTicketsStuckInPayment($tickets)
   {
      //! Womöglich hängt ein altes Ticket in Status 1, mit selber user_id. Vor dieser Methode werden Tickets von Status 0 = selected auf 1 = inPayment gesetzt. Diese Tickets haben somit einen neueren updated_at Zeitstempel 
      //! Wenn updated_at älter ist, dann ist es ein STUCK Ticket,das NICHT verkauft werden darf.

      $ticketsToSell = [];
      $latestTimestamp = $tickets[0]->updated_at;
      foreach ($tickets as $ticket) {
         if ($ticket->updated_at != $latestTimestamp) break;
         $ticketsToSell[] = $ticket;
      }
      $ticketsToSellCollection = collect($ticketsToSell);
      return $ticketsToSellCollection;
   }

   /**
    * Get Tickets that are currently in Cart
    * @param string $condition1 DB query condition 
    * @param string $condition2 DB query condition
    * @param int $userId
    * @return mixed
    */
   public function cartTickets($condition1, $condition2, $userId)
   {
      return Ticket::whereHas($condition1)->where([$condition2 => $userId, "status" => config("constants.ticket_status_codes.selected")])->get();
   }

   /**
    * Get tickets that are about to be reserved
    * @param string $condition1 DB query condition
    * @param string $condition2 DB query condition
    * @param int $userId
    * @return mixed
    */
   public function ticketsToReserve($condition1, $condition2, $userId)
   {
      return Ticket::whereHas($condition1)->where([$condition2 =>  $userId, "status" => config("constants.ticket_status_codes.selected")])->get();
   }

   /**
    * Get tickets that are reserved
    * @param string $condition1 DB query condition 
    * @param string $condition2 DB query condition
    * @return mixed
    */
   public function reservedTickets($condition1, $condition2)
   {
      return Ticket::whereHas($condition1, function ($query) use ($condition2) {
         $query->where("reservation_code", $condition2);
      })->get();
   }

   /**
    * Get tickets that are in payment
    * @param string $condition1 DB query condition
    * @param string $condition2 DB query condition
    * @param int|null $userId
    * @return mixed
    */
   public function ticketsInPayment($condition1, $condition2, $userId = false)
   {
      if ($condition1 == "reservation") {
         $tickets = Ticket::whereHas($condition1, function ($query) use ($condition2) {
            $query->where("reservation_code", $condition2);
         })->orderByDesc("updated_at")->get();
      } else {
         $tickets = Ticket::whereHas($condition1)->where([$condition2 =>  $userId, "status" => config("constants.ticket_status_codes.in_payment")])->orderByDesc("updated_at")->get();
      }
      if (!$tickets->count()) return $tickets;

      return $this->sortOutTicketsStuckInPayment($tickets);
   }

   /**
    * Check if a ticket in payment exists
    * @param string $condition1 DB query condition
    * @param string $condition2 DB query condition
    * @param int|null $userId
    * @return mixed    
    */
   public function ticketInPaymentExists($condition1, $condition2, $userId = null)
   {
      if ($condition1 == "reservation") {
         $exists = Ticket::whereHas($condition1, function ($query) use ($condition2) {
            $query->where("reservation_code", $condition2);
         })->where("status", config("constants.ticket_status_codes.in_payment"))->orderByDesc("updated_at")->first();
      } else {
         $exists = Ticket::whereHas($condition1)->where([$condition2 =>  $userId, "status" => config("constants.ticket_status_codes.in_payment")])->orderByDesc("updated_at")->first();
      }
      return $exists;
   }
}
