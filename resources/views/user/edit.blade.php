@extends('layouts.app')

@section('pageTitle','Mitarbeiter bearbeiten')

@section('content')
<div class="container">

   <div class="my-4">
      <form action="{{ route('user.update',$user->id) }}" method="POST">
         @csrf
         @method("put")
         <div id="test" class="form-group">
            <h3 class="text-center">Mitarbeiter bearbeiten</h3>
            <div class="form-group">
               <label for="email">Email</label>
               <input type="email" class="form-control" id="email" name="email" rows="3"
                  value="{{old("email",$user->email)}}">
               @error("email")
               <div class="text-danger">{{$message}}</div>
               @enderror
            </div>
            <div class="form-group">
               <label for="password">Passwort</label>
               <input type="password" class="form-control" id="password" name="password" rows="3">
               @error("password")
               <div class="text-danger">{{$message}}</div>
               @enderror
            </div>
            <div class="form-group">
               <label for="password-confirm">{{ __('Passwort bestätigen') }}</label>
               <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                  autocomplete="new-password">
               @error("password_confirmation")
               <div class="text-danger">{{$message}}</div>
               @enderror
            </div>
            <div class="form-group">
               <label for="role_id">Role:</label>
               <select @if($admins==1 && $user->employee->role_id == 1) disabled @endif name="role_id" id="">
                  <option value="">auswählen</option>
                  @foreach($roles as $role)
                  <option value="{{$role->id}}"
                     {{-- Mit collect kann ich eben prüfen, ob die Werte übereinstimmen und dann selected vergeben, aber eben nur beim 1.Mal! --}}
                     {{collect(old("role_id",$user->employee->role_id))->contains($role->id) ? "selected" : ""}}>
                     {{$role->name}}</option> @endforeach
               </select> @error("role_id") <div class="text-danger">{{$message}}</div>
               @enderror
            </div>
            <div class="text-center">
               <button type="submit" class="btn btn-primary">Mitarbeiter updaten</button>
            </div>
      </form>
   </div>
</div>

@endsection