<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
   use HasFactory, SoftDeletes;

   protected $fillable = ["reservation_code"];

   /**
    * Get related Ticketmodel Data
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function tickets()
   {
      return $this->hasMany(\App\Models\Ticket::class, "reservation_id", "id");
   }
}
