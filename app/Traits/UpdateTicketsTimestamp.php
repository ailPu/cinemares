<?php

namespace App\Traits;

use App\Models\Ticket;
use App\Traits\GetUser;

trait UpdateTicketsTimestamp
{
   use GetUser;

   /**
    * Synchronize and update selected Tickets' timestamp
    * @param mixed $value Ticket Statuscode
    * @param mixed $user
    * @return void
    */
   public function updateTicketsTimestamp($value, $user = false)
   {
      if (!$user) $user = $this->getUser();
      $tickets = Ticket::where([$user->queryCondition => $user->id, "status" => $value])->get();
      if (!$tickets->count()) return;
      foreach ($tickets as $ticket) $ticket->touch();
   }
}
