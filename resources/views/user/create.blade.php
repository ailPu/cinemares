@extends('layouts.app')

@section('pageTitle','Mitarbeiter hinzufügen')

@section('content')
<div class="container">
   <div class="my-4">
      <h3 class="text-center">Mitarbeiter hinzufügen</h3>
      <form action="{{ route('user.store') }}" method="POST">
         @csrf
         <div class="form-group">
            <label for="firstname">Vorname</label>
            <input type="text" class="form-control" id="firstname" name="firstname" required
               value="{{old("firstname")}}">
            @error("firstname")
            <div class="text-danger">{{$message}}</div>
            @enderror
         </div>
         <div class="form-group">
            <label for="lastname">Nachname</label>
            <input type="text" class="form-control" id="lastname" name="lastname" required value="{{old("lastname")}}">
            @error("lastname")
            <div class="text-danger">{{$message}}</div>
            @enderror
         </div>
         <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" required value="{{old("email")}}">
            @error("email")
            <div class="text-danger">{{$message}}</div>
            @enderror
         </div>
         <div class="form-group">
            <label for="password">Passwort</label>
            <input type="password" class="form-control" id="password" name="password" required>
            @error("password")
            <div class="text-danger">{{$message}}</div>
            @enderror
         </div>
         <div class="form-group">
            <label for="password-confirm">{{ __('Passwort bestätigen') }}</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
               autocomplete="new-password">
            @error("password_confirmation")
            <div class="text-danger">{{$message}}</div>
            @enderror
         </div>

         <div class="form-group">
            <select name="role_id" id="">
               <option value="">Role auswählen</option>
               @foreach($roles as $role)
               <option value="{{$role->id}}">{{$role->name}}</option>
               @endforeach
            </select>
            @error("role_id") <div class="text-danger">{{$message}}</div>
            @enderror
         </div>
         <div class="text-center">
            <button type="submit" class="btn btn-primary">Mitarbeiter hinzufügen</button>
         </div>
      </form>
   </div>
</div>

@endsection