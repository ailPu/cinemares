/**
 * Check if there are unreleased events in current view
 * @param {[]} events 
 * @param {string} startRange 
 * @param {string} endRange 
 * @returns {void} Void
 */
function checkForUnreleasedEventsInCurrentView(events,startRange,endRange){
   $("#release-button").attr("disabled",true);
   // Ist Letzter Tag im View "größer" als Jetztzeit?
   if (Date.parse(endRange) > Date.now()) {
      for (const event of events) {
         // Ist Eventzeit (sind ja alle, wenn man aus Monat kommt) "größer" als der 1.Tag im View (möchte ja nicht anhand von events in vorigen Wochen den Releasebutton freischalten)
         if(Date.parse(event.start) >= Date.parse(startRange)) {
            // Ist das Eventzeit "größer" als die Jetztzeit?
            if ((Date.parse(event.start) >= Date.now()) && (Date.parse(event.end) <= Date.parse(endRange))) {
               if (event.extendedProps.released == "0") {
                  $("#release-button").attr("disabled",false);
                  break;
               }
            }
         }
      }
   }
}