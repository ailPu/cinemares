<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("event_id");
            $table->unsignedBigInteger("seat");
            $table->unsignedBigInteger("row");
            $table->boolean("status");
            $table->string("guest_id")->nullable();
            $table->unsignedBigInteger("user_id")->nullable();
            $table->unsignedBigInteger("employee_id")->nullable();
            $table->unsignedBigInteger("reservation_id")->nullable();
            $table->unsignedBigInteger("receipt_id")->nullable();
            $table->decimal("price", 4, 2);
            $table->timestamps();

            $table->foreign("event_id")->references("id")->on("events")->onDelete("restrict")->onUpdate("cascade");
            $table->foreign("user_id")->references("id")->on("users")->onDelete("set null")->onUpdate("cascade");
            $table->foreign("employee_id")->references("id")->on("employees")->onDelete("set null")->onUpdate("cascade");
            $table->foreign("reservation_id")->references("id")->on("reservations")->onDelete("restrict")->onUpdate("cascade");
            $table->foreign("receipt_id")->references("id")->on("receipts")->onDelete("restrict")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
