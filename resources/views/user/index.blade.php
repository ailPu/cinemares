@extends('layouts.app')

@section('pageTitle','Alle Mitarbeiter')

@section('content')

@push("scripts")
<script src="{{ asset('js/user/removeEmployee.js')}}"></script>
@endpush

<div class="container">

   <div class="my-4">
      <a class="btn btn-outline-primary" href="{{ route('user.create') }}">Mitarbeiter hinzufügen</a>
      <a class="soft-deleted-users btn btn-outline-info @if(!$softDeletedUsers) disabled @endif"
         href="{{ route('user.indexTrash') }}">Softdeleted
         Mitarbeiter</a>
   </div>
   <h1>Alle Mitarbeiter</h1>
   @if(session("success"))
   <div class="alert alert-success">{{ session("success") }}</div>
   @endif
   @if(session("error"))
   <div class="alert alert-danger">{{ session("error") }}</div>
   @endif
   <table class="table table-bordered table-responsive-md">
      <thead>
         <tr>
            <th scope="col">#</th>
            <th scope="col">Vorname</th>
            <th scope="col">Nachname</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col">Bearbeiten</th>
            <th scope="col">Löschen</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($users as $user)
         <tr>
            <th scope="row">{{$user->id}}</th>
            <td>{{$user->employee->firstname ?? "Erstanmeldung steht aus"}}</td>
            <td>{{$user->employee->lastname ?? "Erstanmeldung steht aus"}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->employee->role->name}}</td>
            <td><a class="btn btn-info" href="{{route("user.edit",$user->id)}}"><svg width="1em" height="1em"
                     viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                     <path
                        d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                     <path fill-rule="evenodd"
                        d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                  </svg></a>
            </td>

            {{-- Prevent removing last admin --}}
            <td>
               <form class="formDelete user"
                  data-title="Wollen Sie {{$user->employee->firstname}} {{$user->employee->lastname}} wirklich löschen?"
                  action="{{route("user.destroy",$user->id)}}" method="POST">
                  <button class="btn btn-danger" @if($user->employee->role->name == "admin" && $admins == 1)
                     disabled @endif ><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill"
                        fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                           d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                     </svg></button>
                  @csrf
                  @method("delete")
               </form>
            </td>
         </tr>
         @endforeach
      </tbody>
   </table>
   {{$users->links('pagination::bootstrap-4')}}
</div>


<div class="modal" id="modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Löschen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <p id="modalBody"></p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary">Abbrechen</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Löschen</button>
         </div>
      </div>
   </div>
</div>


@endsection