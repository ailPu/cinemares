<?php

// use App\Events\AccessCalendar;
use App\Events\TicketBought;
use App\Http\Controllers\HomeController;
use App\Mail\Invoice;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Movies
Route::get('/', "MovieController@index");
Route::get('/movie-preview', "MovieController@preview")->name("movie.preview");
//! IDee für Previews - Ansonsten werden auch alle alten Spielzeiten angezeigt, sollte derselbe Filme auch in der nächsten Woche laufen 
Route::get('/movie-preview-show/{movie}', "MovieController@showPreview")->name("movie.showPreview");
Route::get('/movie-program', "MovieController@all")->name("movie.all");

Route::resource('/movie', "MovieController");

// Events
Route::resource('/event', "EventController");

// Tickets
Route::delete('/destroy-tickets-with-different-event', 'TicketController@destroyTicketsWithDifferentEvent')->name("ticket.destroyTicketsWithDifferentEvent");
Route::get('/bad-tickets', 'TicketController@badTickets')->name('ticket.badTickets');
Route::delete('destroy-bad-tickets', 'TicketController@destroyBadTickets')->name('ticket.destroyBadTickets');
Route::resource('/ticket', "TicketController");

// Calendar
Route::get('/calendar', 'CalendarController@index')->name("calendar.index");
Route::get('/calendar-show-with-tools', 'CalendarController@showWithTools')->name("calendar.showWithTools");
Route::get('/calendar-show-without-tools', 'CalendarController@showWithoutTools')->name("calendar.showWithoutTools");
Route::post('/calendar-fetch', 'CalendarController@fetchEvents')->name("calendar.fetchEvents");
Route::post('/calendar-check', 'CalendarController@checkIfMovieInOwnDb')->name("calendar.check");
Route::post('/calendar-insert-new-event', 'CalendarController@insertNewEvent')->name('calendar.insertNewEvent');
Route::post('/calendar-insert-new-movie-and-event', 'CalendarController@insertNewMovieAndEvent')->name('calendar.insertNewMovieAndEvent');
Route::delete('/calendar-destroy', 'CalendarController@destroy')->name("calendar.destroy");
Route::put('/calendar-update', 'CalendarController@update')->name("calendar.update");
Route::put('/calendar-release', 'CalendarController@release')->name("calendar.release");
Route::delete('/calendar-logout', 'CalendarController@logout')->name("calendar.logout");
// Calendar CURL
Route::get('/calendar-search-results', 'CalendarController@searchResults')->name("calendar.searchResults");
Route::get('/calendar-get-search-result-data', 'CalendarController@getSelectedMovieData')->name("calendar.getSearchResultData");

// PAYMENT/STRIPE
Route::post('/charge-online', 'PaymentController@chargeOnline')->name("charge.online");
Route::put('/charge-at-reception/cash', 'PaymentController@chargeAtReceptionWithCash')->name("charge.atReceptionWithCash");
Route::get('/in-payment/{reservation?}', 'PaymentController@awaitCash')->name("payment.awaitCash");
Route::put('/set-sold', 'PaymentController@manuallySetSold')->name("payment.setSold");
Route::get('/payment-success', 'PaymentController@success')->name("payment.success");
//! TODO 
Route::get('/payment-failure', 'PaymentController@failure')->name("payment.failure");

// CART
Route::get('/cart', 'CartController@index')->name("cart.index");

// Reservations
Route::put('/reserve', 'ReservationController@reserve')->name("reservation.reserve");
Route::get('/reservation-success', 'ReservationController@success')->name("reservation.success");
//! TODO 
Route::get('/reservation-failure', 'ReservationController@failure')->name("reservation.failure");

// Support
Route::put('/send-refund-mail', 'SupportController@sendMail')->name("support.sendMail");

// Users
Route::get('/user/trash', 'UserController@indexTrash')->name('user.indexTrash');
Route::put('/user/untrash/{user}', 'UserController@untrash')->name('user.untrash');
Route::delete('/user/destroytrash/{user}', 'UserController@destroyTrash')->name('user.destroyTrash');
Route::resource('/user', 'UserController');

// Employee
Route::resource('/employee', 'EmployeeController');

// Register/Login/Gates
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('/home', 'HomeController');

// Search Tickets
Route::get('/search', 'SearchController@checkIfBoughtOrReservation')->name('search.find');
Route::get('/search/{code}/{type?}/{condition?}/{status?}', 'SearchController@showResults')->name("search.show");

// Legal Notice
Route::view("/agb", "legal.agb");
Route::view("/legal-notice", "legal.legal_notice");
