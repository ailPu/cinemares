<?php

namespace App\Traits;

use App\Models\Ticket;
use App\Traits\GetUser;

trait CartItemCount
{
   use GetUser;

   /**
    * Get cartItemCount 
    * @param mixed|null $user
    * @return int
    */
   public function cartItemCount($user = NULL)
   {
      if ($user) {
         $quantity = Ticket::where([$user->queryCondition => $user->id, "status" => config("constants.ticket_status_codes.selected")])->get();
      } else {
         $user = $this->getUser();
         $quantity = Ticket::where([$user->queryCondition => $user->id, "status" => config("constants.ticket_status_codes.selected")])->get();
      }
      return $quantity->count();
   }
}
