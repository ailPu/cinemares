<!DOCTYPE html>
<html>

<head>
   <title>Cinemares.com</title>
</head>

<body>
   <h1 style="text-align: center">Deine Reservierung wurde erfolgreich abgewickelt</h1>
   <table style="border-collapse: collapse; border: 1px solid black; width: 100%">
      <thead style="text-align: left">
         <tr style="border: 1px solid black">
            <th style="border: 1px solid black">Film</th>
            <th style="border: 1px solid black">Spielzeit</th>
            <th style="border: 1px solid black">Saal</th>
            <th style="border: 1px solid black">Reihe</th>
            <th style="border: 1px solid black">Sitzplatz</th>
            <th style="border: 1px solid black">Preis</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($tickets as $ticket)
         <tr style="border: 1px solid black">
            <td style="border: 1px solid black" scope="row">{{$movieTitle}}</td>
            <td style="border: 1px solid black">{{ \Carbon\Carbon::parse($startEvent)->format('H:i d.m.Y')}}</td>
            <td style="border: 1px solid black">{{$roomName}}</td>
            <td style="border: 1px solid black">{{$ticket->row}}</td>
            <td style="border: 1px solid black">{{$ticket->seat}}</td>
            <td style="border: 1px solid black">{{$ticket->price}}€</td>
         </tr>
         @endforeach
      </tbody>

      <tfoot style="background:lightgray; color: black">
         <tr style="border: 1px solid black">
            <td style="border: 1px solid black">Total:</td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td style="border: 1px solid black">{{$total}}€</td>
         </tr>
      </tfoot>
   </table>

   <div class="mt-2">Dein Reservierungscode: {{$reservationCode}}</div>
</body>

</html>