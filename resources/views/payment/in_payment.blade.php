@extends('layouts.app')

@section('pageTitle','In Zahlung')

@push("scripts")
<script src="{{ asset('js/tickets/manuallySetSold.js')}}"></script>
@endpush

@section("content")
<div class="container">
   <table class="table table-bordered table-responsive-sm text-center">
      <h1 class="text-center my-4">Warte auf Bargeld</h1>
      <thead class="thead-inverse">
         <tr>
            <th>Film</th>
            <th>Spielzeit</th>
            <th>Saal</th>
            <th>Reihe</th>
            <th>Sitzplatz</th>
            <th>Preis</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($tickets as $ticket)
         <tr>
            <td scope="row">{{$eventData["movieTitle"]}}</td>
            <td>{{ \Carbon\Carbon::parse($eventData["startEvent"])->format('H:i | d.m.Y')}}</td>
            <td>{{$eventData["roomName"]}}</td>
            <td>{{$ticket->row}}</td>
            <td>{{$ticket->seat}}</td>
            <td>{{$ticket->price}}€</td>
         </tr>
         @endforeach
      </tbody>

      <tfoot class="bg-dark text-light">
         <tr>
            <td>Total:</td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td>{{$total}}€</td>
         </tr>
      </tfoot>
   </table>

   <div class="text-center my-4">
      <form action="{{route("payment.setSold")}}" class="set-sold" method="POST">
         @csrf
         @method("put")
         @if($reservedByUser)
         <input type="hidden" name="reserved_by_user" value="reserved_by_user">
         <input type="hidden" name="code" value="{{$reservationCode}}">
         @endif
         <button class="btn btn-success">Verkauft</button>
      </form>
   </div>
</div>

@endsection