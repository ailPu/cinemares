@extends("layouts.app")

@section("title","AGB")

@section("content")

<div class="container">
   <h1 class="adsimple-221137511 text-center">Impressum</h1>
   <p class="adsimple-221137511">Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63
      Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz.</p>
   <p class="adsimple-221137511">MusterFirma GmbH<br />Musterstrasse 1, Stiege 1 Tür 1, <br />1234 Musterhausen,
      <br />Österreich</p>
   <p class="adsimple-221137511">
      <strong>Unternehmensgegenstand:</strong> IT Dienstleistungen<br />
      <strong>UID-Nummer:</strong> ATU12345678<br />
      <strong>Firmenbuchnummer:</strong> 123456a<br />
      <strong>Firmenbuchgericht:</strong> Musterhausen<br />
      <strong>Firmensitz:</strong> 1234 Musterhausen</p>
   <p class="adsimple-221137511">
      <strong>Tel.:</strong> 01234/56789<br />
      <strong>Fax:</strong> 01234/56789-0<br />
      <strong>E-Mail:</strong> <a href="mailto:office@musterfirma.com">office@musterfirma.com</a>
   </p>
   <p class="adsimple-221137511">
      <strong>Mitglied bei:</strong> WKO, Landesinnung, etc.<br />
      <strong>Berufsrecht:</strong> Gewerbeordnung: www.ris.bka.gv.at</p>
   <p class="adsimple-221137511">
      <strong>Aufsichtsbehörde/Gewerbebehörde:</strong> Bezirkshauptmannschaft Musterhausen<br />
      <strong>Berufsbezeichnung:</strong> Webdesigner, Grafiker<br />
      <strong>Verleihungsstaat:</strong> Österreich</p>
   <p class="adsimple-221137511">
      <strong>Geschäftsführer</strong>
      <br />Markus Muster, Thomas Muster</p>
   <p class="adsimple-221137511">
      <strong>Beteiligungsverhältnisse</strong>
      <br />Gesellschafter Markus Muster 50%, Thomas Muster 50%</p>
   <p style="margin-top:15px;">Quelle: Erstellt mit dem <a title="Impressum Generator von firmenwebseiten.at"
         href="https://www.firmenwebseiten.at/impressum-generator/" target="_blank" rel="follow"
         style="text-decoration:none;">Impressum Generator</a> von firmenwebseiten.at</p>
   <h2 class="adsimple-221137511">EU-Streitschlichtung</h2>
   <p>Gemäß Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (ODR-Verordnung) möchten wir Sie über
      die Online-Streitbeilegungsplattform (OS-Plattform) informieren.<br />
      Verbraucher haben die Möglichkeit, Beschwerden an die Online Streitbeilegungsplattform der Europäischen Kommission
      unter <a class="adsimple-221137511"
         href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=DE" target="_blank"
         class="external" rel="nofollow">http://ec.europa.eu/odr?tid=221137511</a> zu richten. Die dafür notwendigen
      Kontaktdaten finden Sie oberhalb in unserem Impressum.</p>
   <p>Wir möchten Sie jedoch darauf hinweisen, dass wir nicht bereit oder verpflichtet sind, an
      Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.</p>
   <h2 class="adsimple-221137511">Haftung für Inhalte dieser Webseite</h2>
   <p>Wir entwickeln die Inhalte dieser Webseite ständig weiter und bemühen uns korrekte und aktuelle Informationen
      bereitzustellen. Leider können wir keine Haftung für die Korrektheit aller Inhalte auf dieser Webseite übernehmen,
      speziell für jene die seitens Dritter bereitgestellt wurden.</p>
   <p>Sollten Ihnen problematische oder rechtswidrige Inhalte auffallen, bitten wir Sie uns umgehend zu kontaktieren,
      Sie finden die Kontaktdaten im Impressum.</p>
   <h2 class="adsimple-221137511">Haftung für Links auf dieser Webseite</h2>
   <p>Unsere Webseite enthält Links zu anderen Webseiten für deren Inhalt wir nicht verantwortlich sind. Haftung für
      verlinkte Websites besteht laut <a class="adsimple-221137511"
         href="https://www.ris.bka.gv.at/Dokument.wxe?Abfrage=Bundesnormen&Dokumentnummer=NOR40025813&tid=221137511"
         target="_blank" rel="noopener nofollow" class="external">§ 17 ECG</a> für uns nicht, da wir keine Kenntnis
      rechtswidriger Tätigkeiten hatten und haben, uns solche Rechtswidrigkeiten auch bisher nicht aufgefallen sind und
      wir Links sofort entfernen würden, wenn uns Rechtswidrigkeiten bekannt werden.</p>
   <p>Wenn Ihnen rechtswidrige Links auf unserer Website auffallen, bitten wir Sie uns zu kontaktieren, Sie finden die
      Kontaktdaten im Impressum.</p>
   <h2 class="adsimple-221137511">Urheberrechtshinweis</h2>
   <p>Alle Inhalte dieser Webseite (Bilder, Fotos, Texte, Videos) unterliegen dem Urheberrecht. Falls notwendig, werden
      wir die unerlaubte Nutzung von Teilen der Inhalte unserer Seite rechtlich verfolgen.</p>
   <h2 class="adsimple-221137511">Bildernachweis</h2>
   <p>Die Bilder, Fotos und Grafiken auf dieser Webseite sind urheberrechtlich geschützt.</p>
   <p>Die Bilderrechte liegen bei den folgenden Fotografen und Unternehmen:</p>
   <ul class="adsimple-221137511">
      <li class="adsimple-221137511">Fotograf Mustermann</li>
   </ul>
   <h1 class="adsimple-221137511">Datenschutzerklärung</h1>
   <h2 class="adsimple-221137511">Datenschutz</h2>
   <p>Wir haben diese Datenschutzerklärung (Fassung 06.12.2020-221137511) verfasst, um Ihnen gemäß der Vorgaben der <a
         class="adsimple-221137511"
         href="https://eur-lex.europa.eu/legal-content/DE/ALL/?uri=celex%3A32016R0679&tid=221137511" target="_blank"
         rel="noopener noreferrer nofollow" class="external">Datenschutz-Grundverordnung (EU) 2016/679</a> zu erklären,
      welche Informationen wir sammeln, wie wir Daten verwenden und welche Entscheidungsmöglichkeiten Sie als Besucher
      dieser Webseite haben.</p>
   <p>Leider liegt es in der Natur der Sache, dass diese Erklärungen sehr technisch klingen, wir haben uns bei der
      Erstellung jedoch bemüht die wichtigsten Dinge so einfach und klar wie möglich zu beschreiben.</p>
   <h2 class="adsimple-221137511">Automatische Datenspeicherung</h2>
   <p>Wenn Sie heutzutage Webseiten besuchen, werden gewisse Informationen automatisch erstellt und gespeichert, so auch
      auf dieser Webseite.</p>
   <p>Wenn Sie unsere Webseite so wie jetzt gerade besuchen, speichert unser Webserver (Computer auf dem diese Webseite
      gespeichert ist) automatisch Daten wie</p>
   <ul class="adsimple-221137511">
      <li class="adsimple-221137511">die Adresse (URL) der aufgerufenen Webseite</li>
      <li class="adsimple-221137511">Browser und Browserversion</li>
      <li class="adsimple-221137511">das verwendete Betriebssystem</li>
      <li class="adsimple-221137511">die Adresse (URL) der zuvor besuchten Seite (Referrer URL)</li>
      <li class="adsimple-221137511">den Hostname und die IP-Adresse des Geräts von welchem aus zugegriffen wird</li>
      <li class="adsimple-221137511">Datum und Uhrzeit</li>
   </ul>
   <p>in Dateien (Webserver-Logfiles).</p>
   <p>In der Regel werden Webserver-Logfiles zwei Wochen gespeichert und danach automatisch gelöscht. Wir geben diese
      Daten nicht weiter, können jedoch nicht ausschließen, dass diese Daten beim Vorliegen von rechtswidrigem Verhalten
      eingesehen werden.</p>
   <h2 class="adsimple-221137511">Cookies</h2>
   <p>Unsere Webseite verwendet HTTP-Cookies, um nutzerspezifische Daten zu speichern.<br />
      Im Folgenden erklären wir, was Cookies sind und warum Sie genutzt werden, damit Sie die folgende
      Datenschutzerklärung besser verstehen.</p>
   <h3 class="adsimple-221137511">Was genau sind Cookies?</h3>
   <p>Immer wenn Sie durch das Internet surfen, verwenden Sie einen Browser. Bekannte Browser sind beispielsweise
      Chrome, Safari, Firefox, Internet Explorer und Microsoft Edge. Die meisten Webseiten speichern kleine Text-Dateien
      in Ihrem Browser. Diese Dateien nennt man Cookies.</p>
   <p>Eines ist nicht von der Hand zu weisen: Cookies sind echt nützliche Helferlein. Fast alle Webseiten
      verwenden Cookies. Genauer gesprochen sind es HTTP-Cookies, da es auch noch andere Cookies für andere
      Anwendungsbereiche gibt. HTTP-Cookies sind kleine Dateien, die von unserer Webseite auf Ihrem Computer gespeichert
      werden. Diese Cookie-Dateien werden automatisch im Cookie-Ordner, quasi dem „Hirn“ Ihres Browsers, untergebracht.
      Ein Cookie besteht aus einem Namen und einem Wert. Bei der Definition eines Cookies müssen zusätzlich ein oder
      mehrere Attribute angegeben werden.</p>
   <p>Cookies speichern gewisse Nutzerdaten von Ihnen, wie beispielsweise Sprache oder persönliche Seiteneinstellungen.
      Wenn Sie unsere Seite wieder aufrufen, übermittelt Ihr Browser die „userbezogenen“ Informationen an unsere Seite
      zurück. Dank der Cookies weiß unsere Webseite, wer Sie sind und bietet Ihnen die Einstellung, die Sie gewohnt
      sind. In einigen Browsern hat jedes Cookie eine eigene Datei, in anderen wie beispielsweise Firefox sind
      alle Cookies in einer einzigen Datei gespeichert.</p>
   <p>Es gibt sowohl Erstanbieter Cookies als auch Drittanbieter-Cookies. Erstanbieter-Cookies werden direkt von unserer
      Seite erstellt, Drittanbieter-Cookies werden von Partner-Webseiten (z.B. Google Analytics) erstellt. Jedes Cookie
      ist individuell zu bewerten, da jedes Cookie andere Daten speichert. Auch die Ablaufzeit eines Cookies variiert
      von ein paar Minuten bis hin zu ein paar Jahren. Cookies sind keine Software-Programme und enthalten keine Viren,
      Trojaner oder andere „Schädlinge“. Cookies können auch nicht auf Informationen Ihres PCs zugreifen.</p>
   <p>So können zum Beispiel Cookie-Daten aussehen:</p>
   <p>
      <strong class="adsimple-221137511">Name:</strong> _ga<br />
      <strong class="adsimple-221137511">Wert:</strong> GA1.2.1326744211.152221137511-6<br />
      <strong class="adsimple-221137511">Verwendungszweck:</strong> Unterscheidung der Webseitenbesucher<br />
      <strong class="adsimple-221137511">Ablaufdatum:</strong> nach 2 Jahren</p>
   <p>Diese Mindestgrößen sollte ein Browser unterstützen können:</p>
   <ul class="adsimple-221137511">
      <li class="adsimple-221137511">Mindestens 4096 Bytes pro Cookie</li>
      <li class="adsimple-221137511">Mindestens 50 Cookies pro Domain</li>
      <li class="adsimple-221137511">Mindestens 3000 Cookies insgesamt</li>
   </ul>
   <h3 class="adsimple-221137511">Welche Arten von Cookies gibt es?</h3>
   <p>Die Frage welche Cookies wir im Speziellen verwenden, hängt von den verwendeten Diensten ab und wird in den
      folgenden Abschnitten der Datenschutzerklärung geklärt. An dieser Stelle möchten wir kurz auf die verschiedenen
      Arten von HTTP-Cookies eingehen.</p>
   <p>Man kann 4 Arten von Cookies unterscheiden:</p>
   <p>
      <strong class="adsimple-221137511">Unerlässliche Cookies<br />
      </strong>Diese Cookies sind nötig, um grundlegende Funktionen der Webseite sicherzustellen. Zum Beispiel braucht
      es diese Cookies, wenn ein User ein Produkt in den Warenkorb legt, dann auf anderen Seiten weitersurft und später
      erst zur Kasse geht. Durch diese Cookies wird der Warenkorb nicht gelöscht, selbst wenn der User sein
      Browserfenster schließt.</p>
   <p>
      <strong class="adsimple-221137511">Zweckmäßige Cookies<br />
      </strong>Diese Cookies sammeln Infos über das Userverhalten und ob der User etwaige Fehlermeldungen bekommt. Zudem
      werden mithilfe dieser Cookies auch die Ladezeit und das Verhalten der Webseite bei verschiedenen Browsern
      gemessen.</p>
   <p>
      <strong class="adsimple-221137511">Zielorientierte Cookies<br />
      </strong>Diese Cookies sorgen für eine bessere Nutzerfreundlichkeit. Beispielsweise werden eingegebene Standorte,
      Schriftgrößen oder Formulardaten gespeichert.</p>
   <p>
      <strong class="adsimple-221137511">Werbe-Cookies<br />
      </strong>Diese Cookies werden auch Targeting-Cookies genannt. Sie dienen dazu dem User individuell angepasste
      Werbung zu liefern. Das kann sehr praktisch, aber auch sehr nervig sein.</p>
   <p>Üblicherweise werden Sie beim erstmaligen Besuch einer Webseite gefragt, welche dieser Cookiearten Sie zulassen
      möchten. Und natürlich wird diese Entscheidung auch in einem Cookie gespeichert.</p>
   <h3 class="adsimple-221137511">Wie kann ich Cookies löschen?</h3>
   <p>Wie und ob Sie Cookies verwenden wollen, entscheiden Sie selbst. Unabhängig von welchem Service oder welcher
      Webseite die Cookies stammen, haben Sie immer die Möglichkeit Cookies zu löschen, zu deaktivieren oder nur
      teilweise zuzulassen. Zum Beispiel können Sie Cookies von Drittanbietern blockieren, aber alle anderen Cookies
      zulassen.</p>
   <p>Wenn Sie feststellen möchten, welche Cookies in Ihrem Browser gespeichert wurden, wenn Sie Cookie-Einstellungen
      ändern oder löschen wollen, können Sie dies in Ihren Browser-Einstellungen finden:</p>
   <p>
      <a class="adsimple-221137511" href="https://support.google.com/chrome/answer/95647?tid=221137511" target="_blank"
         rel="noopener noreferrer nofollow" class="external">Chrome: Cookies in Chrome löschen, aktivieren und
         verwalten</a>
   </p>
   <p>
      <a class="adsimple-221137511" href="https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221137511"
         target="_blank" rel="noopener noreferrer nofollow" class="external">Safari: Verwalten von Cookies und
         Websitedaten mit Safari</a>
   </p>
   <p>
      <a class="adsimple-221137511"
         href="https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221137511"
         target="_blank" rel="noopener noreferrer nofollow" class="external">Firefox: Cookies löschen, um Daten zu
         entfernen, die Websites auf Ihrem Computer abgelegt haben</a>
   </p>
   <p>
      <a class="adsimple-221137511"
         href="https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221137511"
         target="_blank" rel="noopener noreferrer nofollow" class="external">Internet Explorer: Löschen und Verwalten
         von Cookies</a>
   </p>
   <p>
      <a class="adsimple-221137511"
         href="https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221137511" target="_blank"
         rel="noopener noreferrer nofollow" class="external">Microsoft Edge: Löschen und Verwalten von Cookies</a>
   </p>
   <p>Falls Sie grundsätzlich keine Cookies haben wollen, können Sie Ihren Browser so einrichten, dass er Sie immer
      informiert, wenn ein Cookie gesetzt werden soll. So können Sie bei jedem einzelnen Cookie entscheiden, ob Sie das
      Cookie erlauben oder nicht. Die Vorgangsweise ist je nach Browser verschieden. Am besten Sie suchen die Anleitung
      in Google mit dem Suchbegriff “Cookies löschen Chrome” oder „Cookies deaktivieren Chrome“ im Falle eines Chrome
      Browsers.</p>
   <h3 class="adsimple-221137511">Wie sieht es mit meinem Datenschutz aus?</h3>
   <p>Seit 2009 gibt es die sogenannten „Cookie-Richtlinien“. Darin ist festgehalten, dass das Speichern von Cookies
      eine Einwilligung von Ihnen verlangt. Innerhalb der EU-Länder gibt es allerdings noch sehr unterschiedliche
      Reaktionen auf diese Richtlinien. In Österreich erfolgte aber die Umsetzung dieser Richtlinie in § 96 Abs. 3 des
      Telekommunikationsgesetzes (TKG).</p>
   <p>Wenn Sie mehr über Cookies wissen möchten und technische Dokumentationen nicht scheuen, empfehlen wir <a
         class="adsimple-221137511" href="https://tools.ietf.org/html/rfc6265" target="_blank"
         rel="nofollow noopener noreferrer" class="external">https://tools.ietf.org/html/rfc6265</a>, dem Request for
      Comments der Internet Engineering Task Force (IETF) namens „HTTP State Management Mechanism“.</p>
   <h2 class="adsimple-221137511">Speicherung persönlicher Daten</h2>
   <p>Persönliche Daten, die Sie uns auf dieser Website elektronisch übermitteln, wie zum Beispiel Name, E-Mail-Adresse,
      Adresse oder andere persönlichen Angaben im Rahmen der Übermittlung eines Formulars oder Kommentaren im Blog,
      werden von uns gemeinsam mit dem Zeitpunkt und der IP-Adresse nur zum jeweils angegebenen Zweck verwendet, sicher
      verwahrt und nicht an Dritte weitergegeben.</p>
   <p>Wir nutzen Ihre persönlichen Daten somit nur für die Kommunikation mit jenen Besuchern, die Kontakt ausdrücklich
      wünschen und für die Abwicklung der auf dieser Webseite angebotenen Dienstleistungen und Produkte. Wir geben Ihre
      persönlichen Daten ohne Zustimmung nicht weiter, können jedoch nicht ausschließen, dass diese Daten beim Vorliegen
      von rechtswidrigem Verhalten eingesehen werden.</p>
   <p>Wenn Sie uns persönliche Daten per E-Mail schicken – somit abseits dieser Webseite – können wir keine sichere
      Übertragung und den Schutz Ihrer Daten garantieren. Wir empfehlen Ihnen, vertrauliche Daten niemals
      unverschlüsselt per E-Mail zu übermitteln.</p>
   <h2 class="adsimple-221137511">Rechte laut Datenschutzgrundverordnung</h2>
   <p>Ihnen stehen laut den Bestimmungen der DSGVO und des österreichischen <a class="adsimple-221137511"
         href="https://www.ris.bka.gv.at/GeltendeFassung.wxe?Abfrage=Bundesnormen&Gesetzesnummer=10001597&tid=221137511"
         target="_blank" rel="noopener noreferrer nofollow" class="external">Datenschutzgesetzes (DSG)</a> grundsätzlich
      die folgende Rechte zu:</p>
   <ul class="adsimple-221137511">
      <li class="adsimple-221137511">Recht auf Berichtigung (Artikel 16 DSGVO)</li>
      <li class="adsimple-221137511">Recht auf Löschung („Recht auf Vergessenwerden“) (Artikel 17 DSGVO)</li>
      <li class="adsimple-221137511">Recht auf Einschränkung der Verarbeitung (Artikel 18 DSGVO)</li>
      <li class="adsimple-221137511">Recht auf Benachrichtigung – Mitteilungspflicht im Zusammenhang mit der
         Berichtigung oder Löschung personenbezogener Daten oder der Einschränkung der Verarbeitung (Artikel 19 DSGVO)
      </li>
      <li class="adsimple-221137511">Recht auf Datenübertragbarkeit (Artikel 20 DSGVO)</li>
      <li class="adsimple-221137511">Widerspruchsrecht (Artikel 21 DSGVO)</li>
      <li class="adsimple-221137511">Recht, nicht einer ausschließlich auf einer automatisierten Verarbeitung —
         einschließlich Profiling — beruhenden Entscheidung unterworfen zu werden (Artikel 22 DSGVO)</li>
   </ul>
   <p>Wenn Sie glauben, dass die Verarbeitung Ihrer Daten gegen das Datenschutzrecht verstößt oder Ihre
      datenschutzrechtlichen Ansprüche sonst in einer Weise verletzt worden sind, können Sie sich bei der
      Aufsichtsbehörde beschweren, welche in Österreich die Datenschutzbehörde ist, deren Webseite Sie unter <a
         class="adsimple-221137511" href="https://www.dsb.gv.at/?tid=221137511" class="external"
         rel="nofollow">https://www.dsb.gv.at/</a> finden.</p>
   <h2 class="adsimple-221137511">Auswertung des Besucherverhaltens</h2>
   <p>In der folgenden Datenschutzerklärung informieren wir Sie darüber, ob und wie wir Daten Ihres Besuchs dieser
      Website auswerten. Die Auswertung der gesammelten Daten erfolgt in der Regel anonym und wir können von Ihrem
      Verhalten auf dieser Website nicht auf Ihre Person schließen.</p>
   <p>Mehr über Möglichkeiten dieser Auswertung der Besuchsdaten zu widersprechen erfahren Sie in der folgenden
      Datenschutzerklärung.</p>
   <h2 class="adsimple-221137511">TLS-Verschlüsselung mit https</h2>
   <p>Wir verwenden https um Daten abhörsicher im Internet zu übertragen (Datenschutz durch Technikgestaltung <a
         class="adsimple-221137511"
         href="https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016R0679&from=DE&tid=221137511"
         target="_blank" rel="noopener noreferrer nofollow" class="external">Artikel 25 Absatz 1 DSGVO</a>). Durch den
      Einsatz von TLS (Transport Layer Security), einem Verschlüsselungsprotokoll zur sicheren Datenübertragung im
      Internet können wir den Schutz vertraulicher Daten sicherstellen. Sie erkennen die Benutzung dieser Absicherung
      der Datenübertragung am kleinen Schlosssymbol links oben im Browser und der Verwendung des Schemas https (anstatt
      http) als Teil unserer Internetadresse.</p>
   <h2 class="adsimple-221137511">Newsletter Datenschutzerklärung</h2>
   <p>
      <span class="adsimple-221137511" style="font-weight: 400;">Wenn Sie sich für unseren Newsletter eintragen
         übermitteln Sie die oben genannten persönlichen Daten und geben uns das Recht Sie per E-Mail zu kontaktieren.
         Die im Rahmen der Anmeldung zum Newsletter gespeicherten Daten nutzen wir ausschließlich für unseren Newsletter
         und geben diese nicht weiter.</span>
   </p>
   <p>
      <span class="adsimple-221137511" style="font-weight: 400;">Sollten Sie sich vom Newsletter abmelden – Sie finden
         den Link dafür in jedem Newsletter ganz unten – dann löschen wir alle Daten die mit der Anmeldung zum
         Newsletter gespeichert wurden.</span>
   </p>
   <h2 class="adsimple-221137511">Stripe Datenschutzerklärung</h2>
   <p>Wir verwenden auf unserer Website ein Zahlungstool des amerikanischen Technologieunternehmens und
      Online-Bezahldienstes Stripe. Für Kunden innerhalb von der EU ist Stripe Payments Europe (Europe Ltd., 1 Grand
      Canal Street Lower, Grand Canal Dock, Dublin, Irland) verantwortlich. Das heißt, wenn Sie sich für Stripe als
      Zahlungsart entscheiden, wird Ihre Zahlung über Stripe Payments abgewickelt. Dabei werden Daten, die für den
      Zahlungsvorgang nötig sind, an Stripe weitergeleitet und gespeichert. In dieser Datenschutzerklärung geben wir
      Ihnen einen Überblick über diese Datenverarbeitung und Speicherung durch Stripe und erklären, warum wir Stripe auf
      unserer Website verwenden.</p>
   <h3 class="adsimple-221137511">Was ist Stripe?</h3>
   <p>Das Technologieunternehmen Stripe bietet Zahlungslösungen für Online-Bezahlungen an. Mit Stripe ist es möglich
      Kredit- und Debitkartenzahlungen in unserem Webshop zu akzeptieren. Stripe übernimmt den gesamten Zahlungsvorgang.
      Ein großer Vorteil von Stripe ist etwa, dass Sie während des Zahlungsvorgangs nie unsere Website bzw. den Shop
      verlassen müssen und die Zahlungsabwicklung sehr schnell erfolgt.</p>
   <h3 class="adsimple-221137511">Warum verwenden wir Stripe für unsere Website?</h3>
   <p>Wir wollen natürlich mit unserer Website und unserem eingebundenen Onlineshop das bestmögliche Service bietet,
      damit Sie sich auf unserer Seite wohl fühlen und unsere Angebote nutzen. Wir wissen, dass Ihre Zeit kostbar ist
      und daher speziell Zahlungsabwicklungen schnell und reibungslos funktionieren müssen. Neben unseren anderen
      Zahlungsanbietern haben wir mit Stripe einen Partner gefunden, der eine sichere und schnelle Zahlungsabwicklung
      gewährleistet.</p>
   <h3 class="adsimple-221137511">Welche Daten werden von Stripe gespeichert?</h3>
   <p>Wenn Sie sich für Stripe als Zahlungsart entscheiden, werden auch personenbezogene Daten von Ihnen an Stripe
      übermittelt und dort gespeichert. Dabei handelt es sich um Transaktionsdaten. Zu diesen Daten zählen etwa die
      Zahlungsmethode (also Kreditkarten- Debitkarten oder Kontonummer), Bankleitzahl, Währung, der Betrag und das Datum
      der Zahlung. Bei einer Transaktion kann weiters Ihr Name, Ihre E-Mail-Adresse, Ihre Rechnungs- oder Versandadresse
      und manchmal auch Ihr Transaktionsverlauf übermittelt werden. Diese Daten sind zur Authentifizierung nötig.
      Weiters kann Stripe zur Betrugsabwehr, der Finanzberichterstattung und um die eigenen Dienste vollständig anbieten
      zu können, auch neben technischen Daten zu Ihrem Gerät (wie IP-Adresse)  Name, Adresse, Telefonnummer und Ihr Land
      erfassen.</p>
   <p>Stripe verkauft keine Ihrer Daten an unabhängige Dritte, wie etwa Marketingagenturen oder andere Unternehmen, die
      mit dem Stripe-Unternehmen nichts zu tun haben. Die Daten können aber etwa an interne Abteilungen, einer
      beschränkten Anzahl externer Stripe-Partner oder zur Einhaltung gesetzlicher Vorschriften weitergeleitet werden.
      Stripe verwendet zur Erfassung von Daten auch Cookies. Hier finden Sie eine Auswahl an Cookies, die Stripe während
      des Zahlungsprozesses setzen kann:</p>
   <p>
      <strong class="adsimple-221137511">Name:</strong> m<br />
      <strong class="adsimple-221137511">Wert:</strong> edd716e9-d28b-46f7-8a55-e05f1779e84e040456221137511-5<br />
      <strong class="adsimple-221137511">Verwendungszweck:</strong> Dieses Cookie erscheint, wenn Sie die
      Zahlungsmethode auswählen. Es speichert und erkennt, ob Sie über einen PC, ein Tablet oder ein Smartphone auf
      unsere Website zugreifen.<br />
      <strong class="adsimple-221137511">Ablaufdatum:</strong> nach 2 Jahren</p>
   <p>
      <strong class="adsimple-221137511">Name:</strong> __stripe_mid<br />
      <strong class="adsimple-221137511">Wert:</strong> fc30f52c-b006-4722-af61-a7419a5b8819875de9221137511-1<br />
      <strong class="adsimple-221137511">Verwendungszweck:</strong> Um eine Kreditkartentransaktion durchführen zu
      können, wird dieses Cookie benötigt. Dazu speichert das Cookie Ihre Sitzungs-ID.<br />
      <strong class="adsimple-221137511">Ablaufdatum:</strong> nach einem Jahr</p>
   <p>
      <strong class="adsimple-221137511">Name:</strong> __stripe_sid<br />
      <strong class="adsimple-221137511">Wert:</strong> 6fee719a-c67c-4ed2-b583-6a9a50895b122753fe<br />
      <strong class="adsimple-221137511">Verwendungszweck:</strong> Auch dieses Cookie speichert Ihre ID und wird für
      den Zahlungsprozess auf unserer Website durch Stripe verwendet.<br />
      <strong class="adsimple-221137511">Ablaufdatum</strong>: nach Ablauf der Sitzung</p>
   <h3 class="adsimple-221137511">Wie lange und wo werden die Daten gespeichert?</h3>
   <p>Personenbezogene Daten werden grundsätzlich für die Dauer der Diensterbringung gespeichert. Das heißt, die Daten
      werden so lange gespeichert, bis wir die Zusammenarbeit mit Stripe auflösen. Um allerdings die gesetzlichen und
      behördlichen Pflichten zu erfüllen kann Stripe auch über die Dauer der Diensterbringung personenbezogene Daten
      speichern. Da Stripe ein weltweit tätiges Unternehmen ist, können die Daten auch in jedem Land, wo Stripe
      Dienstleistungen anbietet, gespeichert werden. So können auch Daten außerhalb Ihres Landes, zum Beispiel in den
      USA gespeichert werden.</p>
   <h3 class="adsimple-221137511">Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?</h3>
   <p>Stripe ist nach wie vor Teilnehmer des <a class="adsimple-221137511"
         href="https://www.privacyshield.gov/participant?id=a2zt0000000TQOUAA4?tid=221137511" target="_blank"
         rel="follow noopener noreferrer" class="external">EU-U.S. Privacy Shield Framework</a>, wodurch bis 16. Juli
      2020 der korrekte und sichere Datentransfer persönlicher Daten geregelt wurde. Nachdem der Europäische Gerichtshof
      das Abkommen für ungültig erklärt hat, stützt sich das Unternehmen nun nicht mehr auf dieses Abkommen, handelt
      aber noch nach den Prinzipien des Privacy Shields.</p>
   <p>Sie haben immer das Recht auf Auskunft, Berichtigung und Löschung Ihrer personenbezogenen Daten. Bei Fragen können
      Sie auch jederzeit das Stripe-Team über <a class="adsimple-221137511"
         href="https://support.stripe.com/contact/email" target="_blank" rel="noopener noreferrer nofollow"
         class="external">https://support.stripe.com/contact/email</a> kontaktieren.</p>
   <p>Cookies, die Stripe für ihre Funktionen verwenden, können Sie in Ihrem Browser löschen, deaktivieren oder
      verwalten. Je nachdem welchen Browser Sie verwenden, funktioniert dies auf unterschiedliche Art und Weise. Bitte
      beachten Sie aber, dass dann eventuell der Zahlungsvorgang nicht mehr funktioniert. Die folgenden Anleitungen
      zeigen, wie Sie Cookies in Ihrem Browser verwalten:</p>
   <p>
      <a class="adsimple-221137511" href="https://support.google.com/chrome/answer/95647?tid=221137511" target="_blank"
         rel="noopener noreferrer nofollow" class="external">Chrome: Cookies in Chrome löschen, aktivieren und
         verwalten</a>
   </p>
   <p>
      <a class="adsimple-221137511" href="https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221137511"
         target="_blank" rel="noopener noreferrer nofollow" class="external">Safari: Verwalten von Cookies und
         Websitedaten mit Safari</a>
   </p>
   <p>
      <a class="adsimple-221137511"
         href="https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221137511"
         target="_blank" rel="noopener noreferrer nofollow" class="external">Firefox: Cookies löschen, um Daten zu
         entfernen, die Websites auf Ihrem Computer abgelegt haben</a>
   </p>
   <p>
      <a class="adsimple-221137511"
         href="https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221137511"
         target="_blank" rel="noopener noreferrer nofollow" class="external">Internet Explorer: Löschen und Verwalten
         von Cookies</a>
   </p>
   <p>
      <a class="adsimple-221137511"
         href="https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221137511" target="_blank"
         rel="noopener noreferrer nofollow" class="external">Microsoft Edge: Löschen und Verwalten von Cookies</a>
   </p>
   <p>Wir haben Ihnen nun einen allgemeinen Überblick über die Verarbeitung und Speicherung der Daten durch Stripe
      gegeben. Wenn Sie noch mehr und noch genauere Informationen einholen wollen, dient Ihnen die ausführliche
      Stripe-Datenschutzerklärung unter <a class="adsimple-221137511" href="https://stripe.com/at/privacy?tid=221137511"
         target="_blank" rel="noopener noreferrer nofollow" class="external">https://stripe.com/at/privacy</a> als gute
      Quelle.</p>
   <p style="margin-top:15px;">Quelle: Erstellt mit dem <a title="Datenschutz Generator von firmenwebseiten.at"
         href="https://www.firmenwebseiten.at/datenschutz-generator/" target="_blank" rel="follow"
         style="text-decoration:none;">Datenschutz Generator</a> von firmenwebseiten.at</p>

</div>
@endsection