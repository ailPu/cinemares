/**
 * Remove all Sessiondata from modal
 */
function sessRemoveModal(){
   sessionStorage.removeItem("addEventModal");
   sessionStorage.removeItem("modalInput");
}