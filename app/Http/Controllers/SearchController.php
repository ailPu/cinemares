<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Traits\CartItemCount;
use App\Traits\GetEventData;
use App\Traits\GetUser;
use App\Traits\SelectTickets;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    use SelectTickets, GetUser, CartItemCount, GetEventData;

    public function __construct()
    {
        $this->middleware(['auth', 'can:admin&employee']);
    }

    /**
     * Check if Ticket for given code from Request Object is reserved or bought
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkIfBoughtOrReservation(Request $request)
    {
        $request->validate([
            "searchCode" => "min:4|max:4",
        ]);
        // Search Boughttickets 
        $boughtTickets = $this->searchForTickets("receipt", "order_code", $request->input("searchCode"), config("constants.ticket_status_codes.bought_online"));
        if ($boughtTickets) {
            return redirect()->action("SearchController@showResults",  ["code" => $request->searchCode, "condition" => "order_code", "type" => "receipt", "status" => config("constants.ticket_status_codes.bought_online")]);
        }

        // Search Reservations
        $reservedTickets = $this->searchForTickets("reservation", "reservation_code", $request->input("searchCode"),  config("constants.ticket_status_codes.reserved"));
        if ($reservedTickets) {
            return redirect()->action("SearchController@showResults", ["code" => $request->searchCode, "condition" => "reservation_code", "type" => "reservation", "status" => config("constants.ticket_status_codes.reserved")]);
        }

        return redirect()->action("SearchController@showResults", ["code" => $request->searchCode]);
    }

    /**
     * Show Searchresults for given code
     * @param string $code order- or reservationcode
     * @param string|null $ticketType
     * @param string|null $condition DB query condition
     * @param int|null $value ticket Statuscode
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function showResults($code, $ticketType = null, $condition = null, $value = null)
    {
        $user = $this->getUser();
        $cartQuantity = $this->cartItemCount($user);

        $total = 0;
        $tickets = collect([]);
        if ($ticketType) {
            $tickets = Ticket::whereHas($ticketType, function ($query) use ($condition, $code) {
                $query->where($condition, $code);
            })->get();
            if ($tickets->count()) {
                $total = $tickets->sum("price");
                $eventData = $this->getEventData($tickets[0]->event);
                return view("search.results", compact("user", "ticketType", "code", "eventData", "tickets", "total", "ticketType"));
            }
        }
        return view("search.results", compact("code", "tickets", "total", "ticketType", "cartQuantity"));
    }

    /**
     * Search for Tickets 
     * @param string $condition1 DB query condition
     * @param string $condition2 DB query condition
     * @param string $condition3 DB query condition
     * @param int $value Ticket Statuscode
     * @return mixed
     */
    public function searchForTickets($condition1, $condition2, $condition3, $value)
    {
        return Ticket::whereHas($condition1, function ($query) use ($condition2, $condition3) {
            $query->where($condition2, $condition3);
        })->where("status", $value)->first();
    }
}
