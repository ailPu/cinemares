<?php

namespace App\Http\Controllers;

use App\Events\CalendarOccupied;
use App\Events\LeftCalendar;
use App\Events\WantsToAccessCalendar;
use App\Models\Event;
use App\Models\Movie;
use App\Models\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CalendarController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'can:admin&employee', "prevent-history"]);
    }

    /**
     * Display calendar with or without tools depending on occupied state
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $roomsMap = $this->fetchRooms();

        $allowedToEditCalendar = $this->checkIfCalendarOccupied();
        if ($allowedToEditCalendar["isAllowed"] !== true) {
            event(new WantsToAccessCalendar);
            return view("calendar.show_without_tools", compact("roomsMap"));
        } else {
            event(new CalendarOccupied);
            return view("calendar.show_with_tools", compact("roomsMap"));
        }
    }

    /** 
     * Deletes current User from current_calendar_users table to make calendar-with-tools available for other users 
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        event(new LeftCalendar);
        DB::table("current_calendar_users")->where("user_id", Auth::id())->delete();

        if (request()->ajax()) return response()->json(["status" => 200, "redirectRoute" => route("movie.index")]);
        return redirect()->route("movie.index");
    }

    /**
     * Fetch Events to display in current calendar view
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchEvents(Request $request)
    {
        if ($request->permissions == "true") {
            $allowedToEditCalendar = $this->checkIfCalendarOccupied();
            if ($allowedToEditCalendar["isAllowed"] !== true) {
                return $allowedToEditCalendar["response"];
            }
        }

        $events = Event::with("movie")->where([["start_event", ">=", $request->start_event], ["end_event", "<=", $request->end_event], ["room_id", $request->room_id]])->get();
        $data = [];
        foreach ($events as $event) {
            $data[] = [
                "id" => $event["id"],
                "title" => $event["movie"]["title"],
                "movieId" => $event["movie_id"],
                "start" => $event["start_event"],
                "end" => $event["end_event"],
                "released" => $event["released"],
            ];
        }
        return response()->json(["status" => 200, "data" => $data]);
    }

    /**
     * Check if Movie already exists in own DB
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkIfMovieInOwnDb(Request $request)
    {
        $allowedToEditCalendar = $this->checkIfCalendarOccupied();
        if ($allowedToEditCalendar["isAllowed"] !== true) {
            return $allowedToEditCalendar["response"];
        }

        $movie = Movie::where("title", $request->title)->first();
        return response()->json(["status" => 200, "movie" => "$movie"]);
    }

    /**
     * Insert new movie and event in DB
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function insertNewMovieAndEvent(Request $request)
    {
        $allowedToEditCalendar = $this->checkIfCalendarOccupied();
        if ($allowedToEditCalendar["isAllowed"] !== true) {
            return $allowedToEditCalendar["response"];
        }

        $movie = new Movie($request->all());
        $movie->save();

        $eventData = ["movie_id" => $movie->id, "room_id" => $request->room_id, "start_event" => $request->start_event, "end_event" => $request->end_event];
        $eventRequestData = new Request($eventData);
        $eventRequestData->setMethod("POST");
        $this->insertNewEvent($eventRequestData, true);
        return response()->json(["status" => 200, "movie_id" => $movie->id]);
    }

    /**
     * Insert new Event in DB
     * @param \Illuminate\Http\Request $request
     * @param bool $previouslyInsertedMovie
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function insertNewEvent(Request $request, $previouslyInsertedMovie = false)
    {
        if (!$previouslyInsertedMovie) {
            $allowedToEditCalendar = $this->checkIfCalendarOccupied();
            if ($allowedToEditCalendar["isAllowed"] !== true) {
                return $allowedToEditCalendar["response"];
            }
        }

        $event = new Event($request->all());
        $event->save();

        if (!$previouslyInsertedMovie) return response()->json(["status" => 200]);
        else return;
    }

    /**
     * Remove event from DB
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $allowedToEditCalendar = $this->checkIfCalendarOccupied();
        if ($allowedToEditCalendar["isAllowed"] !== true) {
            return $allowedToEditCalendar["response"];
        }

        Event::findOrFail($request->event_id)->delete();
        return response()->json(["status" => 200, "success" => "Event erfolgreicht entfernt"]);
    }

    /**
     * Update event in DB 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $allowedToEditCalendar = $this->checkIfCalendarOccupied();
        if ($allowedToEditCalendar["isAllowed"] !== true) {
            return $allowedToEditCalendar["response"];
        }

        $event = Event::findOrFail($request->event_id);
        $event->update($request->except("event_id"));
        return response()->json(["status" => 200, "success" => $request->all()]);
    }

    /**
     * Release Events that are shown in current view 
     * @param \Illuminate\Http\Request $request 
     * @return \Illuminate\Http\JsonResponse
     */
    public function release(Request $request)
    {
        $allowedToEditCalendar = $this->checkIfCalendarOccupied();
        if ($allowedToEditCalendar["isAllowed"] !== true) {
            return $allowedToEditCalendar["response"];
        }

        Event::where([["room_id", "=", $request->room_id], ["start_event", ">=", $request->start_event], ["end_event", "<=", $request->end_event], ["released", "=", config("constants.calendar_status_codes.not_released")]])->update(["released" => config("constants.calendar_status_codes.released")]);
        return response()->json(["status" => 200, "success" => "Events erfolgreich live geschaltet"]);
    }

    /**
     * Check if calendar is occupied or if current User still has access
     * @return (false|\Illuminate\Http\JsonResponse)[]|true[] Contains information about Occupiedstatus or if Session timed out  
     */
    public function checkIfCalendarOccupied()
    {
        $currentlyLoggedInUser = DB::table("current_calendar_users")->first();
        if ($currentlyLoggedInUser && $currentlyLoggedInUser->updated_at < Carbon::now()->subMinutes(5)) {
            DB::table("current_calendar_users")->delete();
            if ($currentlyLoggedInUser->user_id == Auth::id()) {
                DB::table("current_calendar_users")->insert(["user_id" => Auth::id(), "created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
                return ["isAllowed" => true];
            }

            $currentlyLoggedInUser = false;
        }

        if (!$currentlyLoggedInUser) {
            DB::table("current_calendar_users")->insert(["user_id" => Auth::id(), "created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
            $currentlyLoggedInUser = DB::table("current_calendar_users")->first();
            return ["isAllowed" => true];
        } else if ($currentlyLoggedInUser->user_id == Auth::id()) {
            DB::table("current_calendar_users")->update(["updated_at" => Carbon::now()]);
            return ["isAllowed" => true];
        } else {
            return ["isAllowed" => false, "response" => response()->json(["status" => 400, "msg" => "Der Kalender wird gerade bearbeitet."])];
        }
    }

    /**
     * Fetch Cinemarooms
     * @return array $roomsMap Map of cinema rooms
     */
    public function fetchRooms()
    {
        $rooms = Room::select("name", "id")->get();
        $roomsMap = [];
        foreach ($rooms as $room) $roomsMap[$room->id] = $room;
        return $roomsMap;
    }

    /**
     * Searches Movies via API in external DB using curl for hiding API KEY from frontend
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchResults(Request $request)
    {
        $curl = curl_init();
        $searchString = preg_replace('/\s+/', "+", $request->searchQuery);
        $url = "https://api.themoviedb.org/3/search/movie?&query=$searchString&language=de";

        $this->setCurlOptions($curl, $url);

        $result = curl_exec($curl);
        curl_close($curl);

        return response()->json(["data" => json_decode($result)]);
    }

    /**
     * Get Data for clicked Searchresult 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSelectedMovieData(Request $request)
    {
        $curl = curl_init();
        $url = "https://api.themoviedb.org/3/movie/$request->movieId?language=de&append_to_response=credits";

        $this->setCurlOptions($curl, $url);

        $result = curl_exec($curl);
        curl_close($curl);
        return response()->json(["movie" => json_decode($result)]);
    }

    /** Sets curl options
     * @param mixed $curl Curl Session
     * @param string $url 
     * @return void
     */
    public function setCurlOptions($curl, $url)
    {
        $header = [];
        $header[] = 'Authorization: Bearer ' . config('theMovieDbApiKey.THE_MOVIE_DB_BEARER_TOKEN');
        $header[] = 'Content-Type: application/json;charset=utf-8';

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    }
}
