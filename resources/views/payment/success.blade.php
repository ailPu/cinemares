@extends('layouts.app')

@section('pageTitle','Kauf erfolgreich')

@section("content")

<div class="container">

   <table class="table table-bordered text-center">
      <h1 class="text-center my-4">
         @if($user->type != "employee")Dein @endif Kauf wurde erfolgreich abgewickelt
      </h1>
      @if($user->type != "employee")
      <h3 class="text-center">Wir haben dir deine Bestelldetails auch an
         {{$orderData["mail"]}}
         geschickt</h3>
      @endif
      <thead class="thead-inverse">
         <tr>
            <th>Film</th>
            <th>Spielzeit</th>
            <th>Saal</th>
            <th>Reihe</th>
            <th>Sitzplatz</th>
            <th>Preis</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($orderData["tickets"] as $ticket)
         <tr>
            <td scope="row">{{$orderData["movieTitle"]}}</td>
            <td>{{ \Carbon\Carbon::parse($orderData["startEvent"])->format('H:i d.m.Y')}}</td>
            <td>{{$orderData["roomName"]}}</td>
            <td>{{$ticket->row}}</td>
            <td>{{$ticket->seat}}</td>
            <td>{{$ticket->price}}€</td>
         </tr>
         @endforeach
      </tbody>

      <tfoot class="bg-dark text-light">
         <tr>
            <td>Total:</td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td class="border-0"></td>
            <td>{{$orderData["total"]}}€</td>
         </tr>
      </tfoot>
   </table>

   <div class="my-4">
      <div class="mt-2">@if($user->type != "employee")Dein @endif Bestellcode: {{$orderData["orderCode"]}}</div>
      {{-- <div class="mt-2">@if($user->type != "employee")Deine @endif Transaktions-ID:
         {{$orderData["transactionId"]}}
   </div> --}}
   @if($user->type != "employee")
   <div class="mt-2">Deine Referenz ID: {{$orderData["stripeReferenceId"]}}, falls du Fragen hast.</div>
   @endif
</div>
</div>

@endsection