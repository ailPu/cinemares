<?php

namespace App\Traits;

use App\Classes\Guest;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait GetUser
{
   /**
    * Get current User
    * @return mixed
    */
   public function getUser()
   {
      $employee = Employee::where("user_id", Auth::id())->first();
      if ($employee) {
         $employee->queryCondition = "employee_id";
         $employee->type = "employee";
         return $employee;
      }
      $registeredUser = User::find(Auth::id());
      if ($registeredUser) {
         $registeredUser->queryCondition = "user_id";
         $registeredUser->type = "registeredUser";
         return $registeredUser;
      }
      $guestObj = Guest::instance();
      if ($guestObj) {
         $guestObj->setSessionId();
         $guestObj->queryCondition = "guest_id";
         $guestObj->type = "guest";
         return $guestObj;
      }
   }
}
