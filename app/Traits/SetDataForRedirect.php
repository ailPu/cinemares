<?php

namespace App\Traits;

trait SetDataForRedirect
{
   /**
    * Set Data for redirect
    * @param mixed $tickets
    * @param mixed $orderCode
    * @param array $boughtOnlineData
    * @param bool $soldAtReceptionTransactionId
    * @param string|null $mail
    * @param bool $reservation
    * @return array
    */
   public function setDataForRedirect($tickets, $orderCode, $boughtOnlineData = [], $soldAtReceptionTransactionId = NULL, $mail = NULL, $reservation = NULL)
   {
      $basicData = [
         "total" => $tickets->sum("price"),
         "tickets" => $tickets,
         "movieTitle" => $tickets[0]->event->movie->title,
         "startEvent" => $tickets[0]->event->start_event,
         "roomName" => $tickets[0]->event->room->name,
      ];

      if (!empty($boughtOnlineData)) {
         $additionalData = [
            "transactionId" => $boughtOnlineData["balance_transaction"],
            "stripeReferenceId" => $boughtOnlineData["id"],
            "posterPath" => $tickets[0]->event->movie->posterPath,
            "mail" => $mail,
            "orderCode" => $orderCode,
         ];
         $data = array_merge($basicData, $additionalData);
      } elseif ($reservation) {
         $reservationData = [
            "posterPath" => $tickets[0]->event->movie->posterPath,
            "mail" => $mail,
            "reservationCode" => $orderCode,
         ];
         $data = array_merge($basicData, $reservationData);
      } elseif ($soldAtReceptionTransactionId) {
         $atReceptionData = [
            "transactionId" => $soldAtReceptionTransactionId,
            "orderCode" => $orderCode,
         ];
         $data = array_merge($basicData, $atReceptionData);
      }
      return $data;
   }
}
