
$(".formRestore.user").on("submit",function(e){
   e.preventDefault();
   const tr = $(this).closest("tr"); 
   const restoreText = $(this).data("title");
   const url = $(this).attr("action");

   $("#modal .modal-title").text("Wiederherstellen"); 
   $("#modalBody").text(restoreText);
   $("#modal .btn.footer").addClass("btn-success").text("Wiederherstellen");
   $('#modal').modal("show");

   $("#modal .btn.btn-primary").on("click",function(e){
      $('#modal').modal("hide");
   })

   $("#modal .btn.btn-success").on("click",function(e){
      $('#modal').modal("hide");

      $(".overlay").toggle();
      $.ajax({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         url,
         type: "put",
         success:function(resp){
            $(".overlay").toggle();
            if(resp.status == 200){
               tr.remove();
               $(`<div class="alert alert-success">${resp.msg}</div>`).insertAfter("h1");
            }
         },
         error:function(err){
            console.log(err);
         }
      })
   })
})