/**
 * If a placeholder elment exists in sessionstorage, draw a draggable placeholder
 */
function sessDrawDraggablePlaceholder(){
   placeholder = sessionStorage.getItem("draggablePlaceholder") ? sessionStorage.getItem("draggablePlaceholder") : null;
   if(placeholder){
      const searchParam = "takenslots=\"";
      const takenSlotsIndex = placeholder.indexOf(searchParam);
      const endIndexOfTakenSlots = takenSlotsIndex + searchParam.length;
      // To check if takenslots is 10 or more
      let takenSlots = placeholder.slice(endIndexOfTakenSlots,endIndexOfTakenSlots + 2);
      if(isNaN(takenSlots * 1)) {
         takenSlots = takenSlots.slice(0,1) * 1;
      }
      else {
         takenSlots = takenSlots * 1;
      }
      /* 
         JSON adds doublequotes at beginning and end of string which causes problem when appending placeholder to body
         That's why last and first char get removed 
      */
      $("body").append(placeholder.slice(1,-1));
      document.onmousemove = updatePlaceHolderOnMousemove;

      /**
       * Whenever User moves mouse update draggable placeholders attributedata
       * @param {event} e Event that gets fired everytime when user moves mouse
       */
      function updatePlaceHolderOnMousemove(e) {
         $("#drag").css({
            "width":`${$(".fc-scrollgrid-sync-inner").width()}px`, 
            "height":`${$(".fc-timegrid-slot.fc-timegrid-slot-lane").outerHeight() * takenSlots}px`, 
            "left": `${e.pageX - $(".fc-scrollgrid-sync-inner").width() / 2}px`,
            "top": `${e.pageY - 10}px`});
      }
   }
}