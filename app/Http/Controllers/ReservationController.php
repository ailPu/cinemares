<?php

namespace App\Http\Controllers;

use App\Mail\Invoice;
use App\Models\Reservation;
use App\Models\Ticket;
use App\Traits\CartItemCount;
use App\Traits\CreateOrderCode;
use App\Traits\GetUser;
use App\Traits\SelectTickets;
use App\Traits\SetDataForRedirect;
use App\Traits\UpdateTicketsTimestamp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ReservationController extends Controller
{
   use CreateOrderCode, UpdateTicketsTimestamp, SelectTickets, SetDataForRedirect, CartItemCount, GetUser;

   /**
    * Update Ticketstatus to reserved
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    */
   public function reserve(Request $request)
   {
      $user = $this->getUser();
      $ticketAllowedToReserve = $this->checkIfReservationPossible($request, $user);
      if ($ticketAllowedToReserve !== true) return $ticketAllowedToReserve;

      $tickets = $this->selectTickets("ticketsToReserve", "event.allowReservation", $user->queryCondition, $user->id);
      if (!$tickets->count()) {
         if (request()->ajax()) return response()->json(["status" => 400, "failureRoute" => route("reservation.failure")]);
         return redirect()->route("reservation.failure");
      }

      $reservationCode = $this->createOrderCode();

      $reservation = new Reservation();
      $reservation->save();
      $reservation->update(["reservation_code" => $reservationCode]);

      Ticket::where([$user->queryCondition => $user->id, "status" => config("constants.ticket_status_codes.selected")])->update(["status" => config("constants.ticket_status_codes.reserved"), "reservation_id" => $reservation->id]);

      if ($user->type == "employee") {
         $dataForRedirect = $this->setDataForRedirect($tickets, $reservationCode, [], false, NULL, true);
      } elseif ($user->type == "registeredUser") {
         $dataForRedirect = $this->setDataForRedirect($tickets, $reservationCode, [], false, $user->email, true);
         Mail::to($user->email)->send(new Invoice($dataForRedirect, "reservation"));
      }

      session(["reservationData" => $dataForRedirect]);
      if (request()->ajax()) return response()->json(["status" => 200, "successRoute" => route("reservation.success")]);
      else return redirect()->route("reservation.success");
   }

   /**
    * Check if reserving Ticket is possible
    * @param \Illuminate\Http\Request $request
    * @param mixed $user
    * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|true 
    */
   public function checkIfReservationPossible(Request $request, $user)
   {
      if ($user->type == "guest") {
         if (request()->ajax()) return response()->json(["status" => 403, "msg" => "Dieses Feature steht nur registrierten Usern zu Verfügung"]);
         else return redirect()->route("cart.index")->with("error", "Dieses Feature steht nur registrierten Usern zu Verfügung");
      } elseif (isset($request->too_late_for_reservation)) {
         if (request()->ajax()) return response()->json(["status" => 400, "tooLate" => true, "msg" => "Es können keine Tickets mehr reserviert werden"]);
         else return redirect()->route("cart.index")->with("error", "Es können keine Tickets mehr reserviert werden");
      }
      return true;
   }


   /**
    * Display Reservationdetails after successful reservation
    * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
    */
   public function success()
   {
      $user = $this->getUser();
      $cartQuantity = $this->cartItemCount($user);

      $reservationData = Session::get("reservationData");
      return view("reservation.success", compact("reservationData", "user", "cartQuantity"));
   }

   /**
    * Display Failuremessage after failed reservation
    * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
    */
   public function failure()
   {
      dd("OOPS... SOMETHING WENT WRONG WITH YOUR RESERVATION. SORRY!");
      return view("reservation.failure");
   }
}
