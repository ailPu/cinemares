<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("movie_id");
            $table->unsignedBigInteger("room_id");
            $table->dateTime("start_event");
            $table->dateTime("end_event");
            $table->boolean("released");
            $table->timestamps();

            $table->foreign("movie_id")->references("id")->on("movies")->onDelete("restrict")->onUpdate("cascade");
            $table->foreign("room_id")->references("id")->on("rooms")->onDelete("restrict")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
