<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use HasFactory;

    protected $fillable = ["event_id", "seat", "row", "status", "guest_id", "user_id", "reservation_id", "receipt_id", "employee_id", "price"];

    /**
     * Get related Eventmodel Data
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(\App\Models\Event::class, "event_id", "id");
    }

    /**
     * Get related Eventmodel data that starts in the future
     * @return mixed
     */
    public function futureEvent()
    {
        return $this->event()->where([["start_event", ">", Carbon::now()], ["released", "=", config("constants.calendar_status_codes.released")]]);
    }

    /**
     * Get related Eventmodel data that ends in the future
     * @return mixed
     */
    public function futureEndEvent()
    {
        return $this->event()->where([["end_event", ">", Carbon::now()], ["released", "=", config("constants.calendar_status_codes.released")]]);
    }

    /**
     * Get related reservations and expired reservations
     */
    public function reservation()
    {
        return $this->belongsTo(\App\Models\Reservation::class, "reservation_id", "id")->withTrashed();
    }

    /**
     * Get related Receiptmodel Data
     */
    public function receipt()
    {
        return $this->belongsTo(\App\Models\Receipt::class, "receipt_id", "id");
    }
}
